#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"
//实现函数的功能
static int Find_byName(const struct contact* ps, char name[MAX_NAME])
{
	int i = 0;
	for (i = 0; i < ps->size; i++)
	{
		if (0 == strcmp(ps->data[i].name, name))
		{
			return i;
		}
	}
	return -1;
}
//初始化通讯录中的信息
void Init_Contact(struct contact* ps)
{
	memset(ps->data, 0, sizeof(ps->data));
	ps->size = 0;//设置通讯录最初只有0个元素
}
//添加通讯录中的信息
void Add_Contacter(struct contact* ps)
{
	if (ps->size == MAX_NUM)
	{
		printf("通讯录已满！无法继续增加！\n");
	}
	else
	{
		printf("请输入名字:");
		scanf("%s", &ps->data[ps->size].name);
		printf("请输入年龄:");
		scanf("%d", &ps->data[ps->size].age);
		printf("请输入性别:");
		scanf("%s", &ps->data[ps->size].sex);
		printf("请输入电话:");
		scanf("%s", &ps->data[ps->size].phone);
		printf("请输入地址:");
		scanf("%s", &ps->data[ps->size].address);

		ps->size++;
		printf("添加成功！\n");
	}
}
//删除通讯录中的信息
void Del_Contacter(struct contact* ps)
{
	char name[MAX_NAME];
	printf("请输入你要删除的联系人的姓名：");
	scanf("%s", name);
	//查找要删除的人所在的位置
	//找到了返回名字所在元素的下标，没找到就返回-1
	int pos = Find_byName(ps,name);
	if (pos==-1)//删除的人不存在
	{
		printf("要删除的人不存在！\n");
	}
	else//删除
	{
		int j = 0;
		for (j = pos; j < ps->size-1; j++)
		{
			ps->data[j] = ps->data[j + 1];
		}
		ps->size--;
		printf("删除成功！\n");
	}
}
//查找通讯录中的信息
void Find_Contacter(const struct contact* ps)
{
	char name[MAX_NAME];
	printf("请输入你要查找的联系人的姓名：");
	scanf("%s", name);
	int pos = Find_byName(ps, name);
	if (pos==-1)//查找的人不存在
	{
		printf("要查找的人不存在！\n");
	}
	else
	{
		printf("%-20s\t%-4s\t%-5s\t%-12s\t%-20s\n", "姓名", "年龄", "性别", "电话", "地址");
		printf("%-20s\t%-4d\t%-5s\t%-12s\t%-20s\n", ps->data[pos].name,
			ps->data[pos].age,
			ps->data[pos].sex,
			ps->data[pos].phone,
			ps->data[pos].address);
	}
}
//修改通讯录中的信息
void Mod_Contacter(struct contact* ps)
{
	char name[MAX_NAME];
	printf("请输入你要修改的联系人的姓名：");
	scanf("%s", name);
	int pos = Find_byName(ps, name);
	if (pos==-1)//修改的人不存在
	{
		printf("要修改的人不存在！\n");
	}
	else
	{
		printf("请输入名字:");
		scanf("%s", &ps->data[pos].name);
		printf("请输入年龄:");
		scanf("%d", &ps->data[pos].age);
		printf("请输入性别:");
		scanf("%s", &ps->data[pos].sex);
		printf("请输入电话:");
		scanf("%s", &ps->data[pos].phone);
		printf("请输入地址:");
		scanf("%s", &ps->data[pos].address);
		printf("修改成功！\n");
	}
}
//打印通讯录中的信息
void Print_Contacter(const struct contact* ps)
{
	if (ps->size == 0)
	{
		printf("通讯录为空！\n");
	}
	else
	{
		//标题
		printf("%-20s\t%-4s\t%-5s\t%-12s\t%-20s\n",  "姓名", "年龄", "性别", "电话", "地址");
		int i = 0;
		while (i < ps->size)
		{
			//数据
			printf("%-20s\t%-4d\t%-5s\t%-12s\t%-20s\n", ps->data[i].name, 
				ps->data[i].age, 
				ps->data[i].sex, 
				ps->data[i].phone, 
				ps->data[i].address);
			i++;
		}
	}
}
//对通讯录中的信息进行排序
//排序函数
//1.按照姓名进行排序
int Conpare_ByName(const void *e1,const void *e2)
{
	return strcmp(((struct PeoInfo*)e1)->name, ((struct PeoInfo*)e2)->name);
}
//2.按照年龄进行排序
int Conpare_ByAge(const void* e1, const void* e2)
{
	return ((struct PeoInfo*)e1)->age-((struct PeoInfo*)e2)->age;
}
//3.按照住址进行排序
int Conpare_ByAddress(const void* e1, const void* e2)
{
	return strcmp(((struct PeoInfo*)e1)->address, ((struct PeoInfo*)e2)->address);
}
void Sort_Contacter(struct contact* ps)
{
	printf("请选择你想排序的方式：\n");
	printf("1.姓名\n2.年龄\n3.住址\n");
	int input = 0;
	scanf("%d", &input);
	switch (input)
	{
	case 1:
		qsort(ps->data,ps->size,sizeof(ps->data[0]),Conpare_ByName);
		printf("排序成功\n");
		break;
	case 2:
		qsort(ps->data, ps->size, sizeof(ps->data[0]), Conpare_ByAge);
		printf("排序成功\n");
		break;
	case 3:
		qsort(ps->data, ps->size, sizeof(ps->data[0]), Conpare_ByAddress);
		printf("排序成功\n");
		break;
	}
}
//清空通讯中的信息
void Clear_Contacter(struct contact* ps)
{
	memset(ps->data, 0, sizeof(ps->data));
	ps->size = 0;
	printf("清空成功！\n");
}