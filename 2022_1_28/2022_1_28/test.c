#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<errno.h>
//#include<string.h>
//int main()
//{
//	FILE* pf = fopen("text.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	FILE* pfwrite = fopen("text.txt", "w");
//	if (pfwrite == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	写文件
//	fputc('b', pfwrite);
//	fputc('i', pfwrite);
//	fputc('t', pfwrite);
//	fclose(pfwrite);
//	pfwrite = NULL;
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	FILE* pfread = fopen("text.txt", "r");
//	if (pfread == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	//读文件
//	printf("%c",fgetc(pfread));
//	printf("%c", fgetc(pfread));
//	printf("%c", fgetc(pfread));
//
//	fclose(pfread);
//	pfread = NULL;
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int ch = fgetc(stdin);
//	fputc(ch, stdout);
//	return  0;
//}
//#include<stdio.h>
//#include<errno.h>
//int main()
//{
//	char arr[1024] = { 0 };
//	FILE* pf = fopen("text.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	fputs("hello", pf);
//	fputs("world", pf);
//	fclose(pf);
//	pf = NULL;
//}
//#include<stdio.h>
//int main()
//{
//	//从键盘读取一行信息
//	char buf[1024] = { 0 };
//	fgets(buf, 1024, stdin);//从标准输入流读取
//	fputs(buf,stdout);//输出到标准输出流
//	//上面这两行等价于下面的这两行
//	/*gets(buf);
//	puts(buf);*/
//
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//struct S
//{
//	int n;
//	float score;
//	char arr[10];
//};
//int main()
//{
//	struct S s = { 100,3.14f,"bit" };
//	FILE* pf = fopen("text.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	fprintf(pf, "%d %f %s", s.n, s.score, s.arr);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//struct S
//{
//	int n;
//	float score;
//	char arr[10];
//};
//int main()
//{
//	struct S s = {0};
//	FILE* pf = fopen("text.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	fscanf(pf, "%d %f %s", &s.n, &s.score, &s.arr);
//	printf("%d %f %s", s.n, s.score, s.arr);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include<stdio.h>
//struct S
//{
//	int n;
//	float score;
//	char arr[10];
//};
//int main()
//{
//	struct S s = {0};
//	fscanf(stdin, "%d %f %s", &s.n, &s.score, &s.arr);
//	fprintf(stdout, "%d %f %s", s.n, s.score, s.arr);
//	return 0;
//}
//#include<stdio.h>
//struct S
//{
//	int n;
//	float score;
//	char arr[10];
//};
//int main()
//{
//	struct S s = { 100,3.14,"hello"};
//	struct S tmp = { 0 };
//	char buf[1024] = { 0 };
//	sprintf(buf, "%d %f %s", s.n, s.score, s.arr);
//	sscanf(buf, "%d %f %s", &(tmp.n), &(tmp.score), &(tmp.arr));
//	printf("%d %f %s\n", tmp.n, tmp.score, tmp.arr);
//	return 0;
//}
//#include<stdio.h>
//struct S
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct S s = { "张三",20,55.6 };
//	FILE* pf = fopen("text.txt", "wb");
//	if (pf == NULL)
//	{
//		return 0;
//	}
//	//以二进制的形式写文件
//	fwrite(&s, sizeof(struct S), 1, pf);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
#include<stdio.h>
struct S
{
	char name[20];
	int age;
	double score;
};
int main()
{
	struct S tmp = { 0 };
	FILE* pf = fopen("text.txt", "rb");
	if (pf == NULL)
	{
		return 0;
	}
	//以二进制的形式读文件
	fread(&tmp, sizeof(struct S), 1, pf);
	printf("%s %d %lf", tmp.name, tmp.age, tmp.score);
	fclose(pf);
	pf = NULL;
	return 0;
}
