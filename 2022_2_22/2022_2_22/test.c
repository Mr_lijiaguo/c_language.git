#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//int pivotIndex(int* nums, int numsSize)
//{
//	int i = 0;
//	int ret = 0;
//	int left_sum = 0;
//	int right_sum = 0;
//	for (i = 0; i < numsSize; i++)
//	{
//		right_sum += nums[i];
//	}
//	for (i = 0; i < numsSize; i++)
//	{
//		left_sum += nums[i];
//		if (left_sum == right_sum)
//		{
//			ret = i;
//			return ret;
//		}
//		right_sum -= nums[i];
//	}
//	return -1;
//}
//int main()
//{
//	int arr[] = {1};
//	int ret = pivotIndex(arr, 1);
//	printf("%d", ret);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//    char str[128] = { 0 };
//    char input[500] = { 0 };
//    scanf("%s", input);
//    int i = 0;
//    while (input[i] != '\0')
//    {
//        str[input[i]] = 1;
//        i++;
//    }
//    int num = 0;
//    for (i = 0; i < 128; i++)
//    {
//        if (str[i] != 0)
//        {
//            num++;
//        }
//    }
//    printf("%d\n", num);
//    return 0;
//}

//#include<stdio.h>
//#include<malloc.h>
//int majorityElement(int* nums, int numsSize)
//{
//	int com = numsSize / 2;
//	int count = 0;
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < numsSize; i++)
//	{
//		count = 0;
//		for (j = 0; j < numsSize; j++)
//		{
//			if (nums[i] == nums[j])
//			{
//				count++;
//			}
//		}
//		if (count > com)
//		{
//			return nums[i];
//		}
//	}
//	return -1;
//}
//int main()
//{
//	int arr[10] = { 1,1,3,1,1,6,1,8,1,1 };
//	int ret = majorityElement(arr, 10);
//	printf("%d", ret);
//	return 0;
//}
//int majorityElement(int* nums, int numsSize) {
//	int count = 1;
//	int tmp = nums[0];
//	for (int i = 1; i < numsSize; i++) {
//		if (tmp == nums[i]) {//与保存的字符相同则计数+1
//			count++;
//		}
//		else {//与保存的字符不同则计数-1
//			count--;
//			//计数为0表示有可能保存的字符不是最多的字符，换下一个
//			if (count == 0) tmp = nums[i + 1];
//		}
//	}
//	return tmp;
//}
//int majorityElement(int* nums, int numsSize)
//{
//	int count = 1;
//	int tmp = nums[0];
//	for (int i = 1; i < numsSize; i++)
//	{
//		if (tmp == nums[i])
//		{
//			count++;
//		}
//		else
//		{
//			count--;
//			if (count == 0)
//			{
//				tmp = nums[i + 1];
//			}
//		}
//	}
//	return tmp;
//}
#include<stdio.h>
#include<stdlib.h>
int* selfDividingNumbers(int left, int right, int* returnSize)
{
	int* ret = (int*)calloc(sizeof(int) , (right - left));
	int i = 0;
	int k = 0;
	int tmp = 0;
	int flag = 1;
	for (i = left; i < right; i++)
	{
		tmp = i;
		flag = 1;
		while (tmp)
		{
			if (i % (tmp % 10) != 0)
			{
				flag = 0;
			}
			tmp /= 10;
		}
		if (flag == 1)
		{
			ret[k] = i;
			(*returnSize)++;
			k++;
		}
	}
	return ret;
}
int main()
{
	int num = 0;
	int* ret = selfDividingNumbers(1, 32, &num);
	printf("%d\n", num);
	return 0;
}