#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int* p = malloc(sizeof(int) * 10);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*p++ = i;
//	}
//	free(p-10);
//	return 0;
//}
//#include<stdio.h>
//void GetMemory(char* p)
//{
//	p = (char*)malloc(100);//此处开辟的空间的首地址并没有被str给接收到，即退出函数后就丢失了开辟的这块空间的首地址，无法再对这亏啊空间进行操作
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(str);//这个地方只是一份临时拷贝，即值拷贝，不能进行修改，如果想要修改，就需要在函数那的参数类型设置为二级指针，且此处传地址
//	strcpy(str, "hello world");//程序崩溃，因为str并没有指向一块有效的空间，仍然是空指针
//	printf(str);
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
////1、运行代码会出现程序崩溃
////2、程序会出现内存泄漏的问题（str只以值传递的形式传递给p，p是GetMeory函数的形参，只在函数内部有效，等GetMeoory函数返回之后，动态开辟内存未释放
////并且无法找到，所以会造成内存泄漏）

//#include<stdio.h>
//#include<stdio.h>
//#include<stdlib.h>
//char* GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//	return p;
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory(str);
//	strcpy(str, "hello world");
//	printf(str);
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
//#include<stdio.h>
//char* GetMemory(void)
//{
//	static char p[] = "hello world";
//	return p;
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//}
//#include<stdio.h>
//#include<stdlib.h>
//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//}
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hello");
//	printf(str);
//	free(str);
//	str = NULL;
//}
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//void Test(void)
//{
//	char* str = (char*)malloc(100);
//	strcpy(str, "hello");
//	free(str);
//	if (str != NULL)
//	{
//		strcpy(str, "world");
//		printf(str);
//	}
//}
//
//int main()
//{
//	Test();
//	return 0;
//}
//#include<stdio.h>
//struct st_type
//{
//	int i;
//	int a[0];//柔性数组成员
//}type_a;
//#include<stdio.h>
//#include<stdlib.h>
//struct st_type
//{
//	int i;
//	int a[];//柔性数组成员
//}type_a;
//int main()
//{
//	struct st_type * p = (struct st_type*)malloc(sizeof(struct st_type) + 5 * sizeof(int));
//	//后面追加的这块内存空间就是给这块数组开辟的，即5个整型的空间
//	p->i = 100;
//	int i = 0;
//	/*for (i = 0; i < 5; i++)
//	{
//		p->a[i] = i;
//	}
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", p->a[i]);
//	}*/
//	struct st_type* ptr = realloc(p, 44);
//	if (ptr != NULL)
//	{
//		p = ptr;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		p->a[i] = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p->a[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//struct S
//{
//	int n;
//	int arr[0];
//};
//int main()
//{
//	struct S* ps = (struct S*)malloc(sizeof(struct S) + 5 * sizeof(int));
//	ps->n = 100;
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		ps->arr[i] = i;
//	}
//	struct S* ptr = realloc(ps, 44);
//	if (ptr != NULL)
//	{
//		ps = ptr;
//	}
//	for (i = 5; i < 10; i++)
//	{
//		ps->arr[i] = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", ps->arr[i]);
//	}
//	free(ps);
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//struct S
//{
//	int n;
//	int* p;
//};
//int main()
//{
//
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int sum = 0;
//	int i = 0;
//	int tmp = 0;
//	while (num)
//	{
//		tmp = num % 10;
//		if (tmp % 2 == 1)
//		{
//			tmp = 1;
//		}
//		else
//		{
//			tmp = 0;
//		}
//		sum += (tmp * pow(10, i++));
//		num /= 10;
//	}
//	printf("%d", sum);
//	return 0;
//}
//实现一个函数，可以左旋字符串中的k个字符。
//例如：
//ABCD左旋一个字符得到BCDA
//ABCD左旋两个字符得到CDAB
//#include<stdio.h>
//void Back_Str(char* arr, int k)
//{
//
//}
//int main()
//{
//	char arr[] = "abcd";
//	int k = 0;
//	scanf("%d", &k);
//	Back_Str(arr,k)
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[100] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int del = 0;
//	scanf("%d", &del);
//	int j = 0;
//	for (j = 0,i = 0; i < n; i++)
//	{
//		if (arr[i] != del)
//		{
//			arr[j++] = arr[i];
//		}
//	}
//	for (i = 0; i < j; i++)
//	{
//		printf("%d", arr[i]);
//	}
//	return 0;;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int arr[50] = { 0 };
//    int delete = 0;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    scanf("%d", &delete);
//    int i = 0;
//    int j = 0;
//    for (i = 0, j = 0; i < n; i++)
//    {//i的作用：遍历数组
//     //j的作用：记录存放数据位置的下标
//        if (arr[i] != delete)
//        {
//            arr[j++] = arr[i];
//        }
//        //此时的j中存放的数据就是删除元素后剩下的元素个数
//
//    }
//    for (int i = 0; i < j; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
