#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<stdlib.h>
//void test()
//{
//	printf("hello\n");
//	exit(0);
//}
//int main()
//{
//	test();
//	printf("world\n");
//	return 0;
//}
//#include<stdio.h>
//#define SWAP(x) ((x&0x55555555)<<1)+((x&0xAAAAAAAA)>>1)
//int fun(int x)
//{
//	int odd = x & (0x55555555);//取得奇数位的数字
//	int even = x & (0xAAAAAAAA);//取得偶数位数字
//	odd <<= 1;
//	even >>= 1;
//	return odd + even;
//}
//int main()
//{
//	int a = 37;
//	int ret = SWAP(a);
//	printf("%d\n", ret);
//	return 0;
//}
//#include<stdio.h>
//#include<stddef.h>
//struct S
//{
//	char c;
//	int a;
//	double b;
//};
//int main()
//{
//	struct S s = { 0 };
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int fun(int x)
//{
//	int a = 0;
//	int b = 1;
//	int ret1 = 0;
//	int ret2 = 0;
//	while (1)
//	{
//		int ret1 = (int)fabs(b - x);
//		int tmp = a + b;
//		a = b;
//		b = tmp;
//		int ret2 = (int)fabs(b - x);
//		if (ret1 < ret2)
//		{
//			return (ret1);
//		}
//	}
//}
//
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	int ret = fun(x);
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//void replaceSpace(char* str, int length)
//{
//	int i = 0;
//	int j = 0;
//	char* ret = (char*)malloc(sizeof(int)*(100000));
//	for (i = 0; i < length; i++)
//	{
//		if (str[i]!= ' ')
//		{
//			ret[j] = str[i];
//			j++;
//		}
//		else
//		{
//			ret[j++] = '%';
//			ret[j++] = '2';
//			ret[j++] = '0';
//		}
//	}
//	ret[j] = '\0';
//	for (int i = 0; i <= j; i++)
//	{
//		str[i] = ret[i];
//	}
//}
//int main()
//{
//	char str[100] = "hello world";
//	replaceSpace(str, 12);
//	printf("%s", str);
//	return 0;
//}
#include<stdio.h>
#define OFFSETOF(s_name,m_name) (int)&(((s_name*)0)->m_name)
struct S
{
	char ch;
	int i;
	double k;
};
int main()
{
	struct S s = { 0 };
	printf("%d\n", OFFSETOF(struct S, ch));
	printf("%d\n", OFFSETOF(struct S, i));
	printf("%d\n", OFFSETOF(struct S, k));

	return 0;
}