#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//	int val = 20;//在栈空间上开辟四个字节
//	char arr[10] = { 0 };//在栈空间上开辟10个字节的连续空间
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int* arr = (int*)malloc(sizeof(int) * n);
//	for (int i = 0; i < n; i++)
//	{
//		arr[i] = i;
//	}
//	for (int i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	int* p = (int*)malloc(sizeof(int) * 10);
//	if (p == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	else
//	{
//		//正常使用空间
//		int i = 0;
//		for (i = 0; i < 10; i++)
//		{
//			*(p+i) = i;
//		}
//		for (i = 0; i < 10; i++)
//		{
//			printf("%d ", *(p + i));
//		}
//	}
//	free(p);//释放ptr所指向的动态内存
//	p = NULL;//将p置为空指针，防止非法操作内存空间
//	return 0;
//}
//#include <stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	int* ptr = NULL;
//	ptr = (int*)malloc(num * sizeof(int));
//	if (NULL != ptr)//判断ptr指针是否为空
//	{
//		int i = 0;
//		for (i = 0; i < num; i++)
//		{
//			*(ptr + i) = 0;
//		}
//	}
//	free(ptr);//释放ptr所指向的动态内存
//	ptr = NULL;//将p置为空指针，防止非法操作内存空间
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int* p = NULL;
//	p = (int*)calloc(5,sizeof(int));
//	if (p != NULL)
//	{
//		for (int i = 0; i < 5; i++)
//		{
//			p[i] = i;
//		}
//	}
//	int *ptr = realloc(p,40);//如果返回的是一个新地址的话，realloc会自动将原来的p指针释放掉
//	if (ptr != NULL)
//	{
//		for (int i = 0; i < 10; i++)
//		{
//			ptr[i] = i;
//		}
//	}
//	free(ptr);//这个地方必须释放的是新地址
//	return 0;
//}
//void test()
//{
//	int* p = (int*)malloc(INT_MAX / 4);
//	*p = 20;//如果p的值是NULL，就会有问题
//	free(p);
//}
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int* p = malloc(sizeof(int) * 5);
//	if (p != NULL)
//	{
//		int i = 0;
//		for (i = 0; i < 10; i++)
//		{
//			p[i] = i;
//		}
//	}
//	free(p);
//	p = NULL;
//	return 0;
//}
//void test()
//{
//	int a = 10;
//	int* p = &a;
//	free(p);//ok?
//}
//void test()
//{
//	int* p = (int*)malloc(100);
//	p++;
//	free(p);//p不再指向动态内存的起始位置
//}
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int arr[10] = { 0 };
//	free(arr);
//	return 0;
//}
