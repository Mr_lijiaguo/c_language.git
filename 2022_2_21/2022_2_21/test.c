#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<math.h>
//#include<stdlib.h>
//int* printNumbers(int n, int* returnSize)
//{
//	*returnSize = pow(10, n)-1;
//	int* arr = (int*)malloc(sizeof(int)*(pow(10, n) - 1));
//	for (int i = 0; i < pow(10, n)-1; i++)
//	{
//		arr[i] = i+1;
//	}
//	return arr;
//}
//int main()
//{
//	int n = 0;
//	int num = 0;
//	int* q = NULL;
//	scanf("%d", &n);
//	q = printNumbers(n, &num);
//	for (int i = 0; i < 99; i++)
//	{
//		printf("%d ", q[i]);
//	}
//	printf("%d ", num);
//	return 0;
//}
//#include<stdio.h>
//int* findErrorNums(int* nums, int numsSize, int* returnSize)
//{
//	int *arr = malloc(sizeof(int) * 2);
//	int i = 0;
//	for (i = 1; i < numsSize; i++)
//	{
//		if (nums[i] == nums[i - 1])
//		{
//			*returnSize = i;
//			arr[0] = nums[i];
//			arr[1] = nums[i] + 1;
//			break;
//		}
//	}
//	return arr;
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,6,8,9,10 };
//	int num = 0;
//	int *arr1 = findErrorNums(arr, 10, &num);
//	printf("%d %d %d", num, arr1[0], arr1[1]);
//	return 0;
//}
//int* findErrorNums(int* nums, int numsSize, int* returnSize) {
//	*returnSize = 2;
//	//遍历nums数组，将其中数据对应的位置1， 哪一位如果已经重置过则意味着数据重复了
//	int* arr = (int*)calloc(numsSize + 1, sizeof(int));//申请numsSize个整形空间，并初始化为0
//	int* ret = (int*)calloc(*returnSize, sizeof(int));//申请2个整形空间，并初始化为0
//	int cur_sum = 0, old_sum = 0;
//	for (int i = 0; i < numsSize; i++) {
//		if (arr[nums[i]] == 1) { //这个数字在上边数组的对应位置已经置过1了，则重复
//			ret[0] = nums[i];//找到重复的数字
//		}
//		arr[nums[i]] = 1; //将标记数组的对应数据位置1
//		old_sum += i + 1; // 1~n的求和
//		cur_sum += nums[i]; //当前数组中的数据求和（多了一个重复的，少了一个丢失的）
//	}
//	ret[1] = old_sum - (cur_sum - ret[0]);//原始总和，减去去掉重复后的当前总和就是丢失的数字
//	free(arr);
//	return ret;
//}
//int* findErrorNums(int* nums, int numsSize, int* returnSize)
//{
//	*returnSize = 2;
//	int* arr = (int*)calloc(numsSize + 1, sizeof(int));
//	int* ret = (int*)calloc(*returnSize, sizeof(int));
//	int cur_sum = 0, old_sum = 0;
//	for (int i = 0; i < numsSize; i++)
//	{
//		if (arr[nums[i]] == 1)
//		{
//			ret[0] = nums[i];
//		}
//		arr[nums[i]] = 1;
//		old_sum += i + 1;
//		cur_sum += nums[i];
//	}
//	ret[1] = old_sum - (cur_sum - ret[0]);
//	free(arr);
//	return ret;
//}
#include<stdio.h>
int pivotIndex(int* nums, int numsSize)
{
	int i = 0;
	int ret = 0;
	int left_sum = 0;
	int right_sum = 0;
	for (i = 0; i < numsSize; i++)
	{
		right_sum += nums[i];
	}
	for (i = 0; i < numsSize; i++)
	{
		if (left_sum == right_sum)
		{
			ret = i;
			return ret;
		}
		left_sum += nums[i];
		right_sum -= nums[i];
	}
	return -1;
}
int main()
{
	int arr[] = { 1,7,3,6,5,6 };
	int ret = pivotIndex(arr, 6);
	printf("%d", ret);
	return 0;
}