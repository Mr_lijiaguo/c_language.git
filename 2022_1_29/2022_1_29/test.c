#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<errno.h>
//#include<string.h>
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	fgetc(pf);
//	rewind(pf);
//	int pos = ftell(pf);
//	printf("%d\n", pos);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int a = 10000;
//	FILE* pf = fopen("test.txt", "wb");
//	fwrite(&a, 4, 1, pf);//二进制的形式写到文件中
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include<stdio.h>
//#include<errno.h>
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	int ch = fgetc(pf);
//	printf("%d", ch);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//int main(void)
//{
//    int c; // 注意：int，非char，要求处理EOF
//    FILE* fp = fopen("test.txt", "r");
//    if (!fp) {
//        perror("File opening failed");
//        return EXIT_FAILURE;
//    }
//    //fgetc 当读取失败的时候或者遇到文件结束的时候，都会返回EOF
//    while ((c = fgetc(fp)) != EOF) // 标准C I/O读取文件循环
//    {
//        putchar(c);
//    }
//    //判断是什么原因结束的
//    if (ferror(fp))
//        puts("I/O error when reading");
//    else if (feof(fp))
//        puts("End of file reached successfully");
//    fclose(fp);
//}

//#include <stdio.h>
//enum { SIZE = 5 };
//int main(void)
//{
//    double a[SIZE] = { 1.,2.,3.,4.,5. };
//    FILE* fp = fopen("test.bin", "wb"); // 必须用二进制模式
//    fwrite(a, sizeof * a, SIZE, fp); // 写 double 的数组
//    fclose(fp);
//    double b[SIZE];
//    fp = fopen("test.bin", "rb");
//    size_t ret_code = fread(b, sizeof * b, SIZE, fp); // 读 double 的数组
//    if (ret_code == SIZE) {
//        puts("Array read successfully, contents: ");
//        for (int n = 0; n < SIZE; ++n) printf("%f ", b[n]);
//        putchar('\n');
//    }
//    else { // error handling
//        if (feof(fp))
//            printf("Error reading test.bin: unexpected end of file\n");
//        else if (ferror(fp)) {
//            perror("Error reading test.bin");
//        }
//    }
//    fclose(fp);
//}
//#include<stdio.h>
//int main()
//{
//	FILE* pf = fopen("text.txt", "r");//text2.txt文件并不存在
//	if (pf == NULL)
//	{
//		perror("hehe");
//		return 0;
//	}
//	int ch = 0;
//	while ((ch = fgetc(pf)) != EOF)
//	{
//		putchar(ch);
//	}
//	if (ferror(pf))
//	{
//		printf("error\n");
//	}
//	else if (feof(pf))
//	{
//		printf("end of file\n");
//	}
//	fclose(pf);
//	pf = NULL;
//	return  0;
//}
//#include<stdio.h>
//int main()
//{
//
//	FILE* pfwrite = fopen("text.txt", "w");
//	if (pfwrite == NULL)
//	{
//		perror("fopen\n");
//	}
//	
//	fclose(pfwrite);
//	pfwrite = NULL;
//	return 0;
//}
//写代码，把example.txt文件拷贝一份放在example2.txt中、
//从example.txt文件中读取数据放到example2.txt文件中，读一个写一个、
//#include<stdio.h>
//int main()
//{
//	FILE* pfread = fopen("example.txt", "r");
//	if (pfread == NULL)
//	{
//		//打开文件example.txt失败
//		perror("fopen");
//		return 1;
//	}
//	else
//	{
//		//打开文件example.txt成功
//		FILE* pfwrite = fopen("example2.txt", "w");
//		if (pfwrite == NULL)
//		{
//			//打开文件example2.txt失败
//			perror("fopen");
//			//打开example.txt文件成功的，打开example2.txt文件失败、
//			fclose(pfread);
//			pfread = NULL;
//			return 1;
//		}
//		else
//		{
//			//打开文件example2.txt成功
//			//读写文件
//			int ch = 0;
//			//while ((ch = fgetc(pfread)) != EOF)
//			//{
//			//	//写入文件
//			//	fputc(ch, pfwrite);
//			//}
//
//			while ((fgetc(pfread)) != EOF)
//			{
//				//写入文件
//				fputc(fgetc(pfread), pfwrite);
//			}
//
//		}
//		//关闭文件
//		fclose(pfwrite);
//		pfwrite = NULL;
//	}
//	//关闭文件
//	fclose(pfread);
//	pfread = NULL;
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	FILE* p = fopen("text.txt", "w");
//	fputs("", p);
//	fclose(p);
//	return 0;
//}
//#include<stdio.h>
//struct S
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct S tmp = { 0 };
//	FILE* pf = fopen("text.txt", "rb");
//	if (pf == NULL)
//	{
//		return 0;
//	}
//	//以二进制的形式读文件
//	fread(&tmp, sizeof(struct S), 1, pf);
//	printf("%s %d %lf", tmp.name, tmp.age, tmp.score);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
#include<stdio.h>
int main()
{
	/*int ch = fgetc(stdin);
	fputc(ch,stdout);*/
	FILE* pf = fopen("text.txt", "r");
	if (pf == NULL)
	{
		perror("");
	}
	/*char ch[20];
	scanf("%s", ch);
	fputs(ch,pf);*/
	//char arr[20] = "xxxxxxxxxxxxxxxxxxx";
	char arr[20] = { 0 };
	fscanf(pf, "%s", arr);
	printf("%s", arr);
	/*fgets(arr, 5, pf);*/
	//fprintf(pf, "%s", arr);
	/*printf("%c",fgetc(pf));
	printf("%c", fgetc(pf));
	printf("%c", fgetc(pf));
	printf("%c", fgetc(pf));
	printf("%c", fgetc(pf));*/
	fclose(pf);
	pf = NULL;
	return 0;
}