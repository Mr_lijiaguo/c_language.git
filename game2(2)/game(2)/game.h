#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define ROW 9
#define COL 9
#define ROWS ROW+2
#define COLS COL+2
#define EASY_COUNT 10

void menu();//菜单函数声明
void game();//游戏主初始化函数声明
void Init_board(char board[ROWS][COLS], int rows, int cols, char ch);//初始化函数声明
void Set_mine(char mine[ROWS][COLS], int row, int col, int count);//埋雷函数声明
void Display_board(char board[ROWS][COLS], int row, int col);//展示函数声明
void Find_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int rows, int cols);//扫雷函数声明
