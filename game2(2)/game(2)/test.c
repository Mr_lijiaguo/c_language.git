#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
//游戏函数
void game()
{
	char mine[ROWS][COLS] = { 0 };//存取放置好雷的信息的雷盘
	char show[ROWS][COLS] = { 0 };//存取展示给用户的信息的雷盘
	Init_board(mine, ROWS, COLS, '0');//初始化雷盘
	Init_board(show, ROWS, COLS, '*');//初始化展示的雷盘
	Set_mine(mine, ROW, COL, EASY_COUNT);//埋雷
	Display_board(show, ROW, COL);//展示想要展示雷盘
	Find_mine(mine, show, ROWS, COLS);//排雷游戏开始
}
void test()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		menu();
		printf("请输入->");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏！\n");
			break;
		}
	} while (input);

}
int main()
{
	test();
	return 0;
}