#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
//菜单
void menu()
{
	printf("***********************\n");
	printf("*******  1.play  ******\n");
	printf("*******  0.exit  ******\n");
	printf("***********************\n");
}
//初始化函数
void Init_board(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}
//展示函数
void Display_board(char board[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i <= row; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ", board[i][j]);
		}
		printf("\n");
	}
}
//埋雷函数
void Set_mine(char mine[ROWS][COLS], int row, int col, int count)
{
	while (count)
	{
		int x = rand() % 9 + 1;
		int y = rand() % 9 + 1;
		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}
static int count_num(char mine[ROWS][COLS], int x, int y)
{
	return mine[x - 1][y - 1] + mine[x - 1][y] + mine[x + 1][y] +
		mine[x + 1][y - 1] + mine[x + 1][y + 1] + mine[x - 1][y + 1] + mine[x][y + 1] +
		mine[x][y - 1] - 8 * '0';
}
//递归实现展开空白
void is_blank(int x, int y, char mine[ROWS][COLS], char show[ROWS][COLS],int *win)
{
	if (x >= 1 && x <= ROW && y >= 1 && y <= COL)
	{
		if (show[x][y] == ' ' || show[x][y] != '*')//排除已经检查过的点
			return;
		else if (count_num(mine, x, y) == 0)
		{
			show[x][y] = ' ';
			(*win)++;
			is_blank(x, y - 1, mine, show, win);
			is_blank(x-1, y - 1, mine, show, win);
			is_blank(x-1, y, mine, show, win);
			is_blank(x+1, y - 1, mine, show, win);
			is_blank(x+1, y + 1, mine, show, win);
			is_blank(x, y + 1, mine, show, win);
			is_blank(x+1, y, mine, show, win);
			is_blank(x - 1, y + 1, mine, show, win);
		}
		else
		{
			show[x][y] = count_num(mine, x, y) + '0';
			(*win)++;
		}
	}
}

//扫雷函数
void Find_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int rows, int cols)
{
	int x = 0;
	int y = 0;
	int win = 0;
	while (win < (rows - 2) * (cols - 2) - EASY_COUNT)
	{
		printf("请输入你要排查的坐标：");
		scanf("%d %d", &x, &y);
		if (x > ROW || x<0 || y>COL || y < 0)
		{
			printf("输入错误，请重新输入！\n");
		}
		else
		{
			if (mine[x][y] == '1')
			{
				printf("很遗憾，你被炸死了！\n");
				Display_board(mine, ROW, COL);
				break;
			}
			else //不是雷
			{
				is_blank(x,y,mine, show,&win);
				Display_board(show, ROW, COL);
			}
		}
	}
	if (win == (rows - 2) * (cols - 2) - EASY_COUNT)
	{
		printf("排雷成功！\n");
	}
}

