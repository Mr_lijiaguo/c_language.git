#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<stdbool.h>
//int main()
//{
//	/*bool x = true;
//	printf("%d\n", x);
//	printf("%d\n", sizeof(x));
//	x = false;
//	printf("%d\n", x);
//	printf("hello,world!");*/
//	
//	return 0;
//}
//int b = 100;
//int main()
//{
//	int a = 10;
//	{
//		int d = 0;
//	}
//	//printf("%d\n", d);
//	/*printf("%d", b);
//	printf("%d", a);*/
//
//	return 0;
//}
//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	scanf("%d %d", &num1, &num2);
//	int sum = num1 + num2;//C89或者C90标准都是在代码块最开始的地方定义
//	//而C99标准则是边定义边使用，两种方法都可以,此处遵循的是边定义边使用
//	printf("%d\n", sum);
//	return 0;
//}
//变量的生命周期与作用域
//int g = 100;
//void test()
//{
//	printf("test::g:%d\n", g);
//}
//int main()
//{
//	printf("%d\n", g);
//	test();
//	int a = 10;
//	{
//		int b = 20;
//		printf("%d\n", b);
//		printf("%d\n", a);
//		printf("%d\n", g);
//	}
//	//printf("%d\n", b);
//	printf("%d\n", a);
//	printf("%d\n", g);
//	return 0;
//}
//int main()
//{
//	////1.字面常量
//	//100;//整型常量
//	//3.14;//浮点常量
//	//"abfdj";//字符串型常量
//	//'w';//字符型常量
//	//int a[100];//其中的100也是常量，此处需注意，[]中的数字必须是常量，后面我们会用到这个
//	const int a = 10;
//	//a = 20;
//	int b[a];
//
//}
//#define A 100
//int main()
//{
//	A = 10;
//}
//enum Sex
//{
//	MALE,
//	FEMALE,
//	SECRET
//};
//int main()
//{
//	int a[SECRET];
//}
//#include<string.h>
//int main()
//{
//	printf("%c", '\60');
//}
//#include<stdio.h>
//int main()
//{
//	printf("     **     \n");
//	printf("     **     \n");
//	printf("************\n");
//	printf("************\n");
//	printf("   *    *   \n");
//	printf("   *    *   \n");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int M = 0;
//	scanf("%d", &M);
//	while (M < 1 || M > 100000)
//	{
//		scanf("%d", &M);
//	}
//	if (M % 5 == 0)
//	{
//		printf("YES");
//	}
//	else
//	{
//		printf("NO");
//	}
//}
//#include<stdio.h>
//int main()
//{
//	char arr1[] = "abc";
//	char arr2[5] = "abcd";
//	char arr3[5] = "abcde";
//	printf("%d\n", strlen(arr1));
//	printf("%d\n", strlen(arr2));
//	printf("%d\n", strlen(arr3));
//}
//#include<stdio.h>
//int main()
//{
//	char arr1[] = { 'a','b','c','d','\0' };
//	char arr2[] = { 'a','b','c','d', };
//	char arr3[6] = { 'a','b','c','d','e'};
//	printf("%d\n", sizeof(arr1));
//	printf("%d\n", sizeof(arr2));
//	printf("%d\n", sizeof(arr3));
//
//	printf("%d\n", strlen(arr1));
//	printf("%d\n", strlen(arr2));
//	printf("%d\n", strlen(arr3));
//
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	printf("%s\n", arr3);
//}
//#include<stdio.h>
//int main()
//{
//	//printf("hehe??");
//	//printf("%c", 'a');
//	//printf("%c", '\'');
//	//printf("");
//	//printf("c:\\code\\test.c\\n");
//	//printf("你好啊\t");
//	//printf("\r");
//	//printf("%d\n", '\171');
//
//	return 0;
//}
//#include<stdio.h>
//int MAX(int x, int y)
//{
//	int z = 0;
//	if (x > y)
//		z = x;
//	else
//		z = y;
//	return z;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int c = MAX(a, b);
//	printf("你输入的最大值为%d\n", c);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	printf("%d ", (a / b));
//	printf("%d ", (a % b));
//  return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 40;
//	int b = 212;
//	printf("%d\n", (-8 + 22) * a - 10 + b / 2);
//	return 0;
//}
//#define A int
//#include<stdio.h>
//A main()
//{
//	A a = 10;
//	printf("%d", a);
//	return 0;
//}
//int main()
//{
//	printf("%d\n", sizeof(10));
//	printf("%d\n", sizeof(3.14));
//	return 0;
//}
//int main()
//{
//	int a;
//	int b;
//	printf("%d %d", a, b);
//	return 0;
//}
//int main()
//{
//	int a ;
//	if (printf("%d",a))
//	{
//		printf("你好啊！");
//	}
//
//	return 0;
//}
//int main()
//{
//	int a = 1;
//	if (scanf("%d", &a))
//	{
//		printf("呵呵");
//	}
//	return 0;
//}
//int main()
//{
//	int a = 1;
//	if (printf("anjgfdk\n"))
//	{
//		printf("哈哈\n");
//	}
//	int b = printf("abc\n");
//	printf("%d\n", b);
//	
//	return 0;
//}

//int main()
//{
//	int x = 0;
//	int a = 0;
//	int b = 0;
//	x = scanf("%d %d", &a, &b);
//	printf("%d", x);
//	return 0;
//}
//void PRINTF()
//{
//	printf("呵呵");
//}
//int main()
//{
//	if (PRINTF())
//	{
//		printf("哈哈");
//	}
//	return 0;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	b = (a = 5);
//	printf("%d\n", b);
//	b = (a = 0);
//	printf("%d\n", b);
//	return 0;
//}
// 7/
//#include<stdio.h>
//int main()
//{
//    printf("0%o 0X%X", 1234, 1234);
//    return 0;
//}
// 8/
//#include<stdio.h>
//int main()
//{
//	printf("%15d", (int)0xABCDEF);
//	return 0;
//}
//9
//#include<stdio.h>
//int main()
//{
//	int a= printf("Hello world!");
//	printf("\n");
//	printf("%d\n", a);
//	return 0;
//}
// 10
//#include<stdio.h>
//int main()
//{
//	int score1 = 0;
//	int score2 = 0;
//	int score3 = 0;
//	scanf("%d %d %d", &score1, &score2, &score3);
//	printf("score1=%d,score2=%d,score3=%d", score1, score2, score3);
//
//	return 0;
//}
//11
//#include<stdio.h>
//int main()
//{
//    int NO = 0;
//    float grade1;
//    float grade2;
//    float grade3;
//    scanf("%d;%f,%f,%f", &NO, &grade1, &grade2, &grade3);
//    printf("The each subject score of  No. %d is %.2f, %.2f, %.2f.", NO, grade1, grade2, grade3);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	while (n)
//	{
//		printf("%d", n % 10);
//		n = n / 10;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	//EOF------end of file 文件结束标志(使文件能够停下来）
//	while ((a = getchar())!=EOF)
//	{
//		putchar(a + 32);
//		getchar();//把输入缓冲区的\n加载掉，但并不使用
//		printf("\n");
//	}
//	return 0;
//}
// 
//#include<stdio.h>
//int main()
//{
//	printf("%15d", 0xABCDEF);//注意此处要有0x作为开头，这样计算机才认为\
//	它是十六进制数字，不然计算机会认为是变量
//		return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int ch = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		if ((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
//		{
//			printf("YES\n");
//			getchar();
//		}
//		else
//		{
//			printf("NO");
//			getchar();
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int ch = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		if (isalpha(ch))
//		{
//			printf("YES\n");
//		}
//		else
//		{
//			printf("NO\n");
//		}
//		getchar();
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int ch = 0;
//	ch = getchar();
//	for (int i = 0; i < 5; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 4 - i; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j <= i; j++)
//		{
//			printf("%c ", ch);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a[] = { 73, 32, 99, 97, 110, 32, 100, 111, 32, 105, 116 , 33 };
//    for (int i = 0; i < sizeof(a)/sizeof(a[0]); i++)
//    {
//        printf("%c ", a[i]);
//    }
//}
//#include<stdio.h>
//int main()
//{
//	int year = 0;
//	int month = 0;
//	int date = 0;
//	//按照格式进行输入
//	scanf("%4d%2d%2d", &year, &month, &date);
//	//按照格式进行输出
//	printf("year=%4d\n", year);
//	printf("month=%02d\n", month);
//	printf("date=%2d\n", date);
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    scanf("%d", &a);
//    int num = 1;
//    for (int i = 0; i < a; i++)
//    {
//        num=num<<1;
//    }
//    printf("%d", num);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while ((scanf("%d", &n)) != EOF)
//    {
//        printf("%d", 1<<n);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while ((scanf("%d", &n)) != EOF)
//    {
//        printf("%d\n", 1 << n);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 40;
//    int c = 212;
//    printf("%d\n", (-8 + 22) * (-10) + c / 2);
//    return 0;
//}
//#include<stdio.h>
//extern int a;
//int main()
//{
//	printf("%d", a);
//}
//#include<stdio.h>
//int main()
//{
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    //输入
//    int height = 0;
//    int weight = 0;
//    float BMI = 0.0;
//    scanf("%d %d", &weight, &height);
//    //计算
//    BMI =  weight / ((height / 100.0) * (height / 100.0));
//    //输出
//    printf("%.2f", BMI);
//    return 0;
//}
//#include<stdio.h>
//#define pi 3.1415926
//int main()
//{
//    double r = 0.0;
//    double v = 0.0;
//    //输入
//    scanf("%lf", &r);
//    //计算
//    v = pi * r * r * r * 4.0 / 3.0;
//    //输出
//    printf("%.3lf", v);
//    return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int n = 0;
//	n = pow(2, 6);
//	printf("%d\n", n);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int score[3] = { 0 };
//    //输入
//    int i = 0;
//    for (i = 0; i < 3; i++)
//    {
//        scanf("%d", &score[i]);
//    }
//    printf("score1=%d,score2=%d,score3=%d", score[0], score[1], score[2]);
//    //输出
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int score[3] = { 0 };
//    //输入
//    int i = 0;
//    for (i = 0; i < 3; i++)
//    {
//        scanf("%d", &score[i]);
//    }
//    //输出
//    for (i = 0; i < 3; i++)
//    {
//        printf("score[%d]=%d ", i, score[i]);
//    }
//    
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int i = 0;
//    for (int i = 10000; i < 99999; i++)
//    {
//        int sum = 0;
//        int j = 0;
//        for (int j = 10; j <=10000; j*=10)
//        {
//            sum +=(i / j) * (i % j);
//        }
//        if (sum == i)
//        {
//            printf("%d ", i);
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	for (int i = 10000; i <= 99999; i++)
//	{
//		int sum = 0;
//		for (int j = 10; j <= 10000; j *= 10)
//		{
//			sum += (i / j) * (i % j);
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    //输入
//    int x1 = 0;
//    int x2 = 0;
//    int ret = 0;
//    scanf("%d %d", &x1, &x2);
//    //计算
//    ret = x1 + x2;
//    //输出
//    printf("%d", ret % 10 + ret / 10 % 10 * 10);
//    return 0;
//}
//需要注意这个
////#include<stdio.h>
////int main()
////{
////    //输入
////    double a = 0.0;
////    double ret = 0.0;
////    int num = 0;
////    scanf("%lf", &a);
////    //计算
////    ret = a - (int)a;
////    while ((ret*=10)>0)
////    {
////        num++;
////        ret = ret - (int)ret;
////    }
////    //输出
////    printf("%d", num);
////    return 0;
////}
//#include<stdio.h>
//int main()
//{
//	//输入
//	double a = 0.0;
//	int ret = 0;
//	scanf("%lf", &a);
//	//计算
//	ret = (int)a%10;
//	//输出
//	printf("%d", ret);
//}
//#include<stdio.h>
//int main()
//{
//	//输入
//	int ret = 0;
//	scanf("%d", &ret);
//	//计算
//	ret = ret % 10;
//	//输出
//	printf("%d", ret);
//}
//#include<stdio.h>
//int main()
//{
//    int age = 0;
//    long int ret = 0;
//    scanf("%d", &age);
//    ret = age * 3.156 * (10E+7);
//    printf("%ld", ret);
//    return 0;
//}
//#include<stdio.h>
//int PRINT(int x)
//{
//    if (x < 0)
//    {
//        return 1;
//    }
//    else if (x == 0)
//    {
//        return 0;
//    }
//    else
//    {
//        return -1;
//    }
//}
//int main()
//{
//    int x = 0;
//    int y = 0;
//    scanf("%d", &x);
//    y = PRINT(x);
//    printf("%d\n", y);
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//    int seconds = 0;
//    int hour = 0;
//    int minute = 0;
//    scanf("%d", &seconds);
//    hour = seconds / 60 / 60;
//    minute = seconds / 60;
//    printf("%d %d %d", hour, minute, seconds);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int score1 = 0;
//    int score2 = 0;
//    int score3 = 0;
//    int score4 = 0;
//    int score5 = 0;
//    float ave = 0.0;
//    scanf("%d %d %d %d %d", &score1, &score2, &score3, &score4, &score5);
//    ave = (score1 + score1 + score2 + score3 + score4 + score5) / 5.0;
//    printf("%.1f", ave);
//}
//#include<stdio.h>
//int main()
//{
//    char name[5] = "Jack";
//    int age = 18;
//    char gender[5] = "man";
//    printf("Name    Age    Gender\n");
//    printf("---------------------\n");
//    printf("%s    %d    %s", name, age, gender);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    double score1 = 0.0;
//    double score2 = 0.0;
//    double score3 = 0.0;
//    double sum = 0.0;
//    double ave =0.0;
//    //输入
//    scanf("%lf %lf %lf", &score1, &score2, &score3);
//    //计算
//    sum = score1 + score2 + score3;
//    //输出
//    printf("%.2lf %.2lf", sum, ave);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    double score[3] = { 0 };
//    double sum = 0.0;
//    double ave = 0.0;
//    //输入
//    for (int i = 0; i < 3; i++)
//    {
//        scanf("%lf", &score[i]);
//    }
//    //计算
//    
//    for (int j = 0; j < 3; j++)
//    {
//        sum += score[j];
//    }
//    ave = sum / 3;
//    printf("%.2lf %.2lf", sum,ave);
//    //输出
//    /*for (int i = 0; i < 3; i++)
//    {
//        printf("%lf", score[i]);
//    }*/
//
//    /*printf("%.2lf %.2lf", sum, ave);*/
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int h = 0;
//    int m = 0;
//    int ret = 0;
//    //输入
//    while ((scanf("%d %d %d", &n, &h, &m)) != EOF)
//        //计算
//    {
//        if (m % h == 0)
//        {
//            printf("%d", n - m / h);
//        }
//        else
//        {
//            printf("%d\n", n - m / h - 1);
//        }
//    }//输出
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int score[5] = { 0 };
//    //输入
//    int sum = 0;
//    double ave = 0.0;
//    for (int i = 0; i < 5; i++)
//    {
//        scanf("%d", &score[i]);
//    }
//    //计算
//    for (int j = 0; j < 5; j++)
//    {
//        sum += score[j];
//    }
//    ave = sum / 5.0;
//    //输出
//    printf("%.1lf", ave);
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int ret = 0;
//    //输入
//    scanf("%x %o", &a, &b);
//    //计算
//    ret = a + b;
//    //输出
//    printf("%d", ret);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int price = 0;
//    int month = 0;
//    int sale = 0;
//    double lastprice = 0.0;
//    //输入
//    scanf("%d %d %d", &price, &month, &sale);
//    //输出
//    if (month == 11)
//    {
//        if (sale == 1)
//        {
//            lastprice = price * 0.7 - 50;
//        }
//        else
//        {
//            lastprice = price * 0.7;
//        }
//
//    }
//    else if (month == 12)
//    {
//        if (sale == 1)
//        {
//            lastprice = price * 0.8 - 50;
//        }
//        else
//        {
//            lastprice = price * 0.8;
//        }
//    }
//    printf("%.2lf", lastprice);
//
//
//    return 0;
//
//}
//#include<stdio.h>
//int main()
//{
//    double price = 0;
//    int month = 0;
//    int day = 0;
//    int sale = 0;
//    double lastprice = 0.0;
//    //输入
//    scanf("%lf %d %d %d", &price, &month, &day, &sale);
//    //输出
//    if (month == 11 && day == 11)
//
//    {
//        if (sale == 1)
//        {
//            lastprice = price * 0.7 - 50.0;
//        }
//        else
//        {
//            lastprice = price * 0.7;
//        }
//
//    }
//    else if (month == 12 && day == 12)
//    {
//        if (sale == 1)
//        {
//            lastprice = price * 0.8 - 50.0;
//        }
//        else
//        {
//            lastprice = price * 0.8;
//        }
//    }
//    if (lastprice < 0)
//        printf("%.2lf", 0.0);
//    else
//    {
//        printf("%.2lf", lastprice);
//    }
//
//
//    return 0;
//
//}

//#include<stdio.h>
//#include<stdlib.h>
//int cmp_int(const void* e1, const void* e2)
//{
//    return *(int*)e2 - *(int*)e1;
//}
//int main()
//{
//    int arr[40] = { 0 };
//    int n = 0;
//    //输入
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    //排序
//    qsort(arr, n, 4, cmp_int);
//    //输出
//    for (int i = 0; i < 5; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	switch (day)
//	{
//	case 1:
//		printf("星期一！");
//		break;
//	case 2:
//		printf("星期二！");
//		break;
//	case 3:
//		printf("星期三！");
//		break;
//	case 4:
//		printf("星期四！");
//		break;
//	case 5:
//		printf("星期五！");
//		break;
//	case 6:
//		printf("星期六！");
//		break;
//	case 7:
//		printf("星期天！");
//		break;
//	default:
//		printf("您输入的数据有误！");
//		break;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 1;
//	for (a = 1; a <= 100; a++)
//	{
//		if (a % 2 == 1)
//		{
//			printf("%d ", a);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	switch (day)
//	{
//	case 1:
//		printf("星期一！\n");
//	case 2:
//		printf("星期二！\n");
//	case 3:
//		printf("星期三！\n");
//	case 4:
//		printf("星期四！\n");
//	case 5:
//		printf("星期五！\n");
//	case 6:
//		printf("星期六！\n");
//	case 7:
//		printf("星期天！\n");
//
//	default:
//		printf("您输入的数据有误！\n");
//
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	switch (day)
//	{
//	default:
//		printf("您输入的数据有误！");
//	case 1:
//		printf("星期一！");
//		return 0;
//	case 2:
//		printf("星期二！");
//		break;
//	case 3:
//		printf("星期三！");
//		break;
//	case 4:
//		printf("星期四！");
//		break;
//	case 5:
//		printf("星期五！");
//		break;
//	case 6:
//		printf("星期六！");
//		break;
//	case 7:
//		printf("星期天！");
//		break;
//	}
//	printf("呵呵！");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	//printf("%p", &a);
//	//int* p = &a;
//	//int* p = &(10);
//	//int* p = &a;
//	//printf("%d", *p);
//	int* p;
//	p = &a;
//	//printf("%d\n", sizeof(p));
//	printf("%p\n", &a);
//	printf("%p\n", p);
//	return 0;
//}
//struct Stu
//{
//	char name[20];
//	int age;
//	char sex[10];
//};
//#include<stdio.h>
//int main()
//{
//	struct Stu zhangsan = { "张三",30,"男" };
//	struct Stu lisi = { 0 };
//	struct Stu* p = &lisi;
//	//printf("%d %s %s", p->age, p->name, p->sex);
//	//printf("%s %d %s", zhangsan.name, zhangsan.age, zhangsan.sex);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int age = 0;
//	scanf("%d", &age);
//	/*if (age >= 18 && age < 40)
//	{
//		printf("呵呵");
//	}*/
//	/*if (18 <= age < 40)
//	{
//		printf("少年");
//	}*/
//	if (age < 18)
//	{
//		printf("少年");
//	}
//	else if (age >= 18 && age < 40)
//	{
//		printf("青年");
//	}
//	else if (age >= 40 && age < 60)
//	{
//		printf("中年");
//	}
//	else if (age >= 60 && age < 80)
//	{
//		printf("老年");
//	}
//	else
//	{
//		printf("老寿星");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int age = 0;
//	scanf("%d", &age);
//	if (age < 18)
//	{
//		printf("少年");
//	}
//	else
//	{
//		if (age >= 18 && age < 30)
//		{
//			printf("青年");
//		}
//		else if (age >= 30 && age < 60)
//		{
//			printf("中年");
//		}
//		else
//		{
//			printf("老年");
//		}
//	}
//	return  0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	if (a == 1)
//		if (b == 2)
//			printf("hehe\n");
//	else
//		printf("haha\n");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	if (5 == a)
//	{
//		printf("haha\n");
//	}
//	else
//	{
//		printf("hehe\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	if (a % 2 == 0)
//	{
//		printf("你输入的数字是偶数！");
//	}
//	else
//	{
//		printf("你输入的数字是奇数！");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	while (i <= 100)
//	{
//		printf("%d ", i);
//		i = i + 2;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	while (i < 100)
//	{
//		if (i % 2 != 0)
//		{
//			printf("%d ", i);
//		}
//		i++;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int day = 0;
//	scanf("%d", &day);
//	int n = 1;
//	switch (day)
//	{
//	case 1:
//	case 2:
//	case 3:
//	case 4:
//	case 5:
//		printf("你要好好听课哦！");
//		break;
//	case 6:
//	case 7:
//		printf("你可以稍微放松一下了！");
//		break;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 1;
//	int m = 2;
//	switch (n)
//	{
//	case 1:m++;
//	case 2:n++;
//	case 3:
//		switch (n)
//		{
//		case 1:
//			n++;
//		case 2:
//			m++;
//			n++;
//			break;
//		}
//	case 4:
//		m++;
//		break;
//	default:
//		break;
//	}
//	printf("m=%d,n=%d\n", m, n);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	while (i <= 10)
//	{
//		if (5 == i)
//			continue;
//		printf("%d", i);
//		i++;
//	}
//	return;
//}
//#include<stdio.h>
//int num = 10;
//int main()
//{
//	int num = 1;
//	printf("%d\n", num);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[3] = { 'b','i','t' };
//	printf("%d", sizeof(arr));
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	printf("%d\n", strlen("c:\test\121"));
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3 };
//	int arr[];
//	return 0;
//}
//int MAX(int x,int y)
//{
//	return (x > y ? x:y);
//}
//#include<stdio.h>
//int main()
//{
//	int n1 = 0;t n2 = 0;
//	int max = 0;
//	scanf("%d %d", &n1, &n2);
//	//max = MAX(n1, n2);
//	printf("%d\n", max);
//	return 0;
//}
//void test()
//{
//	static int a = 1;
//	a++;
//	printf("%d ", a);
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	while (i<10)
//	{
//		test();
//		i++;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	/*while (1)
//	{
//		int c = getchar();
//		if ('#' == c)
//		{
//			break;
//		}
//		putchar(c);
//	}*/
//	int  b = 1;
//	char c = 1;
//	putchar(b);
//	printf("\n");
//	putchar(c);
//	return 0;
//}
//#include <stdio.h>
//
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (i = 5)
//			printf("%d ", i);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	//输入
//	int a[3];
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	//输出
//	for (i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 3 - i - 1; j++)
//		{
//			if (a[i] < a[i + 1])
//			{
//				int temp = a[i];
//				a[i] = a[i + 1];
//				a[i + 1] = temp;
//			}
//		}
//	}
//	for (i = 0; i < 3; i++)
//	{
//		printf("%d ", a[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	for (i = 1; i < 100; i++)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	//输入
//	int x1 = 0;
//	int x2 = 0;
//	scanf("%d %d", &x1, &x2);
//	//计算
//	int x = 0;
//	x = (x1 < x2 ? x1 : x2);
//	int i = x1;
//	for (i = x1; i <= x1; i--)
//	{
//		if (x1 % i == 0 && x2 % i == 0)
//		{
//			printf("最大公约数为%d", i);
//			break;
//		}
//		else
//			continue;
//	}
//	//printf("% d % d", x1, x2);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int year = 1000;
//	for (year = 1000; year <= 2000; year++)
//	{
//		if (year % 4 == 0 && year % 100 != 0 )
//		{
//			printf("%d ", year);
//		}
//		if (year % 400 == 0)
//		{
//			printf("%d ", year);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int i = 100;
//	for (i = 100; i < 201; i++)
//	{
//		int j = 2;
//		for ( j = 2; j < sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				break;
//			}
//			else
//			{
//				continue;
//			}
//		}
//		if (j > sqrt(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char a = '0';
//	char b = 0;
//	printf("%d\n", a);
//	printf("%d\n", b);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	getchar();
//	if()
//	return 0;
//}
// 求1到n的阶乘和
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 1;
//	int j = 1;
//	int ret = 1;
//	int sum = 0;
//	for (i = 1; i <=n; i++)
//	{
//		ret = 1;
//		for (int j = 1; j <=i; j++)
//		{
//			ret *= j;
//		}
//		sum += ret;
//	}
//	printf("%d", sum);
//}
//#include<stdio.h>
//int main()
//{
//	int age = 0;
//	scanf("%d", &age);
//	if (age < 18)
//	{
//		printf("未成年！");
//	}
//	else if( 18 <= age < 60  )
//	{
//		printf("青年");
//	}
//	else if (age >= 30 && age < 60)
//	{
//		printf("中年");
//	}
//	else
//	{
//		printf("老年");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 1;
//	int num = 0;
//	for (a = 1; a < 101; a++)
//	{
//		if (9 == a % 10)
//		{
//			num++;
//		}
//		if (9 == a / 10)
//		{
//			num++;
//		}
//	}
//	printf("%d\n", num);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	double sum = 0.0;
//	for (i = 1; i < 101; i++)
//	{
//		if (i % 2 == 1)
//		{
//			sum += (1.0 / i);
//		}
//		else
//		{
//			sum += (-1.0 / i);
//		}
//	}
//	printf("%lf", sum);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	//输入
//	int a[10] = { 0 };
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	//求最大值
//	int max = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (a[i] > max)
//		{
//			max = a[i];
//		}
//		else
//		{
//			continue;
//		}
//	}
//	//输出
//	printf("%d\n", max);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <=9; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d * %d = %d  ", i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 4;
//	int size = sizeof(a) / sizeof(a[0]);
//	//printf("%d", size);
//	int left = 0;
//	int right = size - 1;
//	int mid = 0;
//	while (left <= right)
//	{
//		mid = (left + right) / 2;
//		if (k > a[mid])
//		{
//			left = mid + 1;
//		}
//		else if (k < a[mid])
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了！下标是%d\n", mid);
//			break;
//		}
//	}
//	if (left > right)
//	{
//		printf("没找到！");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int choice = 0;
//	printf("你要去表白吗？0表示不去，1表示去\n");
//	scanf("%d", &choice);
//	if (choice == 1)
//	printf("表白成功！\n");
//	printf("她成为了你的女朋友！\n");
//	else
//		printf("她成了别人的女朋友！");
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 2;
//    if (a == 1)
//    {
//        if (b == 2)
//            printf("hehe\n");
//        else
//            printf("haha\n");
//    }
//    return 0;
//}
//代码1
//#include<stdio.h>
//int main()
//{
//	int num = 5;
//
//	if (5 = num)
//	{
//		printf("呵呵！");
//	}
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	//输入
//	int  a = 0;
//	scanf("%d", &a);
//	//判断
//	if (1 == a % 2)
//	{
//		printf("你输入的数字是奇数！\n");
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int day = 0;
//    scanf("%d", &day);
//    switch (day)
//    {
//    case 1:
//        printf("星期一！");
//    case 2:
//        printf("星期二！");
//    case 3:
//        printf("星期三！");
//    case 4:
//        printf("星期四！");
//    case 5:
//        printf("星期五！");
//        break;
//    case 6:
//        printf("星期六！");
//    case 7:
//        printf("星期天！");
//        break;
//    }
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int n = 1;
//    int m = 2;
//    switch (n)//n为2
//    {
//    case 1:
//        m++;
//    case 2:
//        n++;
//    case 3:
//        switch (n)
//        {//switch允许嵌套使用
//        case 1:
//            n++;
//        case 2:
//            m++;
//            n++;
//            break;
//        }
//    case 4:
//        m++;
//        break;
//    default:
//        break;
//    }
//    printf("m = %d, n = %d\n", m, n);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	while (i < 11)
//	{
//		if (5==i)
//		{
//			continue;
//		}
//		printf("%d ", i);
//		i = i + 1;
//	}
//	return 0;
//}
//代码什么意思？
//代码1
//#include <stdio.h>
//int main()
//{
//    int ch = 0;
//    while ((ch = getchar()) != EOF)
//        putchar(ch);
//    return 0;
//}
////这里的代码适当的修改是可以用来清理缓冲区的.
////代码2
//#include <stdio.h>
//int main()
//{
//    char ch = '\0';
//    while ((ch = getchar()) != EOF)
//    {
//        if (ch < '0' || ch > '9')
//            continue;
//        putchar(ch);
//    }
//    return 0;
//}
////这个代码的作用是：只打印数字字符，跳过其他字符的
//#include<stdio.h>
//int main()
//{
//	int ch = 0;
//	while ((ch = getchar()) != EOF)
//		//printf("%c\n", ch);
//	{
//		putchar(ch);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char input[20] = { 0 };
//	printf("请输入密码：");
//	scanf("%s", input);//scanf在将数据存入变量时，遇到空格就停止，但输入并未停止，输入的其它数据会\
//	存入到输入缓冲区中去，就是说，如果输入了abcde hehe，只有前面的数据abcde被scanf吸收了，输入缓冲区中\
//	仍然有hehe\n
//	printf("请确认密码：Y/N：");
//	int tmp = 0;
//	while ((tmp = getchar()) != '\n')//这个地方是针对输入密码时输入abcde hehe\n\
//		最后的\n也会被拿走
//	{
//		;//空语句，什么也不干
//	}
//	int ch = getchar("%c");
//	if ( 'Y'==ch)
//	{
//		printf("确认成功！");
//	}
//	else
//	{
//		printf("确认失败！");
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 10; i++)
//	{
//		if (i == 5)
//			continue;
//		printf("%d ", i);
//	}
//	return 0;
//}
////代码作用：只接收并输出0到9的数字
//#include<stdio.h>
//int main()
//{
//	char ch = 0;
//	while ((ch = getchar()) != EOF)//注意这个地方不要写成(ch = getchar()!=EOF),如果这样写的话，程序就会先判断\
//		输入的是不是EOF，如果是，返回1，如果不是，返回0，然后将１或者0存储到ｃｈ变量中
//	{
//		if (ch < '0' || ch>'9')
//		{
//			continue;
//		}
//		else
//		{
//			printf("%c", ch);//这个地方不用{}也可
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 3; j++)
//		{
//			printf("hehe\n");
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++)
//	{
//		for (; j < 3; j++)
//		{
//			printf("hehe\n");
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int x, y;
//	for (x = 0, y = 0; x < 2 && y < 5; ++x, y++)
//	{
//		printf("hehe\n");
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int i = 0;
//    int k = 0;
//    for (i = 0, k = 0; k = 0; i++, k++)
//        k++;
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	do
//	{
//		printf("%d ", i);
//		i++;
//	} while (i < 10);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int ret = 1;
//	int i = 0;
//	
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//	{
//		ret *= i;
//	}
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int i = 0;
//	int j = 0;
//	int ret = 1;
//	int sum = 0;
//	scanf("%d", &n);
//	for (j = 1; j <= n; j++)
//	{
//		ret = 1;
//		for (i = 1; i <= j; i++)
//		{
//			ret *= i;
//		}
//		sum += ret;
//	}
//	printf("%d", sum);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 3;
//	int left = 0;
//	int size = sizeof(arr)/sizeof(arr[0]);
//	int right = size - 1;
//	int mid = 0;
//	while (left <= right)
//	{
//		mid = (left + right)/ 2;
//		if (k >arr[mid])
//		{
//			left = mid + 1;
//		}
//		else if (k < arr[mid])
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了！下标是%d \n", mid);
//			break;
//		}
//	}
//	if (left > right)
//	{
//		printf("找不到！");
//	}
//	return  0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<windows.h>
//int main()
//{
//	char arr1[] = "Hello,bit!!!!!";
//	char arr2[] = "##############";
//
//	int left = 0;
//	int right = strlen(arr1) - 1;
//	
//	while (left <= right)
//	{
//		arr2[left] = arr1[left];
//		arr2[right] = arr1[right];
//		printf("%s\n", arr2);
//		Sleep(1000);//睡眠函数，单位是毫秒,需要包含windows.h头文件
//		system("cls");//system函数是用来执行系统命令的,cls是清屏的命令
//		left++;
//		right--;
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	//输入
//	int x1 = 0;
//	int x2 = 0;
//	scanf("%d %d", &x1, &x2);
//	//计算
//	int x = 0;
//	x = (x1 < x2 ? x1 : x2);
//	int i = x1;
//	for (i = x1; i <= x1; i--)
//	{
//		if (x1 % i == 0 && x2 % i == 0)
//		{
//			printf("最大公约数为%d", i);
//			break;
//		}
//		else
//			continue;
//	}
//	//printf("% d % d", x1, x2);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int m = 0;
//	int n = 0;
//	int ret = 0;
//	scanf("%d %d", &m, &n);
//	int last = m * n;
//	while (ret = m % n)
//	{
//		m = n;
//		n = ret;
//	}
//	last /= n;
//	printf("%d",last );
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int i = 0;
//	char password[20] = "";//初始化一定不可以这样：char password[]="";这样里面只有一个结束标志，向里面添加字符\
//	或者字符串的时候会出现越界访问的现象,初始化的时候如果明确直到里面又多少元素，就最好先定义好元素个数
//	for (i = 0; i < 3; i++)
//	{
//		printf("请输入密码：>");
//		scanf("%s", password);//password 不取地址的原因就是数组名本身就是地址
//		if (strcmp(password, "lijiaguo"))//比较两个字符串是否相等一定不能直接使用==，\
//										 而应该使用strcmp,返回0，说明两个字符串相等\
//			但要注意引入<string,h>
//		{
//			printf("登陆成功！");
//			break;
//		}
//		else
//		{
//			printf("密码错误！请重新输入：");
//		}
//	}
//	if (3 == i)
//	{
//		printf("三次密码均错误退出程序！");
//	}
//	return 0;
//}
//C语言中生成随机数的方式是rand函数

//#include<stdio.h>
//#include<stdlib.h>
//#include<time.h>
//void game()
//{
//	int i = rand()%100+1;//1-100
//	printf("猜数字：->");
//	int guess = 0;
//	while(1)
//	{
//		scanf("%d", &guess);
//		if (guess < i)
//		{
//			printf("猜小了！\n");
//		}
//		else if (guess < i)
//		{
//			printf("猜大了！\n");
//		}
//		else
//		{
//			printf("猜对了！\n");
//			break;
//		}
//
//	}
//}
//void menu()
//{
//	printf("******************************************\n");
//	printf("***********      1. play     *************\n");
//	printf("***********      0、exit     *************\n");
//	printf("*****************************************\n");
//
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//	do {
//		menu();
//		printf("请选择：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏！");
//			break;
//		default:
//			printf("输入错误，请重新输入！");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//#include<time.h>
//void game()
//{
//	int r = rand()%100+1;
//	printf("猜数字：->\n");
//	int x = 0;
//	while (1)
//	{
//		scanf("%d", &x);
//		if (r > x)
//		{
//			printf("猜小了！\n");
//		}
//		else if (r < x)
//		{
//			printf("猜大了！\n");
//		}
//		else
//		{
//			printf("猜对了！\n");
//			break;
//		}
//	}
//
//}
//void menu()
//{
//	printf("****************************\n");
//	printf("******   1、game   *********\n");
//	printf("******   0、exit   *********\n");
//	printf("****************************\n");
//}
//int main()
//{
//	srand((unsigned)time(NULL));
//	int input = 0;
//	do{
//		menu();
//		scanf("%d", &input);
//		if (1 == input)
//		{
//			game();
//		}
//		else {
//			printf("退出游戏！");
//			break;
//		}
//	} while(input);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char input[20] = { 0 };
//	printf("请输入密码：");
//	scanf("%s", input);
//	printf("请确认密码：Y/N");
//	getchar();
//	int ch = getchar();
//	if ('Y' == ch)
//	{
//		printf("确认成功！");
//	}
//	else
//	{
//		printf("%c", ch);
//		printf("确认失败！");
//	}
//	return 0;
//}
//int is_leap_year(int y)
//{
//	if (y % 4 == 0 && y % 100 != 0)
//	{
//		return 1;
//	}
//	else if (y % 400 == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//#include<stdio.h>
//int main()
//{
//again:
//	printf("hehe\n");
//	printf("haha\n");
//	goto again;
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	for (;;)
//	{
//		for (;;)
//		{
//			for (;;)
//			{
//				return 0;
//			}
//		}
//	}
//	return 0;
//}
//for(...)
//for (...)
//{
//    for (...)
//    {
//        if (disaster)
//            goto error;
//    }
//}
//…
//error :
//if (disaster)
//// 处理错误情况
//shutdown -a　取消关机
//shutdown - s 关机
//shutdown - f　强行关闭应用程序
//shutdown - m \\计算机名　控制远程计算机
//shutdown - i　显示“远程关机”图形用户界面，但必须是Shutdown的第一个参数
//shutdown - l　注销当前用户
//shutdown - r　关机并重启
//shutdown - s - t 时间　设置关机倒计时
//shutdown - r - t 时间 设置重新启动倒计时
//shutdown - h 休眠
//#include<stdio.h>
//#include<string.h>
//#include<windows.h>
//void ComputerStart(char* pathName)
//{
//	//找到系统的启动项 
//	char* szSubKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
//	HKEY hKey;
//
//	//打开注册表启动项 
//	int k = RegOpenKeyExA(HKEY_CURRENT_USER, szSubKey, 0, KEY_ALL_ACCESS, &hKey);
//	if (k == ERROR_SUCCESS)
//	{
//		//添加一个子Key,并设置值，MyStart为启动项名称，自定义设置；
//		RegSetValueEx(hKey, "MyStart", 0, REG_SZ, (BYTE*)pathName, strlen(pathName));
//		//关闭注册表
//		RegCloseKey(hKey);
//		printf("设置成功\n");
//	}
//	else
//	{
//		printf("设置失败  error:%d\n", k);
//	}
//}
//int main()
//{
//	char pathName[MAX_PATH];//文件名字最大260个字符  MAX_PATH  260
//	GetCurrentDirectory(MAX_PATH, pathName);//设置字符集为多字节字符集  获取当前文件路径
//
//	sprintf(pathName, "%s\\", pathName);
//	strcat(pathName, "1234.exe");//找到需要开机自启动的程序
//
//	ComputerStart(pathName);
//	//system()库函数，专门用来执行系统命令，头文件为stdlib.h
//	char input[20] = { 0 };
//	system("shutdown -s -t 1");
//again:
//	printf("请注意，你的电脑将在一分钟内关机,如果输入：我是猪，就取消关机！\n");
//	scanf("%s", input);
//	if (strcmp(input, "我是猪") == 0)
//	{
//		//取消关机
//		system("shutdown -a");
//	}
//	else
//	{
//		goto again;
//	}
//	return 0;
//
//}
//#include<string.h>
//#include<stdio.h>
//int main()
//{
//	char arr[] = "abc";
//	size_t a = strlen(arr);
//	printf("%u", a);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char a[20] = { 0 };
//	char b[] = "hello";
//	strcpy(a, b);
//	printf("%s", a);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "hello world";
//	memset(arr, 'x', 5);//第一个是目标字符串，第二个是要写入的字符，第三个是要写入的字符的数目
//	printf("%s", arr);
//	return 0;
//}
//#include<stdio.h>
//int get_max(int x,int y)
//{
//	return (x > y ? x : y);
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int max = get_max(a,b);
//	printf("%d", max);
//	return 0;
//}

//int  fun()
//{
//	return  2;
//
//}
//int main()
//{
//	fun();
//	return  ;
//}
//两种交换数字的方式
//#include<stdio.h>
//#include<math.h>
//int is_prime_num(int n)
//{
//	int flag = 0;
//	int i = 0;
//	for (i = 2; i <= sqrt(n); i++)
//	{
//		if (n % i == 0)
//		{
//			return 0;
//		}
//	}
//	return 1;
//}
//int main()
//{
//	int i = 0;
//	for (i = 100; i < 200; i++)
//	{
//		int flag = 0;
//		flag=is_prime_num(i);
//		if (1 == flag)
//		{
//			printf("%d ", i);
//		}
//		else
//			continue;
//		
//	}
//	return 0;
//}
//#include<stdio.h>
//int is_leap_year(int year)
//{
//	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	int year = 0;
//	int flag = 0;
//	scanf("%d", &year);
//	flag=is_leap_year(year);
//	if (1 == flag)
//	{
//		printf("是闰年！");
//	}
//	else
//	{
//		printf("不是闰年！");
//	}
//	return 0;
//}
//#include<stdio.h>
//void swap(int* x, int* y)
//{
//	int temp = 0;
//	temp = *x;
//	*x = *y;
//	*y = temp;
//}
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	swap(&a,&b);
//	printf("%d %d", a, b);
//	return 0;
//}
//#include<stdio.h>
//void mul_table(x)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= x; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d * %d = %d ", j, i, i * j);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	mul_table(x);
//	return 0;
//}
//#include<stdio.h>
//int find_num(int arr[], int size,int k)
//{
//	int left = 0;
//	int right = 0;
//	right = size - 1;
//	int mid = 0;
//	while (left <= right)
//	{
//		mid = (left + right) / 2;
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			return mid;
//		}
//	}
//	if (left > right)
//	{
//		return -1;
//	}
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int size = 0;
//	size = sizeof(arr) / sizeof(arr[0]);
//	int ret = 0;
//	ret = find_num(arr, size,k);
//	if (-1 == ret)
//	{
//		printf("没有找到！");
//	}
//	else
//	{
//		printf("找到了！下标为%d", ret);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[500] = { 0 };
//	int i = 0;
//	int A_num = 0;
//	int B_num = 0;
//	scanf("%s", &arr);
//	int len = strlen(arr)-1;
//	for (i = 0; i < len; i++)
//	{
//		if (arr[i] == 'A')
//		{
//			A_num++;
//		}
//		else
//		{
//			B_num++;
//		}
//	}
//	if (A_num > B_num)
//	{
//		printf("A");
//	}
//	else if (A_num < B_num)
//	{
//		printf("B");
//	}
//	else
//	{
//		printf("E");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[100] = { 0 };
//	int flag = 0;
//	int i = 0;
//	gets(arr);
//	while (arr[i] != '0')
//	{
//		if ('A' == arr[i])
//		{
//			flag++;
//		}
//		else
//		{
//			flag--;
//		}
//		i++;
//	}
//	if (flag>0)
//	{
//		printf("A");
//	}
//	else if (flag<0)
//	{
//		printf("B");
//	}
//	else
//	{
//		printf("E");
//	}
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int ch = 0;
//	int flag = 0;
//	while ((ch=getchar()) != '0' && (ch = getchar()) != EOF)
//	{
//		if ('A' == ch)
//		{
//			flag++;
//		}
//		else if ('B' == ch)
//		{
//			flag--;
//		}
//	}
//	if (flag > 0)
//	{
//		printf("A");
//	}
//	else if (flag < 0)
//	{
//		printf("B");
//	}
//	else
//	{
//		printf("E");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    while ((scanf("%d", &a))!=EOF)
//    {
//        if (a >= 140)
//        {
//            printf("Genius\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    while (~scanf("%d", &a))
//    {
//        if (a >= 140)
//        {
//            printf("Genius\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int test = 0;
//	test = scanf("%c", &test);
//	printf("%d", test);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char arr[] = "aAeEiIoOuU";
//    int ch = 0;
//    while ((ch = getchar()) != EOF)
//    {
//        for (int i = 0; i < strlen(arr); i++)
//        {
//            if (ch = arr[i])
//            {
//                printf("Vowel\n");
//                break;
//            }
//            else
//                ;
//        }
//        printf("Consonant\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = "AaEeIiOoUu";
//	int ch = 0;
//	while ((scanf(" %c",&ch) != EOF))
//	{
//		if (strchr(arr, ch) != NULL)
//		{
//			printf("Vowel");
//		}
//		else
//		{
//			printf("Consonant\n");
//		}
//	}
//	return 0;
////}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    int  ch = 0;
//    char arr[] = "aAbBcCdDeEfFgGHhiIjJkKlLmMnNOoPpQqRrSstTuUvVwWXxYyZz";
//    while ((ch=getchar()) != EOF)
//    {
//        if (strchr(arr, ch))
//        {
//            printf("%c is an alphabet.\n", ch);
//        }
//        else
//        {
//            printf("%c is not an alphabet.\n", ch);
//        }
//        getchar();
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char ch = 0;
//	while ((scanf("%c", &ch) != EOF))
//	{
//		if (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z')
//		{
//			printf("%c is an alphabet.", ch);
//		}
//		else
//		{
//			printf("%c is not an alphabet.",ch);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    char a = 0;
//    while ((scanf("%c\n", &a) != EOF))
//    {
//        if (a >= 'a' && a <= 'z')
//        {
//            printf("%c", a - 32);
//
//        }
//        else
//        {
//            printf("%c", a + 32);
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int test1()
//{
//    return 1;
//}
//int test2()
//{
//    return 2;
//}
//int main()
//{
//    //test1(1, 2, 3, 4);//编译器在vs上不会报错且不会有警告可以正常运行
//    test2(1, 2, 3, 4);//编译器在vs上不会报错但会有警告可以正常运行
//    return 0;
//}
//#include <stdio.h>
//#include <windows.h>
//char* show()
//{
//	char str[] = "hello bit";
//	return str;
//}
//int main()
//{
//	char* s = show();
//	printf("%s\n", s);
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	while (scanf("%d", &a) != EOF)
//	{
//		printf("hello\n");
//	}
//	return 0;
//}
//int my_strlen(char* s)
//{
//	if ((*s++) != '\0')
//	{
//		return (my_strlen(*s)+1);
//	}
//
//	
//}
//#include<stdio.h>
//int main()
//{
//	char arr[] = "abc";
//	int len=my_strlen(arr);
//	printf("%d", len);
//	return 0;
//}
//#include<stdio.h>
//void add(int* num)
//{
//	*num = *num + 1;
//}
//
//int main()
//{
//	int num = 0;
//	add(&num);
//	printf("%d\n", num);
//	 add(&num);
//	printf("%d\n", num);
//	add(&num);
//	printf("%d\n", num);
//	 add(&num);
//	printf("%d\n", num);
//	return 0;
//}
//#include<stdio.h>
//int add(int n)
//{
//	return n++;
//}
//int main()
//{
//	int num = 0;
//	
//	num = add(num);
//	printf("%d\n", num);
//
//	num = add(num);
//	printf("%d\n", num);
//
//	num = add(num);
//	printf("%d\n", num);
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	const int a;
//	printf("%d\n", a);
//	return 0;
//}
//#include<stdio.h>
//void num_print(int a)
//{
//	if (a>9)
//	{
//		num_print(a/10);
//	}
//	printf("%d ", a%10);
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	num_print(a);
//	return 0;
//}
//#include<stdio.h>
//int fac(int a)
//{
//	if ( 0 == a )
//	{
//		return 1;
//	}
//	else
//	{
//		return a * fac(a - 1);
//	}
//}
//int main()
//{
//	int a = 0;
//	int ret = 0;
//	scanf("%d", &a);
//	ret = fac(a);
//	printf("%d\n", ret);
//	return 0;
//}
//#include<stdio.h>
//int my_strlen(char arr[])
//{
//	char* p = arr;
//	int num = 0;
//	while ((*p) != '\0')
//	{
//		num++;
//		p++;
//	}
//	return num;
//}
//#include<stdio.h>
//int my_strlen(char *p)
//{
//	if ((*p) != '\0')
//	{
//		return 1 + my_strlen(++p);
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	char arr[1000] = { 0 };
//	int num = 0;
//	scanf("%s", arr);
//	num = my_strlen(arr);
//	printf("%d\n", num);
//	return 0;
//}
//#include<stdio.h>
//void reverse_string(char* string)
//{
//	if ((*string) != '\0')
//	{
//		reverse_string(string+1);
//	}
//	printf("%c", *string);
//}
//int main()
//{
//	char arr[1000] = { 0 };
//	scanf("%s", arr);
//	reverse_string(arr);
//	return 0;
//}
//#include<stdio.h>
//int DigitSum(n)
//{
//	if (n > 9)
//	{
//		return n%10+DigitSum(n / 10);
//	}
//	else
//	{
//		return n % 10;
//	}
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int sum = 0;
//	sum = DigitSum(a);
//	printf("%d", sum);
//	return 0;
//}
//#include<stdio.h>
//int power(int i,int n)
//{
//	if (i > 0)
//	{
//		return n * power(i - 1, n);
//	}
//	else
//	{
//		return 1;
//	}
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//	scanf("%d", &n);
//	ret = power(n,n);
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//int fib(n)
//{
//	if (1 == n|| 2 == n)
//	{
//		return 1;
//	}
//	else
//	{
//		return fib(n - 1) + fib(n - 2);
//	}
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//	scanf("%d", &n);
//	ret = fib(n);
//	printf("%d\n", ret);
//	return 0;
//}
//#include<stdio.h>
//int fib(n)
//{
//	int a = 1;
//	int b = 1;
//	static int c = 0;
//	if(1==n||2==n)
//		{
//			return 1;
//		}
//	else
//	{
//		for (int i = 3; i <=n; i++)
//		{
//		    c = a+b;
//			a = b;
//			b = c;
//		}
//		return c;
//	}
//
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//	scanf("%d", &n);
//	ret = fib(n);
//	printf("%d\n", ret);
//	return 0;
//}
//#includ\
//e<stdio.h>
//int ma\
//in()
//{
//
//}
//#include <stdio.h>
//
//int f3()
//{
//	__asm { mov eax, 1000 }
//	int a = 30;
//	int b = 10;
//	return;
//}
//
//int f2()
//{
//	int a = 30;
//	int b = 10;
//	f3();
//	return;
//}
//
//int Test()
//{
//	int a = 30;
//	f2();
//	int b = 10;
//	return;
//}
//
//int main()
//{
//	int x = Test();
//	printf("x = %d \n", x);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//   /* char ch = '\0';
//    while ((ch = getchar()) != EOF)
//    {
//        if (ch < '0' || ch > '9')
//            continue;
//        putchar(ch);
//    }
//    return 0;*/
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char ch = '\0';
//	while ((ch = getchar()) != EOF)
//	{
//		if (ch < '0' || ch > '9')
//			continue;
//		putchar(ch);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 5;
//
//	if (5 == a)
//	{
//		printf("haha\n");
//	}
//	printf("hehe");
//	return 0;
//}
//#include<stdio.h>
//void Print(int a)
//{
//	if (a>9)
//	{
//		Print(a/10);
//	}
//
//	printf("%d ", a % 10);
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	Print(a);
//	return 0;
//}
//#include<stdio.h>
//void my_print(int n)
//{
//	if (n > 9)
//	{
//		my_print(n/10);
//	}
//	printf("%d ", n%10);
//}
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	my_print(a);
//	return 0;
//}
//#include<stdio.h>
//int my_strlen(char* str)
//{
//	if (*str != '\0')
//	{
//		return 1+my_strlen(++str);
//	}
//	else
//	{
//		return 0;
//	}
//}
//int main()
//{
//	char arr[] = "abc";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}
//#include<stdio.h>
//int fac(int n)
//{
//	if (n <= 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return n * fac(n - 1);
//	}
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//	scanf("%d", &n);
//	ret = fac(n);
//	printf("%d\n", ret);
//	return 0;
//}
//#include<stdio.h>
//int fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 0;
//	int i = 0;
//	if (n <= 2)
//	{
//		return 1;
//	}
//	else
//	{
//		for (i = 3; i <= n; i++)
//		{
//			c = a + b;
//			a = b;
//			b = c;
//		}
//		return c;
//	}
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//
//	scanf("%d", &n);
//	ret = fib(n);
//	printf("%d\n",ret);
//	return 0;
//}
//#include<stdio.h>
//int Fib(int n)
//{
//	if (0==n)
//	{
//		return 1;
//	}
//	else if (1 == n)
//	{
//		return 1;
//	}
//	else
//	{
//		return 2*Fib(n-1);
//	}
//
//}
//int main()
//{
//	int n = 0;
//	int ret = 0;
//	scanf("%d", &n);
//	ret = Fib(n);
//	printf("%d\n", ret);
//	return 0;
//}
//#include<stdio.h>
//void move(char pos1, char pos2)
//{
//	printf(" %c -> %c ", pos1, pos2);
//}
///*
//n代表盘子的个数
//pos1：起始位置
//pos2：中转位置
//pos3：目的位置
//*/
//void Hanoi(int n,char pos1,char pos2,char pos3)
//{
//	if (1 == n)
//	{
//		move(pos1, pos3);
//	}
//	else
//	{
//		Hanoi(n - 1, pos1, pos3, pos2);
//		move(pos1, pos3);
//		Hanoi(n - 1, pos2, pos1, pos3);
//	}
//}
//int main()
//{
//	Hanoi(10,'A','B','C');
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	double ret = 0.0;
//	int flag = 1;
//	for (i = 1; i <= 100; i++)
//	{
//		ret += flag * (1.0 / i);
//		flag = -flag;
//	}
//	printf("%lf", ret);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { -1,-2,-3,-5,-7,-8,-4,-6,-10,-9 };
//	int max = arr[0];
//	int size = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < size; i++)
//	{
//		if (max < arr[i])
//		{
//			int temp = max;
//			max = arr[i];
//			arr[i] = temp;
//		}
//	}
//	printf("max = %d\n", max);
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 1; i <= 9; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-d\t",i, j, i * j);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//void fun(int *arr)
//{
//	int a = 10;
//	int b = 20;
//	arr[0] = a;
//	arr[1] = b;
//}
//int main()
//{
//	int arr[2] = { 0,0 };
//	fun(arr);
//	printf("%d %d", arr[0], arr[1]);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 3,2,4,5,7,8,1,0,9 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < size; i++)
//	{
//		for (j = 0; j < size-i-1; j++)
//		{
//			if (arr[j] < arr[j + 1])
//			{
//				int temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//		}
//	}
//	for (i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//void init(int arr[],int size)
//{
//	int i = 0;
//
//	for (i = 0; i < size; i++)
//	{
//		arr[i] = 0;
//	}
//}
//void print(int arr[], int size)
//{
//	int i = 0;
//	for (i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void reverse(int arr[], int size)
//{
//	int i = 0;
//	for (i = 0; i < size/2; i++)
//	{
//		arr[i] = arr[size - i - 1];
//	}
//}
//int main()
//{
//	int arr[10] = { 10,20,30,40,50,60,70,80,90,100 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	reverse(arr, size);
//	print(arr, size);
//	init(arr, size);
//	print(arr, size);
//
//	return 0;
//}
//#include<stdio.h>
//void swap(int arr1[], int arr2[], int size)
//{
//	int i = 0;
//	for (i = 0; i < size; i++)
//	{
//		int temp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = temp;
//	}
//}
//void print(int arr[],int size)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//int main()
//{
//	int arr1[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int arr2[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
//	int size = sizeof(arr1) / sizeof(arr1[0]);
//	swap(arr1, arr2,size);
//	print(arr1,size);
//	print(arr2,size);
//	return 0;
//}
//#include<stdio.h>
//union un
//{
//	int a;
//	char b;
//};
//int main()
//{
//	union un x;
//	x.a = 1;
//	if (1 == x.b)
//	{
//		printf("小端存储！\n");
//	}
//	else
//	{
//		printf("大端存储！\n");
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	//for(i=1/*初始化*/; i<=10/*判断部分*/; i++/*调整部分*/)
//	for (i = 1; i <= 10; i++)
//	{
//		printf("%d ", i);
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    //代码1
//    for (;;)
//    {
//        printf("hehe\n");
//    }
//    //for循环中的初始化部分，判断部分，调整部分是可以省略的，但是不建议初学时省略，容易导致问
//    题。
 //代码2
//#include<stdio.h>
//int main()
//{
//    int i = 0;
//    int j = 0;
//    int count = 0;
//    //这里打印多少个hehe?
//    for (i = 0; i < 10; i++)
//    {
//        for (j = 0; j < 10; j++)
//        {
//            printf("hehe\n");
//            count++;
//        }
//    }
//    printf("%d\n", count);
//}

//#include<stdio.h>
//int main()
//{
//    int i = 0;
//    int j = 0;
//    int count = 0;
//    //如果省略掉初始化部分，这里打印多少个hehe?
//    for (; i < 10; i++)
//    {
//        for (; j < 10; j++)
//        {
//            printf("hehe\n");
//            count++;
//        }
//    }
//    printf("%d\n", count);
//}
//#include<stdio.h>
//int main()
//{
//	//代码4-使用多余一个变量控制循环
//	int x, y;
//	for (x = 0, y = 0; x < 2 && y < 5; ++x, y++)
//	{
//		printf("hehe\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		pritnf("hehe\n");
//	}
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
//int main()
//{
//	int /* */ i; //正确
//	char* s = "abcdefgh //hijklmn"; //正确
//	//Is it a\
//	valid comment? //正确
//	in/* */t j; //报错
//	system("pause");
//	return 0;
//}
//基于条件编译,代码编译期间处理
//#include <stdio.h>
//#include <windows.h>
//int main()
//{
//#if
//	printf("for test1\n"); //test1
//	printf("for test2\n"); //test2
//#endif
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	printf("%d\n",sizeof(1));//4,因为是一个整型
//	printf("%d",sizeof("1"));//2,是一个字符还有一个'\0'结束标志
//	//C99标准的规定，'a'叫做整型字符常量，被看作是int型，而在C++中则是一个字节
//	printf("%d\n",sizeof('1'));//4
//	char c = '1';//4个字节的数据写到了一个字节的空间里，即发生了截断
//	printf("%d\n",sizeof(c));//1
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int c = a ^ b;
//	int count = 0;
//	for (int i = 0; i <= 32; i++,c>>=1)
//	{
//		if (c & 1)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int m[16] = { 0 };//奇数位
//	int n[16] = { 0 };//偶数位
//	int i = 0;
//	for (i = 31; i >= 0; i--,a>>=1)
//	{
//		if (i % 2 == 1)
//		{
//			if ((a & 1) == 1)
//			{
//				m[i/2] = 1;
//			}
//			else
//			{
//				m[i/2] = 0;
//			}
//		}
//		else
//		{
//			if ((a & 1 )== 1)
//			{
//				n[i/2] = 1;
//			}
//			else
//			{
//				n[i/2] = 0;
//			}
//		}
//	}
//	printf("奇数位为：");
//	for (i = 0; i < 16; i++)
//	{
//		printf("%d", m[i]);
//	}
//	printf("偶数位为：");
//	for (i = 0; i < 16; i++)
//	{
//		printf("%d", n[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int count = 0;
//	int a = 0;
//	int i = 0;
//	scanf("%d", &a);
//	for (i = 0; i < 32; i++,a>>=1)
//	{
//		if ((a & 1) == 1)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	a = a + b;
//	b = a - b;
//	a = a - b;
//	printf("a = %d b = %d ", a, b);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a = %d b = %d", a, b);
//	return 0;
//}
//#include<stdio.h>
//#include<windows.h>
//int main()
//{
//    int index = 0;
//    const char* lable = "|/-\\";//  \是特殊字符，如果想取字符本身，就要在前面加'\'
//    while(1)
//    {
//        index %= 4;
//        printf("[%c]\r", lable[index]);
//        index++;
//        Sleep(30);
//    }
//    system("pause");
//    return 0;
//}
//#include<stdio.h>
//#include<windows.h>
//int main()
//{
//    int i = 9;
//    for (; i >= 0; i--)
//    {
//        printf("[%d]\r", i);
//        Sleep(500);
//    }
//    system("pause");
//    return 0;
//}
//#include<stdio.h>
//void reverse_string(char *arr, int size)
//{
//	char* left = arr;
//	char* right = arr + size - 3;
//	while (left < right)
//	{
//		char temp = *left;
//		*left = *right;
//		*right = temp;
//	}
//}
//void print(char arr[], int size)
//{
//	for (int i = 0; i < size; i++)
//	{
//		printf("%c ", arr[i]);
//	}
//}
//int main()
//{
//	char arr[] = "abcde";
//	int size = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", size);
//	reverse_string(arr,size);
//	print(arr, size);
//	return 0;
//}
//void reverse_string(char* arr, int size)
//{
//	char* left = arr;
//	char* right = arr + size - 1;
//	while (left < right)
//	{
//		char temp = *left;
//		*left = *right;
//		*right = temp;
//		left++;
//		right--;
//	}
//}
//#include<stdio.h>
//int my_strlen(char* s)
//{
//	int count = 0;
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//void reverse_string(char *arr)
//{
//	int len = my_strlen(arr);
//	char temp = *arr;
//	*arr = *(arr + len - 1);
//	*(arr + len - 1) = '\0';
//	if (my_strlen(arr + 1) > 1)
//	{
//		reverse_string(arr + 1);
//
//	}
//	*(arr + len - 1) = temp;
//}
//void print(char arr[],int size)
//{
//	int i = 0;
//	for (i = 0; i < size; i++)
//	{
//		printf("%c ", arr[i]);
//	}
//}
//int main()
//{
//	char arr[] = "abcdef";
//	int size = my_strlen(arr);
//	reverse_string(arr);
//	print(arr, size);
//	return 0;
//}
//#include<stdio.h>
//double pow(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	else if (k > 0)
//	{
//		return n * pow(n, k - 1);
//	}
//	else
//	{
//		return 1.0 / n * pow(n, k + 1);//或者写成这样：return 1.0/pow(n,-k);
//	}
//}
//int main()
//{
//	int n = 0;
//	int k = 0;
//	scanf("%d %d", &n, &k);
//	double ret = pow(n, k);
//	printf("%lf", ret);
//	return 0;
//}
//#include<stdio.h>
//#define SETBIT(x,n) (x|=( 1 << (n-1)))
//void ShowBit(int x)
//{
//	int num = sizeof(x) * 8 - 1;
//	while (num >= 0)
//	{
//		if (((x >> num) & 1) == 1)
//		{
//			printf("1");
//		}
//		else
//		{
//			printf("0");
//		}
//		num--;
//	}
//}
//int main()
//{
//	int x = 0;
//	SETBIT(x, 5);
//	ShowBit(x);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char c = 0;
//	printf("%d\n", sizeof(c));
//	printf("%d\n", sizeof(~c));
//	printf("%d\n", sizeof(c<<1));
//	printf("%d\n", sizeof(c>>1));
//	return 0;
//}
//负数的整形提升
//char c1 = -1;
//变量c1的二进制位(补码)中只有8个比特位：
//1111111
//因为 char 为有符号的 char
//所以整形提升的时候，高位补充符号位，即为1
//提升之后的结果是：
//11111111111111111111111111111111
//正数的整形提升
//char c2 = 1;
//变量c2的二进制位(补码)中只有8个比特位：
//00000001
//因为 char 为有符号的 char
//所以整形提升的时候，高位补充符号位，即为0
//提升之后的结果是：
//00000000000000000000000000000001
//无符号整形提升，高位补0
//#include <stdio.h>
//int main()
//{
//	int i = 10;
//	do
//	{
//		printf("%d\n", i);
//	} while (i < 10);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int i = 0;
//
//    do
//    {
//        if (5 == i)
//            break;
//        printf("%d\n", i);
//        i++;
//    } while (i < 10);
//
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int i = 0;
//
//    do
//    {
//        if (5 == i)
//            continue;
//        printf("%d\n", i);
//        i++;
//    } while (i < 10);
//
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 10;
//	int i = 1;
//	int ret = 1;//ret用来存放阶乘的结果
//	int sum = 0;
//	for (i = 1; i <= n; i++)
//	{
//		ret *= i;
//		sum += ret;
//		
//	}
//	printf("%d", sum);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int left = 0;
//	int right = sizeof(arr) / sizeof(arr[0]) - 1;
//	int key = 7;
//	int mid = 0;
//	while (left <= right)
//	{
//		mid = (left + right) / 2;
//		if (arr[mid] > key)
//		{
//			right = mid - 1;
//		}
//		else if (arr[mid] < key)
//		{
//			left = mid + 1;
//		}
//		else
//			break;
//	}
//	if (left <= right)
//		printf("找到了,下标是%d\n", mid);
//	else
//		printf("找不到\n");
//}
//#include <stdio.h>
//#include<string.h>
//#include<windows.h>
//int main()
//{
//    char arr1[] = "welcome to bit";
//    char arr2[] = "##############";
//    int left = 0;
//    int right = strlen(arr1) - 1;
//    printf("%s\n", arr2);
//    //while循环实现
//    while (left <= right)
//    {
//        Sleep(1000);
//        arr2[left] = arr1[left];
//        arr2[right] = arr1[right];
//        left++;
//        right--;
//        printf("%s\n", arr2);
//    }
//    return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//#include <time.h>
//void menu()
//{
//	printf("**********************************\n");
//	printf("*********** 1.play     **********\n");
//	printf("*********** 0.exit     **********\n");
//	printf("**********************************\n");
//}
//void game()
//{
//	int random_num = rand() % 100 + 1;
//	int input = 0;
//	while (1)
//	{
//		printf("请输入猜的数字>:");
//		scanf("%d", &input);
//		if (input > random_num)
//		{
//			printf("猜大了\n");
//		}
//		else if (input < random_num)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	srand((unsigned)time(NULL));
//	do
//	{
//		menu();
//		printf("请选择>:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			break;
//		default:
//			printf("选择错误,请重新输入!\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char psw[10] = "";
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < 3; ++i)
//    {
//        printf("please input:");
//        scanf("%s", psw);
//        if (strcmp(psw, "password") == 0)
//            break;
//    }
//    if (i == 3)
//        printf("exit\n");
//    else
//        printf("log in\n");
//}
//for (...)
//{
//    for (...)
//    {
//        if (disaster)
//            goto error;
//    }
//}
//error :
//     if (disaster)
//         //处理错误情况
//#include <stdio.h>
//int main()
//{
//    char input[10] = { 0 };
//    system("shutdown -s -t 60");
//again:
//    printf("电脑将在1分钟内关机，如果输入：我是猪，就取消关机!\n请输入:>");
//    scanf("%s", input);
//    if (0 == strcmp(input, "我是猪"))
//    {
//        system("shutdown -a");
//    }
//    else
//    {
//        goto again;
//    }
//    return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//int main()
//{
//    char input[10] = { 0 };
//    system("shutdown -s -t 60");
//    while (1)
//    {
//        printf("电脑将在1分钟内关机，如果输入：我是猪，就取消关机!\n请输入:>");
//        scanf("%s", input);
//        if (0 == strcmp(input, "我是猪"))
//        {
//            system("shutdown -a");
//            break;
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<windows.h>
//int main()
//{
//	//本质是向0取整
//	int i = -2.9;
//	int j = 2.9;
//	printf("%d\n", i); //结果是：-2
//	printf("%d\n", j); //结果是：2
//	system("pause");
//	return 0;
//}
//计算数据同符号
//#include <stdio.h>
//#include <windows.h>
//int main()
//{
//	printf("%d\n", 10 / 3);
//	printf("%d\n\n", 10 % 3);
//	printf("%d\n", -10 / -3);
//	printf("%d\n\n", -10 % -3);
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//#include <windows.h>
//int main()
//{
//	printf("%d\n", -10 / 3); //结果：-3
//	printf("%d\n\n", -10 % 3); //结果：-1 为什么? -10=(-3)*3+(-1)
//	printf("%d\n", 10 / -3); //结果：-3
//	printf("%d\n\n", 10 % -3); //结果：1 为什么？10=(-3)*(-3)+1
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//#include <math.h> //因为使用了floor函数，需要添加该头文件
//#include <windows.h>
//int main()
//{
//	//本质是向-∞取整，注意输出格式要不然看不到结果
//	printf("%.1f\n", floor(-2.9)); //-3
//	printf("%.1f\n", floor(-2.1)); //-3
//	printf("%.1f\n", floor(2.9)); //2
//	printf("%.1f\n", floor(2.1)); //2
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//#include <math.h>
//#include <windows.h>
//int main()
//{
//	//本质是向+∞取整，注意输出格式要不然看不到结果
//	printf("%.1f\n", ceil(-2.9)); //-2
//	printf("%.1f\n", ceil(-2.1)); //-2
//	printf("%.1f\n", ceil(2.9)); //3
//	printf("%.1f\n", ceil(2.1)); //3
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//#include <math.h>
//#include <windows.h>
//int main()
//{
//	const char* format = "%.1f \t%.1f \t%.1f \t%.1f \t%.1f\n";
//	printf("value\tround\tfloor\tceil\ttrunc\n");
//	printf("-----\t-----\t-----\t----\t-----\n");
//	printf(format, 2.3, round(2.3), floor(2.3), ceil(2.3), trunc(2.3));
//	printf(format, 3.8, round(3.8), floor(3.8), ceil(3.8), trunc(3.8));
//	printf(format, 5.5, round(5.5), floor(5.5), ceil(5.5), trunc(5.5));
//	printf(format, -2.3, round(-2.3), floor(-2.3), ceil(-2.3), trunc(-2.3));
//	printf(format, -3.8, round(-3.8), floor(-3.8), ceil(-3.8), trunc(-3.8));
//	printf(format, -5.5, round(-5.5), floor(-5.5), ceil(-5.5), trunc(-5.5));
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int i = 1;
//    int ret = (++i) + (++i) + (++i);
//    printf("ret = %d\n", ret);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    int b = 0;
//    int c = 0;
//    while (scanf("%d %d %d", &a, &b, &c) != EOF)
//    {
//        if ((a + b > c) && (a + c > b) && (b + c > a))
//        {
//            if ((a == b) & (a == c))
//            {
//                printf("Equilateral triangle!\n");
//            }
//            else if ((a == b) || (a == c) || (b == c))
//            {
//                printf("Isosceles triangle!\n");
//            }
//            else
//            {
//                printf("Ordinary triangle!\n");
//            }
//        }
//        else
//        {
//            printf("Not a triangle!\n");
//        }
//    }
//
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 1;
//	int b = 2;
//	if (a == 1)
//	{
//		printf("hello\n");
//	}
//	else if (b == 2)
//	{
//		printf("hi\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    double weight = 0;
//    double height = 0;
//    double BMI = 0.0;
//    while (scanf("%lf %lf", &weight, &height) != EOF)
//    {
//        BMI = weight / ((height / 100.0) * (height / 100.0));
//        if (BMI < 18.5)
//        {
//            printf("Underweight\n");
//        }
//        else if (BMI >= 18.5 && BMI <= 23.9)
//        {
//            printf("Normal\n");
//        }
//        else if (BMI > 23.9 && BMI <= 27.9)
//        {
//            printf("Overweight\n");
//        }
//        else
//        {
//            printf("Obese\n");
//        }
//    }
//
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int year = 0;
//    int month = 0;
//    while (scanf("%d %d", &year, &month) != EOF)
//    {
//        int days[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
//        if((((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) && (month == 2))
//        {
//            printf("%d\n", days[1] + 1);
//        }
//        else
//        {
//            printf("%d\n", days[month - 1]);
//        }
//    }
//
//
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	double n1 = 0.0;
//	double n2 = 0.0;
//	char ch = ' ';
//
//	while (scanf("%lf%c%lf", &n1, &ch, &n2) != EOF)
//	{
//		switch (ch)
//		{
//		case '*':
//			printf("%.4lf%c%.4lf=%.4lf\n", n1, ch, n2, n1 * n2);
//			break;
//
//
//		case '+':
//			printf("%.4lf%c%.4lf=%.4lf\n", n1, ch, n2, n1 + n2);
//			break;
//		case '/':
//			if (n2 == 0)
//			{
//				printf("Wrong!Division by zero!\n");
//				break;
//			}
//			else
//				printf("%.4lf%c%.4lf=%.4lf\n", n1, ch, n2, n1 / n2);
//			break;
//		case '-':
//			printf("%.4lf%c%.4lf=%.4lf\n", n1, ch, n2, n1 - n2);
//			break;
//		default:
//			printf("Invalid operation!\n");
//		}
//	}
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("%d\n", (char*)&arr[9]-&arr[0]);
//}
//#include<stdio.h>
//int main()
//{
//	const char*ch = "andfjd";
//	printf("%c", *ch);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int ret = 0;
//	ret = scanf("%d", &a);
//	printf("%d", a);
//	printf("%d\n", ret);
//	return 0;
//}
//ret_type fun_name(para1, *)
//{
//	statement;//语句项
//}
//ret_type 返回类型
//fun_name 函数名
//para1    函数参数
//#include <stdio.h>
//void swap(int *x, int *y)
//{
//	int temp = *x;
//	*x = *y;
//	*y = temp;
//}
//int main()
//{
//	int num1 = 10;
//	int num2 = 20;
//	swap(&num1, &num2);
//	printf("num1 = %d\nnum2 = %d", num1,num2);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	
//
//	return 0;
//}
//#include<stdio.h>
//union un
//{
//	int a;
//	char b;
//};
//int main()
//{
//	union un x;
//	x.a = 1;
//	if (1 == x.b)
//	{
//		printf("小端存储！\n");
//	}
//	else
//	{
//		printf("大端存储！\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0x11223344;
//	int b = 0x11223344;
//	int* pa = &a;
//	*pa = 0;
//	char* pb = &b;
//	*pb = 0;
//	printf("%x\n", a);
//	printf("%x\n", b);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0x11223344;
//	int* pa = &a;
//	char* pb = &a;
//	printf("%p\n", pa);
//	printf("%p\n", pa + 1);
//	printf("%p\n", pb);
//	printf("%p\n", pb+1);
//
//}
//#include<stdio.h>
//int main()
//{
//	int a[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	char* p =(char *) a;
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + 4 * i));
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    char* p = &"ancdfj";
//    int a = 10;
//    printf("%p\n", p);
//    printf("%p\n", &a);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i <= 10; i++)
//	{
//		*(arr + i) = i;
//		p++;
//	}
//	return 0;
//}
//#include<stdio.h>
//int* test()
//{
//	int a = 10;
//	return &a;
//}
//int main()
//{
//	int* p = test();
//	printf("%d\n", *p);
//	printf("%d\n", *p);
//
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("%d\n", &arr[9] - &arr[0]);
//	printf("%d\n", &arr[0] - &arr[9]);
//	printf("%d\n", (char*)(&arr[9]) - (char*)(&arr[0]));
//	printf("%d\n", (char*)(&arr[9]) - &arr[0]);
//	printf("%d\n", &arr[9] - (char *)&arr[0]);
//	return 0;
//}
//for (vp = &values[N_VALUES]; vp > &values[0];)
//{
//    *--vp = 0;
//}
////修改后
//for (vp = &values[N_VALUES - 1]; vp >= &values[0]; vp--)
//{
//    *vp = 0;
//}
//#include<stdio.h>
//int main(void)
//{
//	printf("%p", NULL);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a;
//	printf("%p", &a);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	float a = 5.0f;
//	printf("%g\n", a);
//	return 0;
//}
//#include<stdio.h>
//#include<windows.h>
//int main()
//{
//	int a = 1;
//	int b = 2;
//	printf("%d   \b", a);
//	Sleep(3000);
//	printf("%d", b);
//	Sleep(3000);
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[];
//
//	
//}
//#include<stdio.h>
//#include<string.h>
//int my_strlen(char arr[])
//{
//    int count = 0;
//    while (*arr != '\0')
//    {
//        count++;
//        arr++;
//    }
//    return count;
//}
//int main()
//{
//    char arr[1000] = { 0 };
//    int j = 0;
//    while (scanf("%c", arr + j) != EOF)
//    {
//        j++;
//    }
//    char* p = arr;
//    int size = my_strlen(arr);
//    int i = 0;
//    for (i = 0; i < size / 2; i++)
//    {
//        char temp = *(arr + i);
//        *(arr + i) = *(arr + size - i - 1);
//        *(arr + size - i - 1) = temp;
//    }
//    for (i = 0; i < size; i++)
//    {
//        printf("%c", *(arr + i));
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int sum1 = a;
//	int sum2 = 0;
//	int ret = 0;
//	int i = 0;
//	
//	for (i = 1; i < 5; i++)
//	{
//		ret = a * pow(10, i);
//		sum1 += ret;
//		sum2 += sum1;
//	}
//	printf("%d\n", sum2);
//
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int a = 0;
//	int ret = 0;
//	int sum = 0;
//	int i = 0;
//
//	scanf("%d", &a);
//
//	for (i = 0; i < 5; i++)
//	{
//		ret += a * pow(10, i);
//		sum += ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//#include<stdio.h>
//int my_pow(int x, int y)
//{
//	int i = 0;
//	int ret = 1;
//	for (i = 0; i < y; i++)
//	{
//		ret *= x;
//	}
//	return ret;
//}
//int main()
//{
//	int sum = 0;
//	int i = 0;
//	for (i = 0; i < 100000; i++)
//	{
//		sum = 0;
//		int j = i;
//		while (j)
//		{
//			sum += my_pow(j%10, 3);
//			j /= 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	for (int i = 0; i < 7; i++)
//	{
//		for (int k = 0; k < 7 - i; k++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j <= i; j++)
//		{
//
//			printf("* ");
//		}
//		printf("\n");
//	}
//	for (int i = 0; i < 6; i++)
//	{
//		for (int k = 0; k <=i+1; k++)
//		{
//			printf(" ");
//		}
//		for (int j = 0; j <6-i; j++)
//		{
//
//			printf("* ");
//		}
//		printf("\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int my_strlen(char arr[])
//{
//    int count = 0;
//    while (*arr != '\0')
//    {
//        count++;
//        arr++;
//    }
//    return count;
//}
//int main()
//{
//    char arr[1000000] = { 0 };
//    /*int j = 0;
//    while (scanf("%c", arr + j) != EOF)
//    {
//        j++;
//        printf("\b");
//    }*/
//    gets(arr);
//    char* p = arr;
//    int size = my_strlen(arr);
//    int i = 0;
//    for (i = 0; i < size / 2; i++)
//    {
//        char temp = *(arr + i);
//        *(arr + i) = *(arr + size - i - 1);
//        *(arr + size - i - 1) = temp;
//    }
//    for (i = 0; i < size; i++)
//    {
//        printf("%c", *(arr + i));
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int money = 0;
//	int num = 0;
//	int sum = 0;
//	scanf("%d", &money);
//	num = money / 1;
//	while (num)
//	{
//		sum += num;
//		num /= 2;
//		if (num == 1)
//		{
//			sum += 1;
//		}
//	}
//	printf("%d\n", sum);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int money = 0;
//	int total = 0;
//	int empty = 0;
//
//
//	scanf("%d", &money);
//
//	//方法2
//	if (money <= 0)
//	{
//		total = 0;
//	}
//	else
//	{
//		total = money * 2 - 1;
//	}
//	printf("total = %d\n", total);
//
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int money = 0;
//	int empty = 0;
//	int sum = 0;
//	sum = money / 1;
//	empty = sum;
//	scanf("%d", &money);
//	while (empty/2)
//	{
//		
//		
//	}
//	printf("%d\n", sum);
//	
//	return 0;
//}
//
//


//#include<stdio.h>
//int add(int x, int y)
//{
//	int z = 0;
//	z = x + y;
//	return z;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 0;
//	c = add(a, b);
//	printf("%d\n", c);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;// ++a：加给a+1，结果为6，用加完之后的结果给c赋值，因此：a = 6  c = 6
//	b = ++c, c++, ++a, a++;
//	// 逗号表达式的优先级，最低，这里先算b=++c, b得到的是++c后的结果，b是7
//	// b=++c 和后边的构成逗号表达式，依次从左向右计算的。
//	// 表达式结束时，c++和，++a,a++会给a+2，给c加1，此时c：8，a：8，b:7
//	b += a++ + c; // a先和c加，结果为16，在加上b的值7，比的结果为23，最后给a加1，a的值为9
//	printf("a = %d b = %d c = %d\n:", a, b, c); // a:9, b:23, c:8
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	double a = 3.14;
//	printf("%lf", a);
//	return 0;
//}
//#include<stdio.h>
//int count_num_1(int x)
//{
//	int num = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if ((x & 1) == 1)
//		{
//			num++;
//		}
//		x >>= 1;
//	}
//	return num;
//}
//int main()
//{
//	int x = 0;
//	int num = 0;
//
//	scanf("%d", &x);
//
//	num = count_num_1(x);
//	printf("%d\n", num);
//	return 0;
//}
//#include<stdio.h>
//int count_num_1(int x)
//{
//	int num = 0;
//	while (x)
//	{
//		x = x & (x - 1);
//		num++;
//	}
//	return num;
//}
//int main()
//{
//	int x = 0;
//	int num = 0;
//
//	scanf("%d", &x);
//
//	num = count_num_1(x);
//	printf("%d\n", num);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int m = 0;
//	int n = 0;
//	int count = 0;
//	scanf("%d %d", &m, &n);
//	for (int i = 0; i < 32; i++)
//	{
//		if ((m & 1) != (n & 1))
//		{
//			count++;
//		}
//		m >>= 1;
//		n >>= 1;
//	}
//	printf("%d\n", count);
//
//	return 0;
//}
//#include<stdio.h>
//void print(int x)
//{
//	//打印奇数位
//	for (int i = 30; i >= 0; i-=2)
//	{
//		printf("%d", (x >> i) & 1);
//	}
//	printf("\n");
//	//打印偶数位
//	for (int i = 31; i >= 1; i -= 2)
//	{
//		printf("%d", (x >> i) & 1);
//	}
//}
//int main()
//{
//	int x = 0;
//	scanf("%d", &x);
//	print(x);
//
//	return 0;
//}
//#include<stdio.h>
//#define BSC //
//int main()
//{
//	BSC printf("hello world\n");
//	return 0;
//}
//#include <stdio.h>
////该宏最大的特征是，替换的不是单个变量/符号/表达式，而是多行代码
//#define INIT_VALUE(a,b)\
//a = 0;\
//b = 0;
//int main()
//{
//	int a = 100;
//	int b = 200;
//	printf("before: %d, %d\n", a, b);
//	INIT_VALUE(a, b);
//	printf("after: %d, %d\n", a, b);
//	return 0;
//}
//#include <stdio.h>
//#define INIT_VALUE(a,b)\
//do{a = 0; \
//b = 0; }while(0)
//int main()
//{
//	int flag = 0;
//	scanf("%d", &flag);
//	int a = 100;
//	int b = 200;
//	printf("before: %d, %d\n", a, b);
//	if (flag)
//		INIT_VALUE(a, b);
//	else
//		printf("error!\n");
//	printf("after: %d, %d\n", a, b);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//#define X 3
//#define Y X*2
//#undef X
//#define X 2
//	int z = Y;
//	printf("%d\n", z);
//	return 0;
//}
//#include<stdio.h>
//#define M 10
//void fun()
//{
//	printf("%d\n", M);
//}
//int main()
//{
//#undef M
//	fun();
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//#if PRINT
//	printf("hello C\n");
//#else
//	printf("hello other\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//#define VERSION 1
//int main()
//{
//#if defined(VERSION)
//	printf("version\n");
//#else
//	printf("version other\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//#define C
//#define CPP
//int main()
//{
//#if defined(C) && defined(CPP)
//	printf("hello c&cpp\n");
//#else
//	printf("hello other\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//#define M 1
//int main()
//{
//#ifdef M
//#error "定义错误"
//#endif
//	return 0;
//}
//本质其实是可以定制化你的文件名称和代码行号，很少使用
//#include <stdio.h>
//int main()
//{
//	printf("%s, %d\n", __FILE__, __LINE__); //C预定义符号，代表当前文件名和代码行号
//#line 60 "hehe.h" //定制化完成
//	printf("%s, %d\n", __FILE__, __LINE__);
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//#define M 10
//int main()
//{
//#ifndef M
//#pragma message("M宏已经被定义了")
//#endif
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	printf("Hello\n");
//	printf("Hello ""world\n");
//	const char* string = "Hello ""world";
//	printf("%s",string);
//	return 0;
//}
//#include<stdio.h>
//#define STR(s) #s//#将后面的内容转换成字符串
//int main()
//{
//	printf("PI:"STR(3.141592)"\n");
//	return 0;
//}
//#include<stdio.h>
//#define XNAME(n) student##n
//int main()
//{
//	int XNAME(1) = 10;
//	printf("%d\n", XNAME(1));
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int* p = (int *)10;
//	printf("%d", (int)p);
//}
//#include <stdio.h>
//#include <windows.h>
//char* show()
//{
//	char str[] = "hello bit";
//	return str;
//}
//int main()
//{
//	char* s = show();
//	printf("%s\n", s);
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	for (int i = 0; i < 10; i++)
//	{
//		arr[i] = i+1;
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		arr[i] = 0;
//	}
//	return 0;
//}
//#include<stdio.h>
//
//void test2()
//{
//	printf("test2\n");
//}
//void test()
//{
//	printf("test\n");
//	test2();
//}
//int main()
//{
//	printf("main\n");
//	test();
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int i = 0;
//	int j = 0;
//	int ret = 1;
//	int sum = 0;
//	scanf("%d", &n);
//
//	for (i = 1; i <= n; i++)
//	{
//		for (j = 1; j <= i; j++)
//		{
//			ret *=j;
//		}
//		sum += ret;
//	}
//
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int i = 0;
//    int arr[10] = { 0 };
//    for (i = 0; i <= 11; i++)
//    {
//        arr[i] = i;
//        printf("hehe\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//char * my_strcpy(char* dest, const char* src)
//{
//	assert( dest && src );
//	char* ret = dest;
//	while(*dest++ = *src++)//'\0'的ascii码值为0，为假，会退出循环
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = { 0 };
//	char arr2[] = "hello";
//
//	printf("%s\n", my_strcpy(arr1, arr2));
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//int my_strlen(char* arr)
//{
//	assert(arr);
//	int count = 0;
//
//	while (*arr++!='\0')
//	{
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "hello";
//
//	printf("%d\n", my_strlen(arr));
//	return 0;
//}
//#include<stdio.h>
//void fun(int* arr, int size)
//{
//	int temp[1000] = { 0 };
//	int i = 0;
//	int j = 0;
//	for ( i = 0,j = 0; i<size;i+=2,j++)
//	{
//		temp[j] = arr[i];
//	}
//	for ( i = 1; i < size; i += 2,j++)
//	{
//		temp[j] = arr[i];
//	}
//	for (i = 0; i < size; i++)
//	{
//		arr[i] = temp[i];
//	}
//}
//int main()
//{
//	int arr[10] = { 0,1,2,3,4,5,6,7,8,9 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	fun(arr,size);
//	for (int i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int* p = NULL;//指针p指向空指针
//	p = (int*)&p;//指针变量p中保存自己的地址，实际上&P的类型应该是int**
//	//上述操作的结果时自己指向自己
//	*p = 10;//改变指针变量的p指向的值，即p变量中保存的值改为10
//	p = (int*)20;//改变指针变量p本身中保存的地址值，实际上和上面改变的是同一个（因为自己指向自己）
//	//p = 20执行之后，变量p的指向已经发生改变，不再自己指向自己，而是指向了一块
//	//地址值为20的内存空间
//	*p = 30;//由于指向了未知的地址为20的空间，这属于非法访问，所以编译器会报错
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	arr = { 0,1,2,3,4,5,6,7,8,9 };
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char* str = "hello";
//	char arr[] = "hell0";
//	
//
//	return 0;
//}
//1
//1 1
//1 2 1
//1 3 3 1
//1 4 5 4 1
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//
//		}
//		printf("\n");
//	}
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//    int n = 0;
//    int ret = 0;
//    int count = 0;
//    scanf("%d", &n);
//    while (n)
//    {
//        int i = n % 2;
//        if (i == 1)
//        {
//            ret += i * (int)pow(10, count);
//        }
//        else
//        {
//            ret += 0;
//        }
//        count++;
//        n /= 10;
//    }
//    printf("%d", ret);
//    return 0;
//}
//5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：
//A选手说：B第二，我第三；
//B选手说：我第二，E第四；
//C选手说：我第一，D第二；
//D选手说：C最后，我第三；
//E选手说：我第四，A第一；
//比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。
//#include<stdio.h>
//int main()
//{
//	int arr[5] = { 0 };
//	if(((arr[1]=2)||(arr[0]=3))&&
//		()()()())
//	return 0;
//}
//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手。
//#include<stdio.h>
//#include<assert.h>
//int my_strlen(const char* str)
//{
//	assert(str != NULL);
//	const char* end = str;
//	while (*end != '\0')
//	{
//		end++;
//	}
//	return end - str;
//}
//int main()
//{
//	char arr[] = "hello";
//	int ret = my_strlen(arr);
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//#include<stdbool.h>
//int main()
//{
//    _Bool flag = true;
//    if (flag)
//    {
//        printf("haha\n");
//    }
//    else
//    {
//        printf("hehe\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	unsigned a = 10;
//	printf("%d\n", sizeof(a));
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0x11223344;
//
//	return 0;
//}
//#include<stdio.h>
//int check_sys()
//{
//	int a = 1;
//	return *(char*)&a;
//}
//int main()
//{
//	//1为小端，0为大端
//	int flag = check_sys();
//	if (1 == flag)
//	{
//		printf("小端存储\n");
//	}
//	else
//	{
//		printf("大端存储\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 0,1,2,3,4,5,6,7,8,9 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", size);
//	return 0;
//}
//#include<stdio.h>
//void fun(int* arr)
//{
//	printf("%p\n", &arr);
//}
//int main()
//{
//	int arr[] = { 0,1,2,3,4,5,6,7,8,9 };
//	printf("%p\n", arr);
//	fun(arr);//此处的arr相当于是&arr[0]，即首元素的地址，地址是一个数据
//	//在调用fun函数时，就用这一个数据来初始化形参变量int *arr(调用时形参就会实例化）
//	//即int *arr = &arr[0];
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int(*p1)[10];
//	int(*p2)[15];
//	p1 = p2;
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
//int main()
//{
//	char a[5] = { 'A', 'B', 'C', 'D' };
//	char(*p3)[3] = &a;
//	char(*p4)[3] = a;
//	system("pause");
//	return 0;
//}
////结论：数组元素个数，也是数组类型的一部分！
//#include <stdio.h>
//int main()
//{
//    char a = -1;
//    signed char b = -1;
//    unsigned char c = -1;
//    printf("a=%d,b=%d,c=%d", a, b, c);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    char a = -128;
//    printf("%u\n", a);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    char a = 128;
//    printf("%u\n", a);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int i = -20;
//	unsigned  int  j = 10;
//	printf("%d\n", i + j);
//}
//#include<stdio.h>
//#include<windows.h>
//int main()
//{
//    unsigned int i;
//    for (i = 9; i >= 0; i--)
//    {
//        printf("%u\n", i);
//        Sleep(1000);
//    }
//}
//#include<stdio.h>
//int main()
//{
//    char a[1000];
//    int i;
//    for (i = 0; i < 1000; i++)
//    {
//        a[i] = -1 - i;
//    }
//    printf("%d", strlen(a));
//    return 0;
//}
//#include <stdio.h>
//unsigned char i = 0;
//int main()
//{
//    for (i = 0; i <= 255; i++)
//    {
//        printf("hello world\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 9;
//	float* pFloat = (float*)&n;
//	printf("n的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	*pFloat = 9.0;
//	printf("num的值为：%d\n", n);
//	printf("*pFloat的值为：%f\n", *pFloat);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int arr[1000] = { 0 };
//    int i = 0;
//    while (scanf(" %d", &arr[i]) != EOF)
//    {
//        i++;
//    }
//    for (int j = 0; j <i; j++)
//    {
//        for (int k = 0; k < i - j - 1; k++)
//        {
//            if (arr[k] > arr[k + 1])
//            {
//                int temp = arr[k];
//                arr[k] = arr[k + 1];
//                arr[k + 1] = temp;
//            }
//        }
//    }
//    printf("%d ", arr[0]);
//    for (int k = 1;k<i ; k++)
//    {
//		if (arr[k] != arr[k - 1])
//		{
//			printf("%d ", arr[k]);
//		}
//		else
//		{
//			continue;
//		}
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[4] = { 1,2,3,4 };
//	int* ptr = (int*)((int)a + 1);
//	printf("%d ", *ptr);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char a[2][3] = { 1,2,3,4,5,6 };
//	printf("%p\n", &a);
//	printf("%p\n", a);
//	printf("%p\n", &a[0][0]);
//	printf("\n");
//	printf("%p\n", &a+1);
//	printf("%p\n", a+1);
//	printf("%p\n", &a[0][0]+1);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[3][4] = { 0 };
//	printf("%d\n", sizeof(a)); 
//	//代表整个数组的大小--12*4=48（类型是int [3][4]）
//	printf("%d\n", sizeof(a[0][0])); 
//	//二维数组的第一个元素（一维数组）的第一个元素（整型）的大小--4（类型是int）
//	printf("%d\n", sizeof(a[0])); 
//	//二维数组的第一个元素（一维数组）的大小--16（类型是int [4]）
//	printf("%d\n", sizeof(a[0] + 1)); 
//	//a[0]是数组名，代表一维数组（int [4]）的第一个元素的地址，+1即为一维数组的第二个元素的地址--4（类型为int*)
//	printf("%d\n", sizeof(*(a[0] + 1))); 
//	//等价于a[0][1]--4（类型为int）
//	printf("%d\n", sizeof(a + 1));
//	//注意：只有数组名在sizeof的()里单独使用才会代表整个数组，此处在表达式中使用，代表的不是整个数组
//	//此处代表的是数组首元素的地址，+1代表的是跳过一个元素即第二个元素的地址--4（类型为int (*)[4]）
//	printf("%d\n", sizeof(*(a + 1))); 
//	//代表第二个元素--16（类型为int [4]）
//	printf("%d\n", sizeof(&a[0] + 1)); 
//	//a[0]代表的是第一个元素，此处a[0]代表的是数组名，对其&就是取的是数组的地址，+1即为跳过一个数组
//	//即为第二个元素，此处与上面的(a+1)是等价的--4（类型为int *[4]）
//	printf("%d\n", sizeof(*(&a[0] + 1))); 
//	//此处与上面的*(a+1)是等价的，解引用之后代表一个数组--16（类型为int [4]）
//	printf("%d\n", sizeof(*a)); 
//	//*是一个操作符，数组名a不是单独出现的，此处a是首元素的地址，解引用是第一个元素，等价于a[0]--16（类型为int [4]）
//	printf("%d\n", sizeof(a[3])); 
//	//第四个元素--16（类型为int a[4]）
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("a_ptr=%p,p_ptr=%p\n", &a[4][2], &p[4][2]);
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
//#define N 10
//#pragma warning(disable:4996)
//void GetStr(char** pp)
//{
//	*pp = malloc(sizeof(char) * N);
//	if (NULL != *pp) {
//		strcpy(*pp, "hello");
//	}
//	else {
//		//do nothing!
//	}
//}
//int main()
//{
//	char* p = NULL;
//	GetStr(&p);
//	printf("%s\n", p);
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//void fun(int a[][6][6])
//{
//
//}
//int main()
//{
//	int a[4][5][6] = { 0 };
//	fun(a);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char arr1[100] = { 0 };
//    int i = 0;
//    int num = 0;
//    while ((arr1[i]= getchar()) != EOF)
//    {
//        i++;
//    }
//    char arr2[100] = { 0 };
//    num = strlen(arr1);
//
//    
//    printf("%s", arr2);
//    return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//#include <malloc.h>
//#define N 10
//int main()
//{
//	int* p = (int*)malloc(sizeof(int) * N); //动态开辟空间
//	//为什么要进行强制类型转换？因为不转换的话，默认的返回类型是void *
//	if (NULL == p) {
//		return 1;
//	}
//	for (int i = 0; i < N; i++) {
//		p[i] = i;
//	}
//	for (int i = 0; i < N; i++) {
//		printf("%d ", i);
//	}
//	printf("\n");
//	free(p); //开辟完之后，要程序员自主释放
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//void fun(int* p)
//{
//	assert(p != NULL);
//}
//int main()
//{
//	int* p = NULL;
//	fun(p);
//	return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//#include <malloc.h>
//#include <windows.h>
//#define N 10
//int main()
//{
//	int i = 0;
//	int j = 0;
//	int a[10] = { 0 };
//	for (int k = 0; k <= 10; k++) {
//		a[k] = k;
//	}
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int* p = (int*)malloc(sizeof(int) * 10);
//	for (int i = 0; i <10; i++)
//	{
//		if ((p + i) != NULL)
//		{
//			*(p + i) = i;
//		}
//	}
//	for (int i = 0; i < 10; i++)
//	{
//		if ((p + i) != NULL)
//		{
//			printf("%d ", *(p + i));
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	while (scanf("%d", &n) != EOF)
//	{
//		int i = 0;
//		for ( i = 0; i < n; i++)
//		{
//			int j = 0;
//			for (j = 0; j < n; j++)
//			{
//				if (i + j < n-1)
//				{
//					printf("  ");
//				}
//				else
//				{
//					printf("* ");
//				}
//				printf("\n");
//			}
//		}
//	}
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        int i = 0;
//        for (i = 0; i < n; i++)
//        {
//            int j = 0;
//            for (j = 0; j < i; j++)
//            {
//                printf(" ");
//            }
//            for (j = 0; j < n - i - 1; j++)
//            {
//                printf("* ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (j == n - i-1)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf(" %d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (j == n - i)
//                {
//                    printf("*");
//                }
//                else if (j == i)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    printf(" ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (i == 0 || i == n - 1)
//                {
//                    printf("*");
//                }
//                else
//                {
//                    if (j == 0 || j == n - 1)
//                    {
//                        printf("*");
//                    }
//                    else
//                    {
//                        printf(" ");
//                    }
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = 0; j <= i; j++)
//            {
//                if (i == j || j == 0 || i==n-1)
//                {
//                    printf("* ");
//                }
//                else
//                {
//                    printf("  ");
//                }
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//int a = 1;
//void test() {
//	int a = 2;
//	a += 1;
//	return a;
//}
//int main() {
//	test();
//	printf("%d\n", a);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int m = 0;
//	int i = 1;
//	scanf("%d %d", &a, &b);
//	m = (a < b) ? a : b;
//	while (m%b!=0)
//	{
//
//		i++;
//		m = i * a;
//	}
//	printf("%d\n", m);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//void reverse(char* left, char* right)
//{
//	assert(left && right);
//	while (left <= right)
//	{
//		char temp = *left;
//		*left = *right;
//		*right = temp;
//		left++;
//		right--;
//	}
//}
//int main()
//{
//	char arr[1000] = { 0 };
//	gets(arr);
//	
//	char* start = arr;
//	char* end = start;
//	//1.逆序每个单词
//	while (*end!='\0')
//	{
//		while (*end != ' '&&*end!='\0')
//		{
//			end++;
//		}
//		reverse(start, end-1);
//		if (*end == '\0')
//			start = end;
//		else
//			start = end + 1;
//
//		end = start;
//	}
//	//2.逆序整个字符串
//	reverse(arr, end - 1);
//	//输出
//	printf("%s\n", arr);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[1000] = { 0 };
//	gets(arr);
//	int num = strlen(arr);
//	char* start = &arr[num - 1];
//	printf("%c\n", arr[num - 1]);
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int* p = (int*)malloc(sizeof(int) * 5);
//	int* ptr = p;
//	free(p);
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	int a = 0;
//	int ret = 0;
//	int sum = 0;
//	scanf("%d %d", & n, &a);
//	for (int i = 0; i < n; i++)
//	{
//		ret = ret * 10 + a;
//		sum += ret;
//	}
//	printf("%d", sum);
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 100000; i++)
//	{
//		//求i有几位数
//		int n = 1;
//		int sum = 0;
//		int temp = i;
//		while (temp/=10)
//		{
//			n++;
//		}
//		//判断是否为水仙花数
//		temp = i;
//		while (temp)
//		{
//			sum+=(int)pow(temp % 10, n);
//			temp /= 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i < 100000; i++)
//	{
//		//算出i有几位数
//		int tmp = 0;
//		int n = 1;
//		int sum = 0;
//
//		tmp = i;
//		while (tmp /= 10)
//		{
//			n++;
//		}
//
//		//算出次方的和来
//		tmp = i;
//		while (tmp)
//		{
//			sum += pow(tmp % 10, n);
//			tmp /= 10;
//		}
//		//判断是否是水仙花数
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	for (int i = 0; i < 100000; i++)
//	{
//		//求出数的位数
//		int tmp = 0;
//		int n = 1;
//		int sum = 0;
//		
//		tmp = i;
//		while (tmp /= 10)
//		{
//			n++;
//		}
//		//求出位数的次方的和来
//		tmp = i;
//		sum += (int)pow(tmp % 10, n);
//		while (tmp/=10)
//		{
//			sum+= (int)pow(tmp % 10, n);
//		}
//		//进行判断
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int /* */ i;
//	char* s = "abcdefgh /* */hijklmn";
//	//Is it a\
//	valid comment? ;
//	//in/* */t j;
//	return 0;
//}
///*这是*/#/*一条*/define/*合法的*/ID/*预处理*/replacement/*指*/list/*令*/
////这段代码指的就是用replacement list替换ID
///*这是*/int/*一条*/abcd/*合法的*/efg/*预处理*/replacement/*指*/list/*令*/
//#include<stdio.h>
//int main()
//{
//	int money = 0;
//	int empty = 0;
//	int total = 0;
//	scanf("%d", &money);
//	//购买
//	total += money;
//	empty += money;
//	//置换
//	while (empty >= 2)
//	{
//		total += empty / 2;
//		empty = empty / 2 + empty % 2;
//	}
//	printf("%d\n", total);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int money = 0;
//	int total = 0;
//	scanf("%d", &money);
//	if (money <= 0)
//	{
//		total = 0;
//	}
//	else
//	{
//		total = money * 2 - 1;
//	}
//	printf("%d", total);
//	return 0;
//}
//#include<stdio.h>
//void move(int* arr,int size)
//{
//	int* left = arr;
//	int* right = arr+size-1;
//	while (left < right)
//	{
//		//从左向右找一个偶数停下来
//		while ((left<right)&&(*left)%2==1)
//		{
//			left++;
//		}
//		//从右向左找一个奇数停下来
//		while ((left<right)&&(*right)%2 == 0)
//		{
//			right--;
//		}
//		if (left < right)
//		{
//			int tmp = *left;
//			*left = *right;
//			*right = tmp;
//		}
//	}
//}
//int main()
//{
//	int size = 0;
//	int arr[10] = { 1,4,5,6,8,9,6,2,5,0 };
//	size = sizeof(arr) / sizeof(arr[0]);
//	move(arr,size);
//	for (int i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
/*
/*
*/
//* /
//int x = 10;
//int y = 10;
//int z = 5;
//int* p = &z;
//y = x/*p;
//#include<stdio.h>
//
//void move(int *arr)
//{
//	int *left = arr;
//	int *right = arr + 10 - 1;
//	while (left < right)
//	{
//		while (left<right&&*left % 2 != 0)
//		{
//			left++;
//		}
//		while (left<right&&*right % 2 == 0)
//		{
//			right--;
//		}
//		if (left < right)
//		{
//			int tmp = *left;
//			*left = *right;
//			*right = tmp;
//		}
//	}
//}
//int main()
//{
//	int arr[10] = { 0,1,3,2,4,6,7,9,8,5 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	move(arr);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//void move(int* arr, int size)
//{
//	int* arr2 = (int*)malloc(sizeof(int) * size);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < size; i++)
//	{
//		if (arr[i] % 2 != 0)
//		{
//			arr2[j] = arr[i];
//			j++;
//		}
//	}
//	j = size - 1;
//	for (i = 0; i < size; i++)
//	{
//		if (arr[i] % 2 == 0)
//		{
//			arr2[j] = arr[i];
//			j--;
//		}
//	}
//	for (i = 0; i < size; i++)
//	{
//		arr[i] = arr2[i];
//	}
//}
//int main()
//{
//	int arr[10] = { 0,2,3,4,5,6,7,8,9,1 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	move(arr, size);
//	for (int i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//1
//1 1
//1 2 1
//1 3 3 1
//#include<stdio.h>
//int main()
//{
//	int arr[10][10] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 10; i++)
//	{
//		for (j = 0; j <=i; j++)
//		{
//			if ((j == 0) || (i == j))
//			{
//				arr[i][j] = 1;
//			}
//			if (i > 1 && j > 0)
//			{
//				arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
//			}
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手
//#include<stdio.h>
//int main()
//{
//	char killer = 0;
//	for (killer = 'a'; killer <= 'd'; killer++)
//	{
//		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)
//		{
//			printf("%c\n", killer);
//		}
//	}
//	return 0;
//}
// 
//5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：
//A选手说：B第二，我第三；
//B选手说：我第二，E第四；
//C选手说：我第一，D第二；
//D选手说：C最后，我第三；
//E选手说：我第四，A第一；
//比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。
//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	int d = 0;
//	int e = 0;
//	for (a = 1; a <= 5; a++)
//	{
//		for (b = 1; b <= 5; b++)
//		{
//			for (c = 1; c <= 5; c++)
//			{
//				for (d = 1; d <= 5; d++)
//				{
//					for (e = 1; e <= 5; e++)
//					{
//						if (((b == 2) + (a == 3) == 1)
//							&& ((b == 2) + (e == 4) == 1)
//							&& ((c == 1) + (d == 2) == 1)
//							&& ((c == 5) + (d == 3) == 1)
//							&& ((e == 4) + (a == 1)) == 1)
//						{
//							if (a * b * c * d * e == 120)
//								printf("a是第%d名，b是第%d名,c是第%d名，d是第%d名，e是第%d", a, b, c, d, e);
//						}
//					}
//
//				}
//
//			}
//
//		}
//
//	}
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int score[7] = { 0 };
//    for (int i = 0; i < 7; i++)
//    {
//        scanf("%d", &score[i]);
//    }
//    int max = score[0];
//    int min = score[0];
//    for (int i = 0; i < 7; i++)
//    {
//        if (max < score[i])
//        {
//            max = score[i];
//        }
//        if (min > score[i])
//        {
//            min = score[i];
//        }
//    }
//    int sum = 0;
//    for (int i = 0; i < 7; i++)
//    {
//        sum += score[i];
//    }
//    sum = sum - min - max;
//    double ret = 0;
//    ret = sum / 5.0;
//    printf("%.2f", ret);
//    return 0;
//}
//#include<stdio.h>
//int add(int x, int y)
//{
//	printf("Before:y = %d\n", y);
//	*(&x + 1) = 100;
//	printf("After:y = %d\n", y);
//	return  x + y;
//}
//int main()
//{
//	int x = 10;
//	int y = 20;
//	int z = add(x, y);
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
////num:表示传入参数的个数
//int FindMax(int num, ...)
//{
//	va_list arg; //定义可以访问可变参数部分的变量，其实是一个char*类型
//	va_start(arg, num); //使arg指向可变参数部分(通过num就能够对其它的数据进行访问
//	//为什么可以？因为传递的那些临时变量的数据在内存空间上是连读的
//	int max = va_arg(arg, int); //根据类型，获取可变参数列表中的第一个数据
//	for (int i = 0; i < num - 1; i++) {//获取并比较其他的
//		int curr = va_arg(arg, int);
//		if (max < curr) {
//			max = curr;
//		}
//	}
//	va_end(arg); //arg使用完毕，收尾工作。本质就是讲arg指向NULL
//	return max;
//}
//int main()
//{
//	int max = FindMax(5, 11, 22, 33, 44, 55);
//	printf("max = %d\n", max);
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
////num:表示传入参数的个数
//int FindMax(int num, ...)
//{
//	va_list arg; //定义可以访问可变参数部分的变量，其实是一个char*类型
//	va_start(arg, num); //使arg指向可变参数部分
//	int max = va_arg(arg, int); //根据类型，获取可变参数列表中的第一个数据
//	for (int i = 0; i < num - 1; i++) {//获取并比较其他的
//		int curr = va_arg(arg, int);
//		if (max < curr) {
//			max = curr;
//		}
//	}
//	va_end(arg); //arg使用完毕，收尾工作。本质就是讲arg指向NULL
//	return max;
//}
//int main()
//{
//	char a = 'a'; //ascii值: 49
//	char b = 'b'; //ascii值: 50
//	char c = 'c'; //ascii值: 51
//	char d = 'd'; //ascii值: 52
//	char e = 'e'; //ascii值: 53
//	int max = FindMax(5, a, b, c, d, e);
//	printf("max = %d\n", max);
//	system("pause");
//	return 0;
//}
////结果并未受影响，可是，我们解析的时候，是按照va_arg(arg, int)来解析的，这是为什么?
//main函数也是一个函数，其实也可以携带参数的
//int main(int argc, char* argv[], char* envp[])
//{
//	program - statements
//}
//#include <stdio.h>
//int main(int argc, char* argv[], char* envp[])
//{
//	int i = 0;
//	for (i = 0; i < argc; i++)
//	{
//		printf("%s\n", argv[i]);
//	}
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h> //包含该头文件，才能使用win提供的GetTickCount()函数，来获取开机到现在的累计时间,此处用它，是因为简单
//int Fib(int n)
//{
//	if (1 == n || 2 == n) {
//		return 1;
//	}
//	return Fib(n - 1) + Fib(n - 2);
//}
//int main()
//{
//	int n = 42;
//	double start = GetTickCount();
//	int x = Fib(n);
//	double end = GetTickCount();
//	printf("fib(%d): %d\n", n, x);
//	printf("%lf ms\n", (end - start) / 1000); //单位是毫秒(ms),转化成为s
//	system("pause");
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
//int Fib(int n)
//{
//	int* dp = (int*)malloc(sizeof(int) * (n + 1)); //暂时不做返回值判定了
//	//[0]不用(当然，也可以用，不过这里我们从1，1开始，为了后续方便)
//	dp[1] = 1;
//	dp[2] = 1;
//	for (int i = 3; i <= n; i++) {
//		dp[i] = dp[i - 1] + dp[i - 2];
//	}
//	int ret = dp[n];
//	free(dp);
//	return ret;
//}
//int main()
//{
//	int n = 42;
//	double start = GetTickCount();
//	int x = Fib(n);
//	double end = GetTickCount();
//	printf("fib(%d): %d\n", n, x);
//	printf("%lf ms\n", (end - start) / 1000);
//	system("pause");
//	return 0;
//}
//这其实是一种动态规划的算法哦，看起来很神秘，其实这些问题也可以使用改方法解决哦
//#include<stdio.h>
//#include<malloc.h>
//int fib(int n)
//{
//	int* f = (int*)(malloc(sizeof(int) * (n + 1)));
//	if (n == 1 || n == 2)
//	{
//		free(f);
//		return 1;
//	}
//	f[1] = 1;
//	f[2] = 1;
//	int i = 3;
//	for ( i = 3; i <= n; i++)
//	{
//		f[i] = f[i - 1] + f[i - 2];
//	}
//	int ret = f[n];
//	free(f);
//	return ret;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = fib(n);
//	printf("%d", ret);
//	return 0;
//}
//#include <stdio.h>
//#include <windows.h>
//int Fib(int n)
//{
//	int first = 1;
//	int second = 1;
//	int third = 1;
//	while (n > 2) {
//		third = second + first;
//		first = second;
//		second = third;
//		n--;
//	}
//	return third;
//}
//int main()
//{
//	int n = 42;
//	double start = GetTickCount();
//	int x = Fib(n);
//	double end = GetTickCount();
//	printf("fib(%d): %d\n", n, x);
//	printf("%lf ms\n", (end - start) / 1000);
//	system("pause");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a = 0b00000000000000000000000000001010
//	printf("%d", a);
//	return 0;
//}
//a = 00000000 00000000 00000000 00001010
//a =  
//#include <stdio.h>
//
//
//int main()
//{
//    int i = 0;
//    int sum = 0;
//    int min = 100;//假设最小值是100
//    int max = 0;//假设最大值是0
//    int score = 0;
//    for (i = 0; i < 7; i++)
//    {
//        scanf("%d ", &score);
//        sum += score;
//        if (score > max)
//            max = score;
//        if (score < min)
//            min = score;
//    }
//    printf("%.2f\n", (sum - min - max) / 5.0);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int* arr = (int*)malloc(sizeof(int) * 100000);
//    int i = 0;
//    while (scanf("%d", &arr[i]) != EOF)
//    {
//        i++;
//    }
//    for (int j = 0; j < i; j++)
//    {
//        for (int k = 0; k < i - j - 1; k++)
//        {
//            if (arr[k] > arr[k + 1])
//            {
//                int temp = arr[k];
//                arr[k] = arr[k + 1];
//                arr[k + 1] = temp;
//            }
//        }
//    }
//    for (int k = 0; k < i; k++)
//    {
//        int flag = 1;
//        for (int j = 0; j < i; j++)
//        {
//            if (arr[j] == arr[k])
//            {
//                flag = 0;
//                break;
//            }
//        }
//        if (flag)
//        {
//            printf("%d ", arr[k]);
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int delete = 0;
//    scanf("%d", &delete);
//    int x = 0;
//    while (scanf("%d", &x) != EOF)
//    {
//        if (x != delete)
//        {
//            printf("%d ", x);
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int arr[10][10] = { 0 };
//    scanf("%d %d", &n, &m);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            printf("%d ", arr[j][i]);
//        }
//        printf("\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//int main()
//{
//    int* arr = (int*)(malloc(sizeof(int) * 100000));
//    int i = 0;
//    scanf("%d", &i);
//    for (int j = 0; j < i; j++)
//    {
//        scanf("%d", &arr[j]);
//    }
//    for (int j = 0; j < i; j++)
//    {
//        for (int k = 0; k < i - j - 1; k++)
//        {
//            if (arr[k] > arr[k + 1])
//            {
//                int temp = arr[k];
//                arr[k] = arr[k + 1];
//                arr[k + 1] = temp;
//            }
//        }
//    }
//    for (int k = 0; k < i; k++)
//    {
//        if (k == 0)
//        {
//            printf("%d ", arr[k]);
//        }
//        else
//        {
//            int flag = 1;
//            for (int j = 0; j < k; j++)
//            {
//                if (arr[j] == arr[k])
//                {
//                    flag = 0;
//                    break;
//                }
//            }
//            if (flag)
//            {
//                printf("%d ", arr[k]);
//            }
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int score[7] = { 0 };
//    while (scanf("%d %d %d %d %d %d %d", &score[0], &score[1], &score[2], &score[3]\
//        , &score[4], &score[5], &score[6]) != EOF)
//    {
//        int max = score[0];
//        int min = score[0];
//        for (int i = 0; i < 7; i++)
//        {
//            if (max < score[i])
//            {
//                max = score[i];
//            }
//            if (min > score[i])
//            {
//                min = score[i];
//            }
//        }
//
//        int sum = 0;
//        for (int i = 0; i < 7; i++)
//        {
//            sum += score[i];
//        }
//        sum = sum - min - max;
//        double ret = 0;
//        ret = sum / 5.0;
//        printf("%.2f", ret);
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//int main()
//{
//    int n = 0;
//    while (scanf("%d", &n) != EOF)
//    {
//        int* p = (int*)malloc(sizeof(int) * (n));
//        for (int i = 0; i < n; i++)
//        {
//            p[i] = i + 1;
//        }
//        int count = 0;
//        for (int i = 1; i < n; i++)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                if (p[i] != 0)
//                {
//                    if (p[j] % p[i] == 0)
//                    {
//                        p[j] = 0;
//                        count++;
//                    }
//                }
//                else
//                    continue;
//            }
//        }
//        for (int i = 1; i < n; i++)
//        {
//            if (p[i] != 0)
//            {
//                printf("%d ", p[i]);
//            }
//        }
//        printf("\n%d", count);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int m = 0;
//    int n = 0;
//    scanf("%d %d", &m, &n);
//    int arr1[100][100] = { 0 };
//    int arr2[100][100] = { 0 };
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr1[i][j]);
//        }
//    }
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            scanf("%d", &arr2[i][j]);
//        }
//    }
//    int count = 0;
//    for (int i = 0; i < m; i++)
//    {
//        for (int j = 0; j < n; j++)
//        {
//            if (arr1[i][j] == arr2[i][j])
//            {
//                count++;
//            }
//        }
//    }
//    double ret = 0;
//    ret = count / (m * n) * 100.0;
//    printf("%.2f", ret);
//    return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char* admin = "admin";
//    char name[100] = { 0 };
//    char security[100] = { 0 };
//    while (scanf("%s %s", name, security)!=EOF)
//    {
//        if ((strcmp(name, admin) == 0) && (strcmp(security, admin) == 0))
//        {
//            printf("Login Success!\n");
//        }
//        else
//        {
//            printf("Login Fail!\n");
//        }
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int a = 0;
//    scanf("%d", &a);
//    if (a % 2)
//    {
//        printf("YES\n");
//    }
//    else
//    {
//        printf("NO\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int i = 0;
//    int count = 0;
//    for (i = 1; i < 2020; i++)
//    {
//        while (i)
//        {
//            if (i % 10 == 9)
//            {
//                count++;
//                break;
//            }
//            i /= 10;
//        }
//    }
//    printf("%d", count);
//    return 0;
//}
//#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//int main()
//{
//
//	int arr[10][10] = { 0 };
//	int n = 0;
//	int m = 0;
//	scanf("%d", &n);
//	scanf("%d", &m);
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//
//	}
//	for (i = 0; i < n; i++)
//	{
//		for (j = 0; j < m; j++)
//		{
//			printf("%d", arr[i][j]);
//		}
//
//	}
//	printf("\n");
//	/*int k = 0;
//	scanf("%d", &k);*/
//	char x = 0;
//	char y = 0;
//	char z = 0;
//	scanf("%c%c%c", &x, &y, &z);
//	printf("%c\n%c\n%c", x, y, z);
//	return 0;
//}
//#include <stdio.h>
//void new_line()
//{
//    printf("hehe\n");
//}
//void three_line()
//{
//    int i = 0;
//    for (i = 0; i < 3; i++)
//    {
//        new_line();
//    }
//}
//int main()
//{
//    three_line();
//    return 0;
//}
//#include <stdio.h>
//#include <string.h>
//int main()
//{
//    char arr[20] = "hello";
//    int ret = strlen(strcat(arr, "bit"));//这里介绍一下strlen函数
//    printf("%d\n", ret);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    printf("%d", printf("%d", printf("%d", 43)));
//    //结果是啥？
//    //注：printf函数的返回值是打印在屏幕上字符的个数
//    return 0;
//}
//#ifndef __TEST_H__
//#define __TEST_H__
////函数的声明
//int Add(int x, int y);
//#endif //__TEST_H__
//#include <stdio.h>
//void print(int n)
//{
//	if (n > 9)
//	{
//		print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//int main()
//{
//	int num = 1234;
//	print(num);
//	return 0;
//}
//#include <stdio.h>
//int Strlen(const char* str)
//{
//    if (*str == '\0')
//        return 0;
//    else
//        return 1 + Strlen(str + 1);
//}
//int main()
//{
//    char* p = "abcdef";
//    int len = Strlen(p);
//    printf("%d\n", len);
//    return 0;
//}
////代码1
//int arr1[10];
////代码2
//int count = 10;
//int arr2[count];//数组时候可以正常创建？
////代码3
//char arr3[10];
//float arr4[1];
//double arr5[20];
//int arr1[10] = { 1,2,3 };
//int arr2[] = { 1,2,3,4 };
//int arr3[5] = { 1,2,3,4,5 };
//char arr4[3] = { 'a',98, 'c' };
//char arr5[] = { 'a','b','c' };
//char arr6[] = "abcdef";
//#include<stdio.h>
//int main()
//{
//    int arr[10] = { 0 };//数组的不完全初始化
//       //计算数组的元素个数
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    //对数组内容赋值,数组是使用下标来访问的，下标从0开始。所以：
//    int i = 0;//做下标
//    for (i = 0; i < 10; i++)//这里写10，好不好？
//    {
//        arr[i] = i;
//    }
//    //输出数组的内容
//    for (i = 0; i < 10; ++i)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//方法一：
//#include <stdio.h>
//void bubble_sort(int arr[])
//{
//    int sz = sizeof(arr) / sizeof(arr[0]);//这样对吗？
//    int i = 0;
//    for (i = 0; i < sz - 1; i++)
//    {
//        int j = 0;
//        for (j = 0; j < sz - i - 1; j++)
//        {
//            if (arr[j] > arr[j + 1])
//            {
//                int tmp = arr[j];
//                arr[j] = arr[j + 1];
//                arr[j + 1] = tmp;
//            }
//        }
//    }
//}
//int main()
//{
//    int arr[] = { 3,1,7,5,8,9,0,2,4,6 };
//    bubble_sort(arr);//是否可以正常排序？
//    for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//方法2
//void bubble_sort(int arr[], int sz)//参数接收数组元素个数
//{
//    //代码同上面函数
//}
//int main()
//{
//    int arr[] = { 3,1,7,5,8,9,0,2,4,6 };
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    bubble_sort(arr, sz);//是否可以正常排序？
//    for (int i = 0; i < sz; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int arr[50] = { 0 };
//    int delete = 0;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    scanf("%d", &delete);
//    int i = 0;
//    int j = 0;
//    for (i = 0,j = 0; i < n; i++)
//    {//i的作用：遍历数组
//     //j的作用：记录存放数据位置的下标
//        if (arr[i] != delete)
//        {
//            arr[j++] = arr[i];
//        }
//        //此时的j中存放的数据就是删除元素后剩下的元素个数
//            
//    }
//    for (int i = 0; i < j; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int arr[1000] = { 0 };
//    scanf("%d", &n);
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    //去重
//    for (i = 0; i < n; i++)
//    {
//        //判断arr[i]是否在后面出现过
//        int j = 0;
//        for (j = i + 1; j < n; j++)
//        {
//            if (arr[i] == arr[j])
//            {
//                //去重，即后面的元素向前面进行覆盖
//                int k = 0;
//                for (k = j; k < n-1; k++)
//                {
//                    arr[k] = arr[k + 1];
//                }
//                n--;
//                j--;//防止覆盖上来的仍然与前面的arr[i]元素重复
//            }
//        }
//    }
//    for (int i = 0; i < n; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int all = m + n;
//    int i = 0;
//    //输入n和m
//    scanf("%d %d", &n, &m);
//    int* arr1 = (int*)malloc(sizeof(int) * n);
//    int* arr2 = (int*)malloc(sizeof(int) * m);
//    int* arr3 = (int*)malloc(sizeof(int) * (m + n));
//    //输入两个有序数组
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr1[i]);
//    }
//    for (int i = 0; i < n; i++)
//    {
//        scanf("%d", &arr2[i]);
//    }
//    i = 0;
//    int j = 0;
//    int k = 0;
//    while (i < n && j < m)
//    {
//        if (arr1[i] < arr2[j])
//        {
//            arr3[k++] = arr1[i++];
//        }
//        else
//        {
//            arr3[k++] = arr2[j++];
//        }
//    }
//    if (i == n)
//    {
//        for (; j < m; j++)
//        {
//            arr3[k++] = arr2[j];
//        }
//    }
//    else
//    {
//        for (; i < n; i++)
//        {
//            arr3[k++] = arr1[i];
//        }
//    }
//    for (int i = 0; i < k; i++)
//    {
//        printf("%d ", arr3[i]);
//    }
//    
//    return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//int main()
//{
//	int m = 3;
//	int n = 2;
//	int** a = (int**)(malloc(sizeof(int*) * m));
//	for (int i = 0; i < m; i++)
//	{
//		a[i] = (int*)(malloc(sizeof(int) * n));
//	}
//	for (int i = 0; i < m; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			scanf("%d", &a[i][j]);
//		}
//	}
//	for (int i = 0; i < m; i++)
//	{
//		for (int j = 0; j < n; j++)
//		{
//			printf("%d", a[i][j]);
//		}
//	}
//	for (int i = 0; i < m; i++)
//	{
//		free(a[i]);
//	}
//	free(a);
//	return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//int main()
//{
//	int(*a)[2] = (int(*)[2])malloc(sizeof(int) * 3 * 2);
//	//前面的int (*a)[2]的类型是指针数组，每个元素是指针，总共有2个元素
//	//究竟该如何理解呢？就是说，总共申请了3*2，总共6个整型地址空间
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 2; j++)
//		{
//			printf("%p\n", &a[i][j]);
//		}
//	}
//	free(a);
//	return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//
//
//int main()
//{
//    int nrows, ncolumns;
//    int* Array;
//    int i, j;
//    printf("please input nrows&ncolumns:\n");
//    scanf("%d%d", &nrows, &ncolumns);
//    Array = (int*)malloc(nrows * ncolumns * sizeof(int));   //申请内存空间
//    for (i = 0; i < nrows; i++)
//    {
//        for (j = 0; j < ncolumns; j++)
//        {
//            Array[i * ncolumns + j] = 1;
//            printf("%d ", Array[i * ncolumns + j]);   //用Array[i*ncolumns+j] 访问第i,j个成员
//        }
//        printf("\n");
//    }
//    free(Array);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int arr[51] = { 0 };
//    int m = 0;
//    int i = 0;
//    {
//        scanf("%d", &n);
//    }
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//    scanf("%d", &m);
//    for (int i = n - 1; i >= 0; i--)
//    {
//        if (arr[i] > m)
//        {
//            arr[i + 1] = arr[i];
//        }
//        else
//        {
//            arr[i + 1] = m;
//            break;
//        }
//    }
//    if (i < 0)
//    {
//        arr[0] = m;
//    }
//    for (i = 0; i < n + 1; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int arr[10][10] = { 0 };
//    scanf("%d %d", &n, &m);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    char ch;
//    int x = 0;
//    int y = 0;
//    
//    scanf("%c", &ch);
//    scanf("%d %d", &x, &y);
//    if (ch == 'r')
//    {
//        for (int j = 0; j < m; j++)
//        {
//            int tmp = 0;
//            tmp = arr[x][j];
//            arr[x][j] = arr[y][j];
//            arr[y][j] = tmp;
//        }
//    }
//    else if (ch == 'c')
//    {
//        for (int i = 0; i < n; i++)
//        {
//            int tmp = 0;
//            tmp = arr[i][x];
//            arr[i][x] = arr[i][y];
//            arr[i][y] = tmp;
//        }
//    }
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            printf("%d ", arr[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int n = 0;
//    int m = 0;
//    int k = 0;
//    int arr[10][10] = { 0 };
//    scanf("%d %d", &n, &m);
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//    scanf("%d", &k);//此处输入缓冲区里面有一个\n
//    int t = 0;
//    int x = 0;
//    int y = 0;
//    //%c前面的空格是用来吸收输入缓冲区里面的\n
//    //只有读字符的时候才会对输入缓冲区里面的数据是否为空有严格的要求
//    //而在%d的时候没有要求
//    scanf(" %c %d %d", &t, &x, &y);
//    for (int i = 0; i < k; i++)
//    {
//        if (t == 'r')
//        {
//            for (int j = 0; j < m; j++)
//            {
//                int tmp = 0;
//                tmp = arr[x-1][j];
//                arr[x-1][j] = arr[y-1][j];
//                arr[y-1][j] = tmp;
//            }
//        }
//        else if (t == 'c')
//        {
//            for (int j = 0; j < n; j++)
//            {
//                int tmp = 0;
//                tmp = arr[i][x];
//                arr[j][x-1] = arr[j][y-1];
//                arr[j][y-1] = tmp;
//            }
//        }
//    }
//    
//    for (int i = 0; i < n; i++)
//    {
//        for (int j = 0; j < m; j++)
//        {
//            printf("%d ", arr[i][j]);
//        }
//        printf("\n");
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    char arr[3][3] = { 0 };
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < 3; i++)
//    {
//        for (j = 0; j < 3; j++)
//        {
//            scanf("%c", &arr[i][j]);
//        }
//    }
//    int flag = 0;
//    //1代表kk胜利，2代表bb胜利
//    for (i = 0; i < 3; i++)
//    {
//        if (arr[i][0] == 'K' && arr[i][0] == arr[i][1] && arr[i][1] == arr[i][2])
//        {
//            flag = 1;
//            break;
//        }
//        else if (arr[i][0] == 'B' && arr[i][0] == arr[i][1] && arr[i][1] == arr[i][2])
//        {
//            flag = 2;
//            break;
//        }
//
//        if (arr[0][i] == 'K' && arr[0][i] == arr[1][i] && arr[1][i] == arr[2][i])
//        {
//            flag = 1;
//            break;
//        }
//        else if (arr[0][i] == 'B' && arr[0][i] == arr[1][i] && arr[1][i] == arr[2][i])
//        {
//            flag = 2;
//            break;
//        }
//    }
//    if (arr[0][0] == 'K' && arr[0][0] == arr[1][1] && arr[1][1] == arr[2][2])
//        flag = 1;
//    else if (arr[0][0] == 'B' && arr[0][0] == arr[1][1] && arr[1][1] == arr[2][2])
//        flag = 2;
//    if (arr[0][2] == 'K' && arr[0][2] == arr[1][1] && arr[1][1] == arr[2][0])
//        flag = 1;
//    else if (arr[0][2] == 'B' && arr[0][2] == arr[1][1] && arr[1][1] == arr[2][0])
//        flag = 2;
//    if (flag == 1)
//    {
//        printf("KiKi wins!\n");
//    }
//    else if (flag == 2)
//    {
//        printf("BoBo wins!\n");
//    }
//    else
//    {
//        printf("No winner!\n");
//    }
//    return 0;
//}
//#include <stdio.h>
//void test1(int arr[])
//{
//	printf("%d\n", sizeof(arr));//(2)
//}
//void test2(char ch[])
//{
//	printf("%d\n", sizeof(ch));//(4)
//}
//int main()
//{
//	int arr[10] = { 0 };
//	char ch[10] = { 0 };
//	printf("%d\n", sizeof(arr));//(1)
//	printf("%d\n", sizeof(ch));//(3)
//	test1(arr);
//	test2(ch);
//	return 0;
//}
////++和--运算符
////前置++和--
//#include <stdio.h>
//int main()
//{
//    int a = 10;
//    int x = ++a;
//    //先对a进行自增，然后对使用a，也就是表达式的值是a自增之后的值。x为11。
//    int y = --a;
//    //先对a进行自减，然后对使用a，也就是表达式的值是a自减之后的值。y为10;
//    return 0;
//}
////后置++和--
//#include <stdio.h>
//int main()
//{
//    int a = 10;
//    int x = a++;
//    //先对a先使用，再增加，这样x的值是10；之后a变成11；
//    int y = a--;
//    //先对a先使用，再自减，这样y的值是11；之后a变成10；
//    return 0;
//}
//代码1
//#include<stdio.h>
//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = (a > b, a = b + 10, a, b = a + 1);//逗号表达式
//	printf("a = %d b = %d c = %d", a, b, c);
//	return 0;
//}
//#include <stdio.h>
//void test1()
//{
//	printf("hehe\n");
//}
//void test2(const char* str)
//{
//	printf("%s\n", str);
//}
//int main()
//{
//	test1();            //实用（）作为函数调用操作符。
//	test2("hello bit.");//实用（）作为函数调用操作符。
//	return 0;
//}
//#include <stdio.h>
//struct Stu
//{
//	char name[10];
//	int age;
//	char sex[5];
//	double score;
//};
//void set_age1(struct Stu stu)
//{
//	stu.age = 18;
//}
//void set_age2(struct Stu* pStu)
//{
//	pStu->age = 18;//结构成员访问
//}
//int main()
//{
//	struct Stu stu;
//	struct Stu* pStu = &stu;//结构成员访问
//
//	stu.age = 20;//结构成员访问
//	set_age1(stu);
//
//	pStu->age = 20;//结构成员访问
//	set_age2(pStu);
//	return 0;
//}
//实例1
//#include<stdio.h>
//int main()
//{
//	char a = 0xb6;
//	short b = 0xb600;
//	int c = 0xb6000000;
//	if (a == 0xb6)
//		printf("a");
//	if (b == 0xb600)
//		printf("b");
//	if (c == 0xb6000000)
//		printf("c");
//	return 0;
//}
//实例2
//#include<stdio.h>
//int main()
//{
//	char c = 1;
//	printf("%u\n", sizeof(!c));
//	return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//
//int main()
//{
//    /**********  Begin   **********/
//    double score = 0.0;
//    double sum = 0.0;
//    double avg = 0.0;
//    for (int i = 0; i < 5; i++)
//    {
//        scanf("%lf", &score);
//        sum += score;
//    }
//    avg = sum / 5.0;
//    printf("%.2lf", avg);
//
//
//    /**********  End   **********/
//    return 0;
//}
//#include <stdio.h>
//
//int main(void)
//{
//    int i, j, n;
//    double e, product;
//    scanf("%d", &n);
//    double sum = 0.0;
//    /**********  Begin  **********/
//    int num = 0;
//    e = 0.0;
//    for (int i = 1; i <= n; i++)
//    {
//        num = 1;
//        for (int j = 1; j <= i; j++)
//        {
//            num *= j;
//        }
//        sum += (1.0 / num);
//    }
//
//    /**********  End  **********/
//    printf("e = %0.4f\n", e);
//    return 0;
//}
//#include <stdio.h>
//
//double fact(int n)
//{
//    int x = 1;
//    for (int i = 1; i <= n; i++)
//    {
//        x *= i;
//    }
//    return x;
//
//}
//int main()
//{
//    int  i, n;
//    double e = 0.0;
//    scanf("%d", &n);
//    /**********  Begin  **********/
//    for (i = 1; i <= n; i++)
//    {
//        e += fact(i);
//    }
//
//    /**********  End  **********/
//    printf("e = %f", e);
//    return 0;
//}
//#include<stdio.h>
//#include<math.h>
//
//int main()
//{
//    int i, n;
//    double sum;
//    /**********  Begin  **********/
//    sum = 0.0;
//    for (int i = 1; i <=n; i++)
//    {
//        sum += (double)sqrt(i);
//    }
//    /**********  End  **********/
//    printf("sum = %.2f", sum);
//    return 0;
//}
//#include <stdio.h>
//#include <stdlib.h>
//
//int main(void)
//{
//	char c;
//	int blank, digit, i, letter, other;
//	blank = 0;
//	digit = 0;
//	letter = 0;
//	other = 0;
//
//	/**********  Begin  **********/
//	char a[1000] = { 0 };
//	gets(a);
//	int i = 0;
//	while (a[i] != '\0')
//	{
//		if ((a[i] >= 65 && a[i] <= 90) || (a[i] >= 97 && a[i] <= 122))
//		{
//			letter++;
//		}
//		else if (a[i] >= 48 && c <= 58)
//		{
//			digit++;
//		}
//		else if (a[i] == 32)
//		{
//			blank++;
//		}
//		else
//		{
//			other++;
//		}
//		i++;
//	}
//
//	/**********  End  **********/
//	printf("letter = %d, blank = %d, digit = %d, other = %d\n", letter, blank, digit, other);
//}
//#include<stdio.h>
//int main()
//{
//    long long n = 0;
//    long long m = 0;
//    scanf("%lld %lld", &n, &m);
//    long long i = 0;
//    long long max = m < n ? m : n;
//    long long min = m > n ? m : n;
//    for (i = max; i >=0; i--)
//    {
//        if (m % i == 0 && n % i == 0)
//        {
//            max = i;
//            break;
//        }
//        else
//        {
//            continue;
//        }
//    }
//    for (i = min;; i++)
//    {
//        if (i % m == 0 && i % n == 0)
//        {
//            min = i;
//            break;
//        }
//        else
//        {
//            continue;
//        }
//    }
//    printf("%lld", max + min);
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	long long n = 0;
//	long long m = 0;
//	long long n2 = n;
//	long long m2 = m;
//	long long max = 0;
//	long long min = 0;
//	long long temp = 0;
//	while (temp = n % m != 0)
//	{
//		n = m;
//		m = temp;
//	}
//	max = m;
//	min = n2 * m2 / max;
//	printf("%lld", max + min);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int a = 10;//在内存中开辟一块空间
//	int* p = &a;//这里我们对变量a，取出它的地址，可以使用&操作符。
//	   //a变量占用4个字节的空间，这里是将a的4个字节的第一个字节的地址存放在p变量
//	中，p就是一个之指针变量。
//		return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int n = 10;
//	char* pc = (char*)&n;
//	int* pi = &n;
//
//	printf("%p\n", &n);
//	printf("%p\n", pc);
//	printf("%p\n", pc + 1);
//	printf("%p\n", pi);
//	printf("%p\n", pi + 1);
//	return  0;
//}
//#include <stdio.h>
//int main()
//{
//	int n = 0x11223344;
//	char* pc = (char*)&n;
//	int* pi = &n;
//	*pc = 0;   //重点在调试的过程中观察内存的变化。
//	*pi = 0;   //重点在调试的过程中观察内存的变化。
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int* p = NULL;
//    //....
//    int a = 10;
//    p = &a;
//    if (p != NULL)
//    {
//        *p = 20;
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int x = 0;
//	int* p = &x;
//	printf("%p\n", p);
//	printf("%p\n", p + 1);
//	char* p2 = (char*)&x;
//	printf("%p\n", p2);
//	printf("%p\n", p2+1);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	int* p1 = &arr[4];
//	int* p2 = &arr[0];
//	printf("%d", (char*)p1 - (char*)p2);
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//    int* p = arr; //指针存放数组首元素的地址
//    int sz = sizeof(arr) / sizeof(arr[0]);
//    for (int i = 0; i < sz; i++)
//    {
//        printf("&arr[%d] = %p   <====> p+%d = %p\n", i, &arr[i], i, p + i);
//    }
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
//	int* p = arr; //指针存放数组首元素的地址
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}
//#include<stdio.h>
//void bubble_sort(int arr[], int n)
//{
//	for (int i = 0; i < n - 1; i++)
//	{
//		for (int j = 0; j < n - i - 1; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int temp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = temp;
//			}
//		}
//	}
//}
//int main()
//{
//	int arr[1000] = { 0 };
//	int n = 0;
//	scanf("%d", &n);
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	bubble_sort(arr, n);
//	for (i = 0; i < n - 1; i++)
//	{
//		if (arr[i] == arr[i + 1])
//		{
//			int k = 0;
//			for (k = i; k < n - 1; k++)
//			{
//				arr[k] = arr[k + 1];
//			}
//			n--;
//			i--;
//		}
//	}
//	for (i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<malloc.h>
//struct Node
//{
//    int num;
//    struct Node* next;
//};
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    int i = 0;
//    int m = 0;
//    int d = 0;//要删除的元素
//    struct Node* list = NULL;//指向链表的指针
//    struct Node* tail = NULL;//指向链表尾部元素的指针
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &m);
//        struct Node* pn = (struct Node*)malloc(sizeof(struct Node));
//        pn->num = m;
//        pn->next = NULL;
//        if (list == NULL)
//        {
//            list = pn;
//            tail = pn;
//        }
//    }
//    //接收要删除的元素
//    scanf("%d", &d);
//    //删除列表中指定的元素
//    struct Node* cur = list;
//    struct Node* prev = NULL;
//    while (cur)
//    {
//        if (cur->num == d)
//        {
//            //删除
//            struct Node* pd = cur;
//            if (cur == list)
//            {
//                list = list->next;
//                cur = list;
//            }
//            else
//            {
//                prev->next = cur->next;
//                cur = prev->next;
//            }
//            free(pd);
//            n--;
//        }
//        else
//        {
//            prev = cur;
//            cur = cur->next;
//        }
//    }
//    //
//    printf("%d\n", n);
//    cur = list;
//    while (cur)
//    {
//        printf("%d", cur->num);
//        cur = cur->next;
//    }
//    //释放单链表
//    cur = list;
//    struct Node* del = NULL;
//    while (cur)
//    {
//        del = cur;
//        cur = cur->next;
//        free(del);
//    }
//    list = NULL;
//    return 0;
//}
//struct tag
//{
//	member - list;
//}variable - list;
//typedef struct Stu
//{
//	char name[20];//名字
//	int age;//年龄
//	char sex[5];//性别
//	char id[20];//学号
//}Stu；//分号不能丢
//struct Point
//{
//	int x;
//	int y;
//}p1; //声明类型的同时定义变量p1
//struct Point p2; //定义结构体变量p2
////初始化：定义变量的同时赋初值。
//struct Point p3 = { x, y };
//struct Stu        //类型声明
//{
//	char name[15];//名字
//	int age;      //年龄
//};
//struct Stu s = { "zhangsan", 20 };//初始化
//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化
//struct S s;
//strcpy(s.name, "zhangsan");//使用.访问name成员
//s.age = 20;//使用.访问age成员
//struct Stu
//{
//	char name[20];
//	int age;
//};
//void print(struct Stu* ps)
//{
//	printf("name = %s   age = %d\n", (*ps).name, (*ps).age);
//	//使用结构体指针访问指向对象的成员
//	printf("name = %s   age = %d\n", ps->name, ps->age);
//}
//int main()
//{
//	struct Stu s = { "zhangsan", 20 };
//	print(&s);//结构体地址传参
//	return 0;
//}
//struct S
//{
//	int data[1000];
//	int num;
//};
//struct S s = { {1,2,3,4}, 1000 };
////结构体传参
//void print1(struct S s)
//{
//	printf("%d\n", s.num);
//}
////结构体地址传参
//void print2(struct S* ps)
//{
//	printf("%d\n", ps->num);
//}
//int main()
//{
//	print1(s);  //传结构体
//	print2(&s); //传地址
//	return 0;
//}
//#include <stdio.h>//引用头文件，使我们可以使用C语言库本身就已经提供给我们的函数，即库函数
//int main()//main()是主函数
//{
//    printf("hello world\n");//printf()是输出函数，'\n'是换行的意思
//    return 0;//使程序退出，0的意思是程序正常退出
//}
////解释：
////main函数是程序的入口
////一个工程中main函数有且仅有一个
//#include<stdio.h>
//int main()
//{
//	char string1[] = "abc";
//	char string2[] = { 'a','b','c' };
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    printf("%d\n", sizeof(char));
//    printf("%d\n", sizeof(short));
//    printf("%d\n", sizeof(int));
//    printf("%d\n", sizeof(long));
//    printf("%d\n", sizeof(long long));
//    printf("%d\n", sizeof(float));
//    printf("%d\n", sizeof(double));
//    printf("%d\n", sizeof(long double));
//    return 0;
//}
//#include <stdio.h>
//int global = 2019;//全局变量
//int main()
//{
//    int local = 2018;//局部变量
//    //下面定义的global会不会有问题？
//    int global = 2020;//局部变量
//    printf("global = %d\n", global);
//    return 0;
//}
//#include <stdio.h>
////举例
//enum Sex
//{
//    MALE,
//    FEMALE,
//    SECRET
//};
////括号中的MALE,FEMALE,SECRET是枚举常量
//int main()
//{
//    //字面常量演示
//    3.14;//字面常量
//    1000;//字面常量
//
//    //const 修饰的常变量
//    const float pai = 3.14f;   //这里的pai是const修饰的常变量
//    pai = 5.14;//是不能直接修改的！会报警告！！！
//
//    //#define的标识符常量 演示
//#define MAX 100
//    printf("max = %d\n", MAX);
//
//    //枚举常量演示
//    printf("%d\n", MALE);
//    printf("%d\n", FEMALE);
//    printf("%d\n", SECRET);
//    //注：枚举常量的默认是从0开始，依次向下递增1的
//    return 0;
//}

//#include <stdio.h>
////下面代码，打印结果是什么？为什么？（突出'\0'的重要性）
//int main()
//{
//    char arr1[] = "bit";
//    char arr2[] = { 'b', 'i', 't' };
//    char arr3[] = { 'b', 'i', 't','\0'};
//    printf("%s\n", arr1);
//    printf("%s\n", arr2);
//    printf("%s\n", arr3);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    printf("c:\code\test.c\n");
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    //问题1：在屏幕上打印一个单引号'，怎么做？
//    //问题2：在屏幕上打印一个字符串，字符串的内容是一个双引号“，怎么做？
//    printf("%c\n", '\'');
//    printf("%s\n", "\"");
//    return 0;
//}
//程序输出什么？
//#include <stdio.h>
//int main()
//{
//    printf("%d\n", strlen("abcdef"));
//    // \62被解析成一个转义字符
//    printf("%d\n", strlen("c:\test\628\test.c"));
//    return 0;
//}
//#include<stdio.h>
//int main()
//{
//    int coding = 0;
//    printf("你会去敲代码吗？（选择1 or 0）:>");
//    scanf("%d", &coding);
//    if (coding == 1)
//    {
//        printf("坚持，你会有好offer，女朋友陪在身边\n");
//    }
//    else
//    {
//        printf("毕业即失业，女朋友跟你分手\n");
//    }
//    return 0;
//}
//while循环的实例
//#include <stdio.h>
//int main()
//{
//    int num1 = 0;
//    int num2 = 0;
//    int sum = 0;
//    printf("输入两个操作数:>");
//    scanf("%d %d", &num1, &num2);
//    sum = num1 + num2;
//    printf("sum = %d\n", sum);
//    return 0;
//}
//上述代码，写成函数如下：
//#include <stdio.h>
//int Add(int x, int y)
//{
//    int z = x + y;
//    return z;
//}
//int main()
//{
//    int num1 = 0;
//    int num2 = 0;
//    int sum = 0;
//    printf("输入两个操作数:>");
//    scanf("%d %d", &num1, &num2);
//    sum = Add(num1, num2);
//    printf("sum = %d\n", sum);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int i = 0;
//    int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//    for (i = 0; i < 10; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//    printf("\n");
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 5;
//	int c = 3;
//	printf("%d\n", a + b);
//	printf("%d\n", a - b);
//	printf("%d\n", a * b);
//	printf("%d\n", a / b);
//	printf("%d\n", a % c);
//	return 0;
//}
//auto  break   case  char  const   continue  default  do   double else  enum
//extern float  for   goto  if   int   long  register    return   short  signed
//sizeof   static struct  switch  typedef union  unsigned   void  volatile  while
//将unsigned int 重命名为uint_32, 所以uint_32也是一个类型名
//typedef unsigned int uint_32;
//int main()
//{
//    //观察num1和num2,这两个变量的类型是一样的
//    unsigned int num1 = 0;
//    uint_32 num2 = 0;
//    return 0;
//}
//代码1
//#include <stdio.h>
//void test()
//{
//    int i = 0;
//    i++;
//    printf("%d ", i);
//}
//int main()
//{
//    int i = 0;
//    for (i = 0; i < 10; i++)
//    {
//        test();
//    }
//    return 0;
//}
//代码2
//#include <stdio.h>
//void test()
//{
//    //static修饰局部变量
//    static int i = 0;
//    i++;
//    printf("%d ", i);
//}
//int main()
//{
//    int i = 0;
//    for (i = 0; i < 10; i++)
//    {
//        test();
//    }
//    return 0;
//}
////代码1
////add.c
//int g_val = 2018;
////test.c
//int main()
//{
//    printf("%d\n", g_val);
//    return 0;
//}
////代码2
////add.c
//static int g_val = 2018;
////test.c
//int main()
//{
//    printf("%d\n", g_val);
//    return 0;
//}
////代码1
////add.c
//int Add(int x, int y)
//{
//    return c + y;
//}
////test.c
//int main()
//{
//    printf("%d\n", Add(2, 3));
//    return 0;
//}
////代码2
////add.c
//static int Add(int x, int y)
//{
//    return c + y;
//}
////test.c
//int main()
//{
//    printf("%d\n", Add(2, 3));
//    return 0;
//}
//define定义标识符常量
//#define MAX 1000
////define定义宏
//#define ADD(x, y) ((x)+(y))
//#include <stdio.h>
//int main()
//{
//    int sum = ADD(2, 3);
//    printf("sum = %d\n", sum);
//
//    sum = 10 * ADD(2, 3);
//    printf("sum = %d\n", sum);
//
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int num = 10;
//    &num;//取出num的地址
//       //注：这里num的4个字节，每个字节都有地址，取出的是第一个字节的地址（较小的地址）
//    printf("%p\n", &num);//打印地址，%p是以地址的形式打印
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int num = 10;
//    int* p = &num;
//    *p = 20;
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    char ch = 'w';
//    char* pc = &ch;
//    *pc = 'q';
//    printf("%c\n", ch);
//    return 0;
//}
//#include <stdio.h>
////指针变量的大小取决于地址的大小
////32位平台下地址是32个bit位（即4个字节）
////64位平台下地址是64个bit位（即8个字节）
//int main()
//{
//    printf("%d\n", sizeof(char*));
//    printf("%d\n", sizeof(short*));
//    printf("%d\n", sizeof(int*));
//    printf("%d\n", sizeof(double*));
//    return 0;
//}
//struct Stu
//{
//    char name[20];//名字
//    int age;      //年龄
//    char sex[5];  //性别
//    char id[15]； //学号
//};
//打印结构体信息
//struct Stu s = { "张三"， 20， "男"， "20180101" };
////.为结构成员访问操作符
//printf("name = %s age = %d sex = %s id = %s\n", s.name, s.age, s.sex, s.id);
////->操作符
//struct Stu* ps = &s;
//printf("name = %s age = %d sex = %s id = %s\n", ps->name, ps->age, ps->sex, ps -
//> id);
//int main()
//{
//    const char* pstr = "hello bit.";//这里是把一个字符串放到pstr指针变量里了吗？
//    printf("%s\n", pstr);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//    char str1[] = "hello bit.";
//    char str2[] = "hello bit.";
//    const char* str3 = "hello bit.";
//    const char* str4 = "hello bit.";
//    if (str1 == str2)
//        printf("str1 and str2 are same\n");
//    else
//        printf("str1 and str2 are not same\n");
//
//    if (str3 == str4)
//        printf("str3 and str4 are same\n");
//    else
//        printf("str3 and str4 are not same\n");
//
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 2,3,4,5,6 };
//	int arr3[] = { 3,4,5,6,7 };
//	int* arr[] = { arr1,arr2,arr3 };//指针数组，每个元素都是指针（数组名代表首元素的地址）
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("%d ", *(arr[i] + j));
//			//上面的这一行也可以用下面这一行来代替
//			//printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int arr[10] = { 0 };
//    printf("%p\n", arr);
//    printf("%p\n", &arr);
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	printf("arr = %p\n", arr);
//	printf("&arr= %p\n", &arr);
//	printf("arr+1 = %p\n", arr + 1);
//	printf("&arr+1= %p\n", &arr + 1);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5 };
//	int(*ptr)[10] = &arr;
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", *((*ptr) + i));
//		/*printf("%d ",(*ptr)[i]);*/
//		//上面的代码可以由下面的一行代码代替，因为(*ptr)就代表数组名
//	}
//	return 0;
//}
//#include<stdio.h>
//void print(int(*p)[5], int x, int y)
//{
//	for (int i = 0; i < x; i++)
//	{
//		for (int j = 0; j < y; j++)
//		{
//			printf("%d ", *(*(p + i) + j));
//		    //printf("%d ", (*(p + i))[j]);
//			//printf("%d ", p[i][j]);
//			//printf("%d ",*(p[i]+j));
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	print(arr, 3, 5);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	printf("hello world");
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//		//printf("%d ", arr[i]);
//		//printf("%d ", *(p + i));
//		//printf("%d ", *(arr + i));
//	}
//	return 0;
//}
//#include<stdio.h>
//void print(int arr[5])//方法一
////void print(int *arr)方法二
////void print(int arr[])方法三
//{
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	print(arr);
//	return 0;
//}
//#include<stdio.h>
//void print(int* arr[5])//方法一（当然，括号中的5也可以不写，也可以随便写一个数）
////void print(int **arr)方法二（因为数组的元素是指针，而数组名代表首元素的地址，指针的地址就是二级指针）
//{
//
//}
//int main()
//{
//	int* arr[5];
//	print(arr);
//	return 0;
//}
//#include<stdio.h>
//void print(int arr[2][3])//方法一（中规中矩的二维数组传参,此处一定要注意，行可以省略掉，但是列一定不能省略，
////同时需要注意，行可以随便写，但是列一定要与原来的数组保持一致，至于为什么，看下一种方法，即本质就能明白，因为列
//// 就是数组类型的一部分）
////void print(int (*arr)[3])方法二（也就是二维数组传参的本质所在）
//{
//
//}
//int main()
//{
//	int arr[2][3] = { {1,2,3},{4,5,6} };
//	print(arr);
//	return 0;
//}
//#include <stdio.h>
//void print(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//一级指针p，传给函数
//	print(p, sz);
//	return 0;
//}
//void test1(int* p)
//{}
////test1函数能接收什么参数？
///*
//	int a = 1;
//	int *p = &a;
//	int arr[] = {1,2,3,4,5};
//	test1(&a);//可以
//	test1(p);//可以
//	test1(arr);//可以
//*/
//void test2(char* p)
//{}
////test2函数能接收什么参数？
///*
//	char ch = 'w';
//	char *p = &ch;
//	char*arr[] = "abcde";
//	test2(&ch);//可以
//	test2(p);//可以
//	test2(arr);//可以
//*/
//#include<stdio.h>
//void print(int** pp)
//{
//
//}
//int main()
//{
//	int* p[5];
//	print(p);
//	//p是指针数组，数组的每一个元素都是指针，而我们传的是指针数组的数组名，即指针数组的首元素的地址
//	//指针数组的数组的首元素的地址即指针的地址，其类型就是二级指针
//	return 0;
//}
//#include <stdio.h>
//void test()
//{
//	printf("hehe\n");
//}
//int main()
//{
//	printf("%p\n", test);
//	printf("%p\n", &test);
//	return 0;
//}
//void test()
//{
//	printf("hehe\n");
//}
////下面pfun1和pfun2哪个有能力存放test函数的地址？
//void (*pfun1)();
//void* pfun2();
//#include<stdio.h>
//int add(int a, int b)
//{
//	return a + b;
//}
//int main()
//{
//	int (*padd)(int a, int b) = add;
//	int sum = (*padd)(3, 5);//方法一
//	//int sum = padd(3, 5);方法二
//	printf("%d ", sum);
//	return 0;
//}
//#include<stdio.h>
//int add(int x,int y)
//{
//	return x + y;
//}
//int main()
//{
//	int (*p)(int x, int y);
//	p = add;//当然，此处也可以写成p = &add
//	//从上面这段代码中其实就可以明白，其实p和add是几乎完全相同的，所以它们的用法也是差不多完全相同的
//	//int sum = p(3, 4);
//	//上面这一行可以用下面的三种形式进行代替
//	//int sum = (*p)(3, 4);方法一
//	//int sum = add(3, 4);方法二
//	int sum = (*(&add))(3, 4);
//	printf("%d", sum);
//	return 0;
//}
//代码1
//(*(void (*)())0)();
////将0进行强制类型转换为（void (*)())，即函数指针类型，然后对其进行解引用，解引用之后就找到了那个函数，然后再进行函数调用
////此处就是把0当作是一个地址，本质上就是一个函数的调用
////代码2
//void (*signal(int, void(*)(int)))(int);
////首先先看signal(int ,void(*)(int))这是一个函数，第一个参数的类型为int，第二个参数的类型为一个函数指针类型
////接下来把上面的这些去掉，剩下的是void (*)(int)，这就是上面那个函数的返回类型，即返回类型又是一个函数指针
//#include <stdio.h>
//int add(int a, int b)
//{
//    return a + b;
//}
//int sub(int a, int b)
//{
//    return a - b;
//}
//int mul(int a, int b)
//{
//    return a * b;
//}
//int div(int a, int b)
//{
//    return a / b;
//}
//int main()
//{
//    int x, y;
//    int input = 1;
//    int ret = 0;
//    int(*p[5])(int x, int y) = { 0, add, sub, mul, div }; //转移表
//    while (input)
//    {
//        printf("*************************\n");
//        printf(" 1:add           2:sub \n");
//        printf(" 3:mul           4:div \n");
//        printf("*************************\n");
//        printf("请选择：");
//        scanf("%d", &input);
//        if ((input <= 4 && input >= 1))
//        {
//            printf("输入操作数：");
//            scanf("%d %d", &x, &y);
//            ret = (*p[input])(x, y);
//        }
//        else
//            printf("输入有误\n");
//        printf("ret = %d\n", ret);
//    }
//    return 0;
//}
	//#include<stdio.h>
	//void menu()
	//{
	//	printf("***************************\n");
	//	printf("*** 1.Add 2.Sub ***********\n");
	//	printf("*** 3.Mul 4.Div ***********\n");
	//	printf("****** 0.Exit *************\n");
	//}
	//int Add(int x, int y)
	//{
	//	return x + y;
	//}
	//int Sub(int x, int y)
	//{
	//	return x - y;
	//}
	//int Mul(int x, int y)
	//{
	//	return x * y;
	//}
	//int Div(int x, int y)
	//{
	//	return x / y;
	//}
	//int main()
	//{
	//	int input = 0;
	//	do
	//	{
	//		menu();
	//		int (*ptr[])(int x, int y) = { 0,Add,Sub,Mul,Div };
	//		printf("请选择你要进行的计算->");
	//		scanf("%d", &input);
	//		if (input >= 1 && input <= 4)
	//		{
	//			printf("请输入操作数：");
	//			int x = 0;
	//			int y = 0;
	//			scanf("%d %d", &x, &y);
	//			int ret = ptr[input](x, y);
	//			printf("%d\n", ret);
	//		}
	//		else if (input == 0)
	//		{
	//			printf("程序退出");
	//			break;
	//		}
	//		else
	//		{
	//			printf("输入错误！请重新输入！\n");
	//		}
	//	
	//	} while (input);
	//	return 0;
	//}
//#include <stdio.h>
//int add(int a, int b)
//{
//	return a + b;
//}
//int sub(int a, int b)
//{
//	return a - b;
//}
//int mul(int a, int b)
//{
//	return a * b;
//}
//int div(int a, int b)
//{
//	return a / b;
//}
//void Calc(int (*ptrf)())
//{
//	int x = 0;
//	int y = 0;
//	printf("输入操作数：");
//	scanf("%d %d", &x, &y);
//	printf("ret = %d\n", ptrf(x,y));
//}
//int main()
//{
//	int input = 1;
//	int ret = 0;
//	do
//	{
//		printf("*************************\n");
//		printf(" 1:add           2:sub \n");
//		printf(" 3:mul           4:div \n");
//		printf("*************************\n");
//		printf("请选择：");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			Calc(add);
//			break;
//		case 2:
//			Calc(sub);
//			break;
//		case 3:
//			Calc(mul);
//			break;
//		case 4:
//			Calc(div);
//			break;
//		case 0:
//			printf("退出程序\n");
//			break;
//		default:
//			printf("选择错误，请重新输入！\n");
//			break;
//		}
//	} while (input);
//
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int(*p)[10] = &arr;//取出数组的地址
//	int (*pfarr[4])(int, int);
//	int (*(*ppfarr)[4])(int, int) = &pfarr;
//	//ppfarr是一个数组指针，指针指向的数组有4个元素
//	//指向的数组的每个元素的类型是一个函数指针 int(*)(int ,int)
//	return 0;
//}
////void qsort(void* base, size_t num, size_t width, int(__cdecl* compare)(const void* elem1, const void* elem2));
////base 数组的起始位置
////num  数组元素的个数
////width 每个数组元素所占的字节数
////compare 比较函数
////elm1、elm2:接收要比较的两个元素的地址
////此处为什么是void类型的指针？因为void *可以接收任意类型的指针
//
//#include<stdio.h>
//#include<stdlib.h>
//int cmp_int(const void* e1, const void* e2)
//{
//	//比较两个整型值的函数
//	return *(int*)e2 - *(int*)e1;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int(*cmp)(const void* e1, const void* e2) = cmp_int;
//	qsort(arr, sz, sizeof(arr[0]),(*cmp));
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//struct student
//{
//	char name[20];
//	int age;
//};
//int cmp_by_name(const void* e1, const void* e2)
//{
//	return strcmp(((struct student*)e1)->name, ((struct student*)e2)->name);
//}
//int main()
//{
//	struct student students[3] = { {"zhangsan",18},{"lisi",19},{"wangwu",20} };
//	int sz = sizeof(students) / sizeof(students[0]);
//	qsort(students, sz, sizeof(students[0]), cmp_by_name);
//}
//void Swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//void bubble_sort(void* base, int sz, int width, int(*cmp)(void* e1, void* e2))
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width)>0)
//			{
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width,width);
//			}
//		}
//	}
//}
//void swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//void bubble_sort(void* base, int sz, int width, int(*cmp)(void* e1, void* e2))
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//}
//#include<stdio.h>
//int main()
//{
//	//一维数组
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a));//16--计算的是数组总大小，单位是字节
//	//数组名代表数组首元素的地址时，有两个意外
//	//1.sizeof(数组名)  注意：里面必须只有数组名，才能代表整个数组 
//	//2.&数组名
//	printf("%d\n", sizeof(a + 0));//4/8--此处的数组名代表首元素的地址，+0之后仍是首元素的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(*a));//4--a是首元素的地址，*a即是首元素，即int型
//	printf("%d\n", sizeof(a + 1));//4/8--数组名是首元素的地址，a+1即是第二个元素的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(a[1]));//4--第二个元素，即int型
//	printf("%d\n", sizeof(&a));//4/8--整个数组的地址，数组的地址也是地址，地址的大小是4/8个字节，类型是数组指针
//	printf("%d\n", sizeof(*&a));//16--&a是数组的地址，对数组的地址进行解引用就是数组，即求的是一个数组的大小，与sizeof(a)等价
//	printf("%d\n", sizeof(&a + 1));//4/8--&a+1是跳过一个数组的大小，即下一个单位a数组的地址，但其本质上仍然是地址
//	printf("%d\n", sizeof(&a[0]));//4/8--数组首元素的地址
//	printf("%d\n", sizeof(&a[0] + 1));//4/8--数组第二个元素的地址
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	//字符数组
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("%d\n", sizeof(arr));//6--此处sizeof计算的是数组的大小，数组有6个char类型的元素，即6个字节
//	printf("%d\n", sizeof(arr + 0));//4/8--arr是数组首元素的地址，arr+1仍为数组首元素的地址，地址的大小为4/8个字节
//	printf("%d\n", sizeof(*arr));//1--arr是数组首元素的地址，*a即为数组首元素，计算的是数组首元素的大小
//	printf("%d\n", sizeof(arr[1]));//1--计算的是数组首元素的大小
//	printf("%d\n", sizeof(&arr));//4/8--&arr取的是数组的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(&arr + 1));//4/8--&arr是数组的地址，&arr+1是跳过一个单位a数组后的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(&arr[0] + 1));//4/8--数组第二个元素的地址
//	printf("%d\n", strlen(arr));//随机值--strlen遇到字符串的结束标志'\0'才会停止，在这个字符数组中没有'\0',后面内存中放的我们并不知道
//	printf("%d\n", strlen(arr + 0));//随机值--与上一个一样
//	printf("%d\n", strlen(*arr));//程序崩溃--*arr = 'a'，此处把97(字符a的ascii码值)当成是一个地址，此处将会发生程序崩溃的现象，因为非法访问内存地址
//	printf("%d\n", strlen(arr[1]));//程序崩溃--非法访问内存地址，造成程序崩溃，与上一个一样
//	printf("%d\n", strlen(&arr));//随机值--与strlen(arr)和strlen(arr+0)一样，因为数组的地址和数组首元素的地址在数值上是一样的
//	printf("%d\n", strlen(&arr + 1));//随机值--但是与前面的strlen(&arr)随机值有一个固定的差值，比那个小6
//	printf("%d\n", strlen(&arr[0] + 1));//随机值--但是与前面的strlen(&arr)随机值有一个固定的差值，比那个小1
//	//strlen(arr+0)==strlen(arr)==strlen(&arr)
//	//strlen(&arr+1)==strlen(arr)-6
//	//strlen(&arr[0]+1)==strlen(arr)-1
//	//strlen是从()中的地址开始，无论这个地址是数组的地址还是数组首元素的地址还是数组中任意一个元素的地址，都是从该地址开始，一个字节记作是一个
//	//字符，直到遇到'\0'才会停止，结果即为从地址开始到'\0'中间的字节数（字符数）
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char arr[] = "abcdef";
//	printf("%d\n", sizeof(arr));//7--数组所占空间的大小，还有一个是字符串的结束标志'\0'
//	printf("%d\n", sizeof(arr + 0));//4/8--arr是数组首元素的地址，＋0后不会发生改变仍为数组首元素的地址，地址的大小是4或者8个字节
//	printf("%d\n", sizeof(*arr));//1--arr是数组首元素的地址，对其进行解引用之后就是数组首元素，即一个字符，占一个字节
//	printf("%d\n", sizeof(arr[1]));//1--数组的第二个元素，即一个字符，占一个字节
//	printf("%d\n", sizeof(&arr));//4/8--数组的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(&arr + 1));//4/8--跳过一个单位数组后的地址，地址的大小是4或者8个字节
//	printf("%d\n", sizeof(&arr[0] + 1));//4/8--数组第二个元素的地址
//	printf("%d\n", strlen(arr));//6--从数组首元素的地址到'\0'总共出现6个字符，'\0'不算在内
//	printf("%d\n", strlen(arr + 0));//6--数组名代表首元素的地址，加0后依旧是数组首元素的地址，跟上面的一样，6个字符
//	printf("%d\n", strlen(*arr));//程序崩溃--非法访问内存地址，因为访问了字符'a'的ascii码值作为的地址，行为非法
//	printf("%d\n", strlen(arr[1]));//程序崩溃--非法访问内存地址，因为访问了数组第二个元素字符'b'的ascii码值作为的地址，行为非法
//	printf("%d\n", strlen(&arr));//6--&arr是数组的地址，数组的地址和数组首元素的地址在数值上是完全一样的，所以与strlen(arr)结果是完全一样的
//	printf("%d\n", strlen(&arr + 1));//随机值--因为跳过了一个数组后的地址，后面的元素不确定，即不确定'\0'出现在什么位置，所以结果随机
//	printf("%d\n", strlen(&arr[0] + 1));//5--数组第二个元素'b'的地址开始到'\0'的字符的数目，'\0'不计入在内，总共5个字符
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char* p = "abcdef";
//	printf("%d\n", sizeof(p));//4/8--计算的是指针变量p的大小，p中存储的是a的地址，地址的大小为4/8个字节
//	printf("%d\n", sizeof(p + 1));//4/8--p中存储的是a的地址，p+1就是b的地址，地址的大小为4/8个字节
//	printf("%d\n", sizeof(*p));//1--p存储的是字符a的地址，对其进行解引用得到的是就是字符a
//	printf("%d\n", sizeof(p[0]));//1--p[0]就是字符a 注意：p[0]==*(p+0)=='a'
//	printf("%d\n", sizeof(&p));//4/8--&p就是指针变量p的地址，地址的大小是4/8个字节
//	printf("%d\n", sizeof(&p + 1));//4/8--指针变量p的地址加1，p的类型是char *p，&p的类型是char **p，&p+1就是跳过一个字符指针的大小，结果仍然是地址
//	printf("%d\n", sizeof(&p[0] + 1));//4/8--结果就是字符'b'的地址，地址的大小就是4/8个字节
//	printf("%d\n", strlen(p));//6--p中存储的是a的地址，从a到'\0'，不把'\0'计入在内总共有6个字符
//	printf("%d\n", strlen(p + 1));//5--p中存储的是a的地址，p的类型是char *，跳过一个char元素的地址，即'b'的地址
//	printf("%d\n", strlen(*p));//程序崩溃--非法访问以'a'的ascii码值作为的地址
//	printf("%d\n", strlen(p[0]));//程序崩溃--非法访问以'a'的ascii码值作为的地址
//	printf("%d\n", strlen(&p));//随机值--此处是从指针变量p的地址开始，后面的值我们并不确定
//	printf("%d\n", strlen(&p + 1));//随机值--&p的类型是char**,&p+1就是指针变量p的地址跳过一个char*类型（指针类型，大小为4/8个字节），此处类似上一个，地址值和地址后面的值我们都不清楚
//	printf("%d\n", strlen(&p[0] + 1));//5--此处就是从第二个元素'b'的地址开始的
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char* p = "abcdef";
//	printf("%p\n", &p);
//	printf("%p", &p + 1);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[3][4] = { 0 };
//	printf("%d\n", sizeof(a));//48--求的是整个数组所占空间的大小，总共12个整型元素，12*4=48个字节
//	printf("%d\n", sizeof(a[0][0]));//4--数组第一行第一个元素的大小
//	printf("%d\n", sizeof(a[0]));//16--a[0]是第一行的数组名，因为数组a是由三个一维数组组成的，这三个一维数组的数组名分别是a[0]、a[1]、a[2]
//	//而当数组名单独出现在sizeof后面的括号中时，代表的是整个一维数组，所以在此时就是整个一维数组a[0]，有4个元素
//	printf("%d\n", sizeof(a[0] + 1));//4/8--数组名代表首元素的地址，a[0]表示的是第一行的数组名，此处表示的是第一行第一个元素的首地址，加1后是第一行第二个元素的地址
//	printf("%d\n", sizeof(*(a[0] + 1)));//4--其实就是对前面这一个进行解引用，解引用后就是第一行第二个元素
//	printf("%d\n", sizeof(a + 1));//4/8--此处a是数组名，但是a既不是sizeof(数组名)，也不是&数组名，a是数组首元素的地址，二维数组的元素即是一维数组，此处a即是第一行的地址，加1后即为第二行的地址
//	printf("%d\n", sizeof(*(a + 1)));//16--此处a+1是第二行的地址，也是第二行的数组名，此处等价于sizeof(a[1]),所以对其解引用后就是第二行的所有元素，即4*4=16
//	printf("%d\n", sizeof(&a[0] + 1));//4/8--a[0]是数组第一行的数组名，&a[0]就是第一行的地址，加1后就是第二行的地址
//	printf("%d\n", sizeof(*(&a[0] + 1)));//16--对第二行的地址进行解引用之后得到的就是第二行的所有元素
//	printf("%d\n", sizeof(*a));//16--a是首元素的地址，*a就是第一行，所以解引用后就是第一行的所有元素
//	printf("%d\n", sizeof(a[3]));//16--第四行的数组名，a[3]是第四行数组，有四个整型元素，所以是16个字节
//	//注意：最后这个为什么不会发生越界访问的错误？因为在sizeof括号中的表达式不参与运算，也就是不进行访问，既然都没有进行访问，所以就不会出现越界访问的错误了
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[5] = { 1, 2, 3, 4, 5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d,%d", *(a + 1), *(ptr - 1));
//	return 0;
//}
////程序的结果是什么？
//#include<stdio.h>
//struct Test
//{
//	int Num;
//	char* pcName;
//	short sDate;
//	char cha[2];
//	short sBa[4];
//}*p;
////假设p 的值为0x100000。 如下表表达式的值分别为多少？
////已知，结构体Test类型的变量大小是20个字节
//int main()
//{
//	p = (struct Test*)0x100000;
//	printf("%p\n", p + 0x1);
//	printf("%p\n", (unsigned long)p + 0x1);
//	printf("%p\n", (unsigned int*)p + 0x1);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[4] = { 1, 2, 3, 4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x,%x", ptr1[-1], *ptr2);
//	return 0;
//}
//#include<stdio.h>
//#include <stdio.h>
//int main()
//{
//	int a[3][2] = { (0, 1), (2, 3), (4, 5) };
//	int* p;
//	p = a[0];
//	printf("%d", p[0]);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a[5][5];
//	int(*p)[4];
//	p = a;
//	printf("%p,%d\n", &p[4][2] - &a[4][2], &p[4][2] - &a[4][2]);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int aa[2][5] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
//	int* ptr1 = (int*)(&aa + 1);
//	int* ptr2 = (int*)(*(aa + 1));
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	char* a[] = { "work","at","alibaba" };
//	char** pa = a;
//	pa++;
//	printf("%s\n", *pa);
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	char* c[] = { "ENTER","NEW","POINT","FIRST" };
//	char** cp[] = { c + 3,c + 2,c + 1,c };
//	char*** cpp = cp;
//	printf("%s\n", **++cpp);
//	printf("%s\n", *-- * ++cpp + 3);
//	printf("%s\n", *cpp[-2] + 3);
//	printf("%s\n", cpp[-1][-1] + 1);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//int my_strlen(const char* str)
//{
//	assert(str != NULL);
//	int count = 0;
//	while (*str != '\0')//while(*str)---'\0'的ascii码值为0
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//int main()
//{
//	char* str = "abcde";
//	int ret = my_strlen(str);
//	printf("%d", ret);
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	const char* str1 = "abcdef";
//	const char* str2 = "bbb";
//	if (strlen(str2) - strlen(str1) > 0)
//	{
//		printf("str2>str1\n");
//	}
//	else
//	{
//		printf("srt1>str2\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//int my_strlen(const char* str)
//{
//	const char* ret = NULL;
//	assert(*str != NULL);
//	ret = str;
//	while (*ret != "\0")
//	{
//		ret++;
//	}
//	return ret - str;
//}
//int main()
//{
//
//}
//#include<stdio.h>
//#include<assert.h>
//char*  my_strcpy(char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* ret = dest;
//	//拷贝src指向的字符串到dest指向的空间，包含'\0'
//	while (*dest++ = *src++)
//	{}
//	//返回目的空间的起始地址
//	return ret;
//}
//
//int main()
//{
//	char str1[] = "abcde";
//	char str2[] = "min";
//	my_strcpy(str1,str2);
//	printf("%s", str1);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//char* my_strcat(char* str1, const char* str2)
//{
//	assert(str1&&str2);
//	char* ret = str1;
//	//1、找到目的字符串中'\0'的位置
//	while (*str1 != '\0')
//	{
//		str1++;
//	}
//	//2、追加字符串（其实就是拷贝字符串）
//	while (*str1++ = *str2++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char str1[50] = "hello ";
//	char str2[] = "world";
//	my_strcat(str1, str2);
//	printf("%s", str1);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;//相等
//		}
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		//大于
//		return 1;
//	}
//	else
//	{
//		//小于
//		return -1;
//	}
//}
//int main()
//{
//	char *str1 = "abcde";
//	char *str2 = "abcfg";
//	int ret = my_strcmp(str1, str2);
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//char* my_strncpy(char* str1, const char* str2,int count)
//{
//	assert(str1 && str2);
//	char* ret = str1;
//	int i = 0;
//	for (i = 0; i < count; i++)
//	{
//		if (*str2 != '\0')
//		{
//			*str1++ = *str2++;
//		}
//		else
//		{
//			*str1++ = '\0';
//		}
//	}
//	return ret;
//}
//char* my_strcpy(char* dest, const char* src, size_t count)
//{
//	char* start = dest;
//	while (count && (*dest++ = *src++))
//		count--;
//
//	if (count)
//		while (--count)
//			*dest++ = '\0';
//	return start;
//}
//int main()
//{
//	char str1[] = "abcdefg";
//	char str2[] = "abc";
//	my_strncpy(str1, str2,6);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//char* my_strncat(char* str1, const char* str2, int count)
//{
//	assert(str1 && str2);
//	char* ret = str1;
//	while (*str1++ != '\0');
//	str1--;
//	int i = 0;
//	for (i = 0; i < count; i++)
//	{
//		if (*str2 != '\0')
//		{
//			*str1++ = *str2++;
//		}
//		else
//		{
//			*str1 = '\0';
//			return ret;
//		}
//	}
//	*str1 = '\0';
//	return ret;
//}
//int main()
//{
//	char str1[50] = "abcde\0xxxxxxxx";
//	char str2[] = "bit";
//	my_strncat(str1, str2, 5);
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//int my_strncmp(const char* str1, const char* str2, int count)
//{
//	assert(str1 && str2);
//	int i = 0;
//	for (i = 0; i < count; i++)
//	{
//		if (*str1 < *str2)
//		{
//			return -1;
//		}
//		else if (*str1 > *str2)
//		{
//			return 1;
//		}
//		else
//		{
//			str1++;
//			str2++;
//		}
//	}
//	return 0;
//}
//int main()
//{
//	char str1[] = "abcdef";
//	char str2[] = "abcdef";
//	int ret = my_strncmp(str1, str2, 10);
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char* str1 = "abcdefghijk";
//	char* str2 = "def";
//	char*ret = strstr(str1, str2);
//	if (ret == NULL)
//	{
//		printf("字串不存在\n");
//	}
//	else
//	{
//		printf("%s\n",ret);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//char* my_strstr(const char* str1,const char* str2)
//{
//	assert(str1 && str2);
//	char* p1 = str1;
//	char* p2 = str2;
//	char* cur = str1;
//	if (*str2 == '\0')
//	{
//		return str1;
//	}
//
//	while (*cur)
//	{
//		p1 = cur;
//		p2 = str2;
//		while ((*p1 != '\0') && (*p2 != '\0') && *p1 == *p2)
//		{
//			p1++;
//			p2++;
//		}
//		if (*p2 == '\0')
//		{
//			return cur;//找到子串
//		}
//
//		cur++;
//	}
//	return NULL;//找不到子串
//}
//int main()
//{
//	char str1[] = "abcdefghi";
//	char str2[] = "def";
//	char *ret = my_strstr(str1, str2);
//	printf("%s", ret);
//	return 0;
//}
//#include<stdio.h>
//char* my_strstr(char* str1, char* str2)
//{
//	assert(str1 && str2);
//	char* p1 = NULL;
//	char* p2 = NULL;
//	char* cur = str1;
//	if (*str2 == '\0')
//	{
//		return str1;
//	}
//	while (*cur)
//	{
//		p1 = cur;
//		p2 = str2;
//		while ((*p1 != '\0') && (*p2 != '\0') && *p1 == *p2)
//		{
//			p1++;
//			p2++;
//		}
//		if (*p2 == '\0')
//		{
//			return cur;
//		}
//		cur++;
//	}
//	return NULL;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char* arr = "123@456@abc.def.com";
//	char* p = "@.";
//	char str[500] = { 0 };
//	strcpy(str, arr);
//	char* ret = NULL;
//	for (ret = strtok(str, p);ret != NULL;ret = strtok(NULL,p))
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<errno.h>
//#include<string.h>
//int main()
//{
//	//errno是一个全局的错误码的变量
//	//当C语言的库函数在执行过程中，发生了错误，就会把对应的错误码，赋值到errno这个变量中
//	char* str = strerror(errno);
//	printf("%s\n", str);
//	return 0;
//}
/* isupper example */
//#include <stdio.h>
//#include <ctype.h>
//int main()
//{
//	int i = 0;
//	char str[] = "Test String.\n";
//	char c;
//	while (str[i])
//	{
//		c = str[i];
//		if (isupper(c))
//			c = tolower(c);
//		putchar(c);
//		i++;
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<ctype.h>
//int main()
//{
//	char arr[] = "I am a student!";
//	int i = 0;
//	while (arr[i])
//	{
//
//		arr[i] = toupper(arr[i]);
//			i++;
//	}
//	printf("%s", arr);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[5] = {0};
//	memcpy(arr2, arr1, sizeof(arr1));
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//void *my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//	int i = 0;
//	while(num--)
//	{
//		*(char*)dest = *(char*)src;
//		((char*)dest)++;
//		((char*)src)++;
//	} 
//	return dest;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[5] = { 0 };
//	my_memcpy(arr2, arr1, sizeof(arr1));
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	memmove(arr + 2, arr, 20);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<assert.h>
//void *my_memmove(void* dest, void* src, size_t num)
//{
//	//dest<src：从前向后拷贝
//	//dest>src&&dest<src+count：从后向前拷贝
//	//dest>src+count：从后向前和从前向后均可
//	assert(dest && src);
//	void* ret = dest;
//	if (dest < src)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			((char*)dest)++;
//			((char*)src)++;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest+num )= *((char*)src+num);
//		}
//	}
//	return dest;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr , arr+2, 20);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 1,2,4,5,6 };
//	int ret = memcmp(arr1, arr2, sizeof(arr1));
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[10] = "";
//	memset(arr, '#', 10);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%c ", arr[i]);
//	}
//	return 0;
//}
//struct tag//tag:结构体类型名
//{
//	member - list;//member - list:成员列表
//}variable - list;//variable - list:变量列表
//struct Stu
//{
//	char name[20];//名字
//	int age;//年龄
//	char sex[5];//性别
//	char id[20];//学号
//}; //分号不能丢
////匿名结构体类型
//struct
//{
//	int a;
//	char b;
//	float c;
//}x;
//struct
//{
//	int a;
//	char b;
//	float c;
//}a[20], * p;
//struct Node
//{
//	int data;
//	struct Node* next;
//}Node;
//struct Point
//{
//	int x;
//	int y;
//}p1; //声明类型的同时定义变量p1
//struct Point p2; //定义结构体变量p2
////初始化：定义变量的同时赋初值。
//struct Point p3 = { x, y };
//struct Stu        //类型声明
//{
//	char name[15];//名字
//	int age;      //年龄
//};
//struct Stu s = { "zhangsan", 20 };//初始化

//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化

//#include<stdio.h>
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//struct S3
//{
//	double d;
//	char c;
//	int i;
//};
//struct S4
//{
//	char c1;
//	struct S3 s3;
//	double d;
//};
//int main()
//{
//	printf("%d %d %d %d", sizeof(struct S1), sizeof(struct S2), sizeof(struct S3), sizeof(struct S4));
//	return 0;
//}
////例如：
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//struct S2
//{
//	char c1;
//	char c2;
//	int i;
//};
//#include <stdio.h>
//#pragma pack(8)//设置默认对齐数为8
//struct S1
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//#pragma pack(1)//设置默认对齐数为1
//struct S2
//{
//	char c1;
//	int i;
//	char c2;
//};
//#pragma pack()//取消设置的默认对齐数，还原为默认
//int main()
//{
//	//输出的结果是什么？
//	printf("%d\n", sizeof(struct S1));
//	printf("%d\n", sizeof(struct S2));
//	return 0;
//}
//#include<stdio.h>
//#include<stddef.h>
//struct S
//{
//	char c1;
//	char c2;
//	int i;
//};
//int main()
//{
//	printf("%d ", offsetof(struct S, c1));
//	printf("%d ", offsetof(struct S, c2));
//	printf("%d ", offsetof(struct S, i));
//	return 0;
//}
//#include<stdio.h>
//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//int main()
//{
//	printf("%d", sizeof(struct A));
//	return 0;
//}
//#include<stdio.h>
//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//int main()
//{
//	struct S s = { 0 };//下面的数据从左向右依次是原数据----二进制位----在内存中存储的数据
//	s.a = 10;//10----1010----010
//	s.b = 20;//20----10010----0010
//	s.c = 3;//3----11----00011
//	s.d = 4;//4----100----0100
//	//下面是在内存中真正存储的数据
//	//在VS中默认是从右向左存的
//	//第一个char型空间:0 0010 010----2 2（16进制的形式）
//	//第二个char型空间:000 00011(c)----0 3（16进制的形式）
//	//第三个char型空间:0000 0100(d)----0 4（16进制的形式）
//	//所以在内存中的布局应为22 03 04
//	return 0;
//}
//#include<stdio.h>
//enum Day//星期
//{
//	Mon,
//	Tues,
//	Wed,
//};
//int main()
//{
//	printf("%d %d %d", Mon, Tues, Wed);
//	return 0;
//}
//#include<stdio.h>
//enum Color//颜色
//{
//	RED = 1,
//	GREEN,
//	BLUE = 4
//};
//int main()
//{
//	/*enum Color RED = 3;
//	printf("%d %d %d", RED, GREEN, BLUE);*/
//	enum Color clr = GREEN;//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异。
//	clr = 5;               //ok??
//	return 0;
//}
//enum Color//颜色
//{
//	RED = 1,
//	GREEN = 2,
//	BLUE = 4
//};
//enum Color clr = GREEN;//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异。
//clr = 5;               //ok??
//enum Color//颜色
//{
//	RED = 1,
//	GREEN = 2,
//	BLUE = 4
//};
//enum Color clr = GREEN;//只能拿枚举常量给枚举变量赋值，才不会出现类型的差异。
//clr = 5;
//#include<stdio.h>
//int main()
//{
//	int year = 0;
//	int month = 0;
//	int day = 0;
//	int ret = 0;
//	scanf("%d %d %d", &year, &month, &day);
//	int arr[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
//	if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
//	{
//		arr[1] = 29;
//	}
//	int i = 0;
//	for (i = 0; i < year; i++)
//	{
//		ret += arr[i];
//	}
//	ret += day;
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//int x = 5, y = 7;
//void swap()
//{
//	int z;
//	z = x;
//	x = y;
//	y = z;
//}
//int main()
//{
//	int x = 3, y = 8;
//	swap();
//	printf("%d,%d\n",x, y);
//	return 0;
//}
//#include<stdio.h>
////联合类型的声明
//union Un
//{
//	char c;
//	int i;
//};
//int main()
//{
//	//联合变量的定义
//	union Un un;
//	//计算连个变量的大小
//	printf("%d\n", sizeof(un));
//	printf("%p\n", &un);
//	printf("%p\n", &un.c);
//	printf("%p\n", &un.i);
//	return 0;
//}
//#include<stdio.h>
//union S
//{
//	int i;
//	char ch;
//};
//
//int check_sys()
//{
//	union S s;
//	s.i = 1;
//	return s.ch;
//}
//int main()
//{
//	union S s;
//	s.i = 1;
//	int ret = check_sys();
//	if (1 == ret)
//	{
//		printf("小端存储\n");
//	}
//	else
//	{
//		printf("大端存储\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//int check_sys()
//{
//	int a = 1;
//	return *(char*)&a;
//}
//int main()
//{
//	int ret = check_sys();
//	if (1 == ret)
//	{
//		printf("小端存储\n");
//	}
//	else
//	{
//		printf("大端存储\n");
//	}
//	return 0;
//}
//#include<stdio.h>
//enum sex
//{
//	MALE,
//	FEMALE,
//	SECRET
//};
//int main()
//{
//	enum sex s = FEMALE;
//	printf("%d\n", sizeof(enum sex));
//	printf("%d\n", sizeof(s));
//	return 0;
//}
union un
{
	int a;//对齐数为4，默认对齐数为8，取较小值，即4
	char arr[5];//最大成员的大小为5，对齐数为1（拿char来算，而不是拿整个数组所占的内存空间来算），默认对齐数为8，取较小值1
	//所以联合体的最大对齐数为4，所以union un的内存空间的大小必须是4的倍数，同时还必须大于5，所以空间大小为8个字节
};