#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"
//可以存放1000个人的信息
//人的姓名、年龄、性别、地址、电话
//增、删、查、改、清空、排序
void menu()
{
	printf("************************************\n");
	printf("******  1.add     2.del     ********\n");
	printf("******  3.search  4.modify  ********\n");
	printf("******  5.clear   6.sort    ********\n");
	printf("******  7.show    0.exit    ********\n");
	printf("************************************\n");

}
int main()
{
	struct contact con;
	Init_contact(&con);
	int input = 0;
	do
	{
		menu();
		printf("请选择->");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			Add_Contact(&con);
			break;
		case 2:
			Del_Contact(&con);
			break;
		case 3:
			Search_Contact(&con);
			break;
		case 4:
			Mol_Contact(&con);
			break;
		case 5:
			Clear_Contact(&con);
			break;
		case 6:
			Sort_Contact(&con);
			break;
		case 7:
			Show_Contact(&con);
			break;
		case 0:
			printf("退出通讯录！\n");
			Destory_Contact(&con);
			break;
		default:
			printf("输入错误，请重新输入！\n");
			break;
		}
	} while (input);
	return 0;
}