#define _CRT_SECURE_NO_WARNINGS 1
#include"contact.h"
//初始化通讯录
void Init_contact(struct contact* con)
{
	assert(con);
	struct PeoInfo*tmp = (struct PeoInfo*)malloc(sizeof(struct PeoInfo) * DEFAULT_SZ);
	if (tmp != NULL)
	{
		con->data = tmp;
	}
	else
	{
		printf("InitContact()::%s\n",strerror(errno));
	}
	con->size = 0;
	con->capacity = DEFAULT_SZ;
}
//添加通讯录中的信息
//void Add_Contact(struct contact*con)
//{
//	assert(con);
//	if (con->size == MAX_NUM)
//	{
//		printf("通讯录已满！\n");
//		return ;
//	}
//	printf("请输入姓名->");
//	scanf("%s", con->data[con->size].name);
//	printf("请输入性别->");
//	scanf("%s", con->data[con->size].sex);
//	printf("请输入年龄->");
//	scanf("%d", &con->data[con->size].age);
//	printf("请输入电话->");
//	scanf("%s", con->data[con->size].tel);
//	printf("请输入地址->");
//	scanf("%s", con->data[con->size].address);
//	printf("添加成功！\n");
//	con->size++;
//}
void Check_Capacity(struct contact* con)
{
	if (con->capacity == con->size)
	{
		//增加容量
		struct PeoInfo* tmp = realloc(con->data, con->capacity + 2);
		if (tmp != NULL)
		{
			con->data = tmp;
			con->capacity += 2;
			printf("增容成功！\n");
		}
		else
		{
			printf("Check_Capacity()::%s\n", strerror(errno));
		}
	}
}
void Add_Contact(struct contact* con)
{
	assert(con);
	Check_Capacity(con);
	//输入联系人的信息
	printf("请输入姓名->");
	scanf("%s", con->data[con->size].name);
	printf("请输入性别->");
	scanf("%s", con->data[con->size].sex);
	printf("请输入年龄->");
	scanf("%d", &con->data[con->size].age);
	printf("请输入电话->");
	scanf("%s", con->data[con->size].tel);
	printf("请输入地址->");
	scanf("%s", con->data[con->size].address);
	printf("添加成功！\n");
	con->size++;
}
//显示通讯录中的信息
void Show_Contact(struct contact* con)
{
	assert(con);
	int i = 0;
	printf("%-20s\t%-5s\t%-5s\t%-15s\t%-20s\n","姓名","年龄","性别","电话","地址");
	for (i = 0; i < con->size; i++)
	{
		printf("%-20s\t%-5d\t%-5s\t%-15s\t%-20s\n", con->data[i].name, 
			con->data[i].age, 
			con->data[i].sex, 
			con->data[i].tel, 
			con->data[i].address);
	}
}
int Find_Contact(char* tmp, struct contact* con)
{
	int i = 0;
	for (i = 0; i < con->size; i++)
	{
		if (strcmp(tmp, con->data[i].name) == 0)
		{
			return i;
		}
	}
	return -1;
}
//删除通讯录中的信息
void Del_Contact(struct contact*con)
{
	assert(con);
	if (con->size == 0)
	{
		printf("当前通讯录为空!\n");
	}
	char tmp[MAX_NAME];
	printf("请输入你要删除的联系人的姓名->");
	scanf("%s", tmp);
	int pos = Find_Contact(tmp, con);
	if (pos == -1)
	{
		printf("你输入的联系人不存在！\n");
		return;
	}
	else
	{
		memmove(&con->data[pos],&con->data[pos+1],(sizeof(con->data[0])*(con->size-pos-1)));
		con->size--;
		printf("删除联系人成功！\n");
	}
}
//查找联系人的信息
void Search_Contact(struct contact* con)
{
	assert(con);
	if (con->size == 0)
	{
		printf("当前通讯录为空!\n");
	}
	char tmp[MAX_NAME];
	printf("请输入你要查找的联系人的姓名->");
	scanf("%s", tmp);
	int pos = Find_Contact(tmp, con);
	if (pos == -1)
	{
		printf("你想查找的联系人不存在！\n");
		return;
	}
	else
	{
		printf("%-20s\t%-5s\t%-5s\t%-15s\t%-20s\n", "姓名", "年龄", "性别", "电话", "地址");
		printf("%-20s\t%-5d\t%-5s\t%-15s\t%-20s\n", con->data[pos].name,
			con->data[pos].age,
			con->data[pos].sex,
			con->data[pos].tel,
			con->data[pos].address);
	}
}
//修改联系人的信息
void Mol_Contact(struct contact*con)
{
	assert(con);
	if (con->size == 0)
	{
		printf("当前通讯录为空!\n");
	}
	char tmp[MAX_NAME];
	printf("请输入你要修改的联系人的姓名->");
	scanf("%s", tmp);
	int pos = Find_Contact(tmp, con);
	if (pos == -1)
	{
		printf("你想修改的联系人不存在！\n");
		return;
	}
	else
	{
		printf("请输入姓名->");
		scanf("%s", con->data[pos].name);
		printf("请输入性别->");
		scanf("%s", con->data[pos].sex);
		printf("请输入年龄->");
		scanf("%d", &con->data[pos].age);
		printf("请输入电话->");
		scanf("%s", con->data[pos].tel);
		printf("请输入地址->");
		scanf("%s", con->data[pos].address);
		printf("修改成功！\n");
	}
}
//清空通讯录的信息
void Clear_Contact(struct contact* con)
{
	assert(con);
	memset(con, 0, sizeof(con->data));
	con->size = 0;
}
int Cmp_age(const void* e1,const void* e2)
{
	return ((struct PeoInfo*)e1)->age - ((struct PeoInfo*)e2)->age;
}
//对通讯录的信息进行排序
void Sort_Contact(struct contact* con)
{
	qsort(con->data, con->size, sizeof(con->data[0]), Cmp_age);
	printf("排序成功！\n");
}
//销毁通讯录
void Destory_Contact(struct contact* con)
{
	assert(con);
	free(con->data);
	con->data = NULL;
	con->capacity = 0;
	con->size = 0;
}