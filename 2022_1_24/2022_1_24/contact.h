#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include<errno.h>
#define MAX_NAME 20
#define MAX_NUM 1000
#define MAX_ADDRESS 20
#define MAX_NAME 20
#define MAX_TEL 11
#define MAX_SEX 5
#define DEFAULT_SZ 3

struct PeoInfo
{
	char name[MAX_NAME];
	int age;
	char sex[MAX_SEX];
	char address[MAX_ADDRESS];
	char tel[MAX_TEL];
};
struct contact
{
	struct PeoInfo* data;//存放数据
	int size;//通讯录中有效信息的个数
	int capacity;//记录当前通讯录的最大容量
};
//初始化通讯录中的信息
void Init_contact(struct contact*);
//添加通讯录中的信息
void Add_Contact(struct contact*);
//显示通讯录中的信息
void Show_Contact(struct contact*);
//删除通讯录的信息
void Del_Contact(struct contact*);
//查找联系人信息
void Search_Contact(struct contact*);
//修改联系人的信息
void Mol_Contact(struct contact*);
//清空通讯录的信息
void Clear_Contact(struct contact*);
//对通讯录的信息进行排序
void Sort_Contact(struct contact*);
//销毁通讯录
void Destory_Contact(struct contact*);