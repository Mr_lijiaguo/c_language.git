#pragma once
#define MAX_NAME 20
#define MAX_PHONE 12
#define MAX_SEX 5
#define MAX_ADDRESS 30
#define _CRT_SECURE_NO_WARNINGS 1
#define DEFAULT_SZ 3
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<errno.h>
struct PeoInfo
{
	char name[MAX_NAME];
	char phone[MAX_PHONE];
	char sex[MAX_SEX];
	char address[MAX_ADDRESS];
	int age;
};
struct contact
{
	struct PeoInfo* data;
	int size;//记录当前已经有的元素个数
	int capacity;//记录当前通讯录的最大容量
};
//声明函数
//初始化通讯录的函数
void Init_Contact(struct contact* ps);
//增加一个信息到通讯录
void Add_Contacter(struct contact* ps);
//删除指定的联系人
void Del_Contacter(struct contact* ps);
//查找指定人的信息
void Find_Contacter(const struct contact* ps);
//修改指定人的信息
void Mod_Contacter(struct contact* ps);
//打印通讯录中的信息
void Print_Contacter(const struct contact* ps);
//对通讯录中的联系人进行排序
void Sort_Contacter(struct contact* ps);
//清空通讯录中的信息
void Clear_Contacter(struct contact* ps);
//销毁通讯录
void Destory_Contact(struct contact*ps);
//保存通讯录的信息
void Save_Contact(struct contact* ps);
//加载文件中的信息到通讯录中
void Load_Contact(struct contact* ps);

