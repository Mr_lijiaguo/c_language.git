
#include"contact.h"
enum Option
{
	EXIT,//0
	ADD,//1
	DEL,//2
	SEARCH,//3
	MODIFY,//4
	SHOW,//5
	CLEAR,//6
	SORT,//7
	SAVE
};
void menu()
{
	printf("************************************\n");
	printf("****  1.add         2.del       ****\n");
	printf("****  3.search      4.modify    ****\n");
	printf("****  5.show        6.clear     ****\n");
	printf("****  7.sort        8.save      ****\n");
	printf("*********     0.exit      **********\n");
	printf("************************************\n");
}
//测试函数的功能
int main()
{
	//创建通讯录
	struct contact con;//con就是通讯录，里面包含：data指针和size,capacity
	//初始化通讯录
	Init_Contact(&con);
	int input = 0;
	int size = 0;//记录当前有多少人
	do
	{
		menu();
		printf("请选择->");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			//增加
			Add_Contacter(&con);
			break;
		case DEL:
			//删除
			Del_Contacter(&con);
			break;
		case SEARCH:
			//查找
			Find_Contacter(&con);
			break;
		case MODIFY:
			//修改
			Mod_Contacter(&con);
			break;
		case SHOW:
			//打印
			Print_Contacter(&con);
			break;
		case CLEAR:
			//清空
			Clear_Contacter(&con);
			break;
		case SORT:
			//排序
			Sort_Contacter(&con);
			break;
		case EXIT:
		    //退出并销毁通讯录
			Save_Contact(&con);
			Destory_Contact(&con);
			printf("退出通讯录！\n");
			break;
		case 8:
			//保存通讯录
			Save_Contact(&con);
			break;
		default:
			printf("选择错误，请重新输入！\n");
		}
	} while (input);
	return 0;
}