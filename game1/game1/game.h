#pragma once
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<windows.h>
#define ROW 3
#define COL 3
//菜单函数声明
void menu();
//游戏函数声明
void game();
//棋盘初始化函数声明
void Init_board(char board[ROW][COL], int row, int col);
//棋盘展示函数声明
void Display_board(char board[ROW][COL], int row, int col);
//玩家下棋函数声明
void player_board(char board[ROW][COL], int row, int col);
//电脑下棋函数声明
void computer_board(char board[ROW][COL],int row,int col);
//判断是否结束函数声明
char is_end(char board[ROW][COL], int row, int col);
//判断谁获胜函数声明
void is_win(char flag);
