#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
//菜单实现
void menu()
{
	printf("*************************\n");
	printf("*******  1.play  ********\n");
	printf("*******  0.exit  ********\n");
	printf("*************************\n");
}
//游戏函数
void game()
{
	char board[ROW][COL] = { 0 };
	Init_board(board, ROW, COL);//棋盘初始化
	Display_board(board, ROW, COL);//棋盘展示
	while (1)
	{
		player_board(board, ROW, COL);//玩家下棋
		Display_board(board, ROW, COL);//棋盘展示
		printf("\n");
		if (is_end(board, ROW, COL) != 'c')
		{
			is_win(is_end(board, ROW, COL));
			break;
		}
		computer_board(board, ROW, COL);//电脑下棋
		Display_board(board, ROW, COL);//棋盘展示
		printf("\n");
		if (is_end(board, ROW, COL) != 'c')
		{
			is_win(is_end(board, ROW, COL));
			break;
		}
	}
}
//棋盘初始化
void Init_board(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}
//棋盘展示
void Display_board(char board[ROW][COL], int row, int col)
{
	for (int i = 0; i < row; i++)
	{
		for (int j = 0; j < col; j++)
		{
			if (j < col - 1)
				printf(" %c |", board[i][j]);
			else
				printf(" %c \n", board[i][j]);
		}
		if (i < row-1 )
		{
			for (int j = 0; j < col; j++)
			{
				if (j < col - 1)
					printf("___|");
				else
					printf("___\n");
			}
		}
		else
		{
			for (int j = 0; j < col; j++)
			{
				if (j < col - 1)
					printf("   |");
				else
					printf("   \n");
			}
		}
	}
}
//玩家下棋
void player_board(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	while (1)
	{
		int i = 0;
		int j = 0;
		printf("请输入下棋位置->");
		scanf("%d %d", &i, &j);
		if (i <= row && i >= 1 && j <= col && j >= 1)
		{
			if (board[i-1][j-1] == ' ')
			{
				board[i-1][j-1] = '*';
				break;
			}
			else
			{
				printf("坐标已有棋，清重新输入！\n");
			}
		}
		else
		{
			printf("坐标非法，清重新输入！\n");
		}
	}
}
//电脑下棋
void computer_board(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	while (1)
	{
		i = rand() % 3;
		j = rand() % 3;
		if (board[i][j] ==' ')
		{
			board[i][j] = '#';
			break;
		}
		else
			continue;
	}

}

//判断是否结束
char is_end(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	//行相同
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] &&board[i][1]== board[i][2] && board[i][0] != ' ')
			return board[i][0];
	}

	//列相同
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] &&board[1][i]== board[2][i] && board[0][i] != ' ')
			return board[0][i];
	}
	//’\'对角线
	if (board[0][0] == board[1][1] && board[1][1]== board[2][2] && board[0][0] != ' ')
		return board[0][0];
	//'/'对角线
	if (board[2][0] == board[1][1] == board[0][2] && board[2][0] != ' ')
		return board[2][0];
	//未结束
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
				return 'c';
		}
	}
	//平局
	return '-';
}
//判断谁获胜
void is_win(char flag)
{
	if (flag == '*')
	{
		printf("玩家获胜！\n");
		system("pause");
		system("cls");
	}
	else if (flag == '#')
	{
		printf("电脑获胜！\n");
		system("pause");
		system("cls");
	}
	else if (flag == '-')
	{
		printf("平局!\n");
		system("pause");
		system("cls");
	}
}
