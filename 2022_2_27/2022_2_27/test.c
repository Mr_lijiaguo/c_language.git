#define _CRT_SECURE_NO_WARNINGS 1
//#include <stdio.h>
//int main()
//{
//    int array[ARRAY_SIZE];
//    int i = 0;
//    for (i = 0; i < ARRAY_SIZE; i++)
//    {
//        array[i] = i;
//    }
//    for (i = 0; i < ARRAY_SIZE; i++)
//    {
//        printf("%d ", array[i]);
//    }
//    printf("\n");
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//#if 1
//	for (int i = 0; i < 10; i++)
//	{
//		printf("hello world\n");
//	}
//#endif
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int n = 10;
//#if n
//	for (int i = 0; i < 10; i++)
//	{
//		printf("hello world\n");
//	}
//#endif
//	return 0;
//}

//#include<stdio.h>
//#define M 100
//int main()
//{
//#if M<100
//	printf("less\n");
//#elif M>100
//	printf("big\n");
//#else
//	printf("equal\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//
//int main()
//{
//#if M==0
//	printf("less\n");
//#elif M>100
//	printf("==\n");
//#else
//	printf("more\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//#define M 10
//int main()
//{
//#if defined(M)
//	printf("hello world\n");
//#endif 
//	return 0;
//}
//#include<stdio.h>
//#define M 100
//int main()
//{
//#ifdef M
//	printf("hello\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//
//int main()
//{
//#if !defined(M)
//	printf("hello\n");
//#endif
//	return 0;
//}
//#include<stdio.h>
//#define M 100
//int main()
//{
//#ifndef M
//	printf("haha\n");
//#endif
//	return 0;
//}

//#include<stdio.h>
//#include<assert.h>
//#include<ctype.h>
//#include<limits.h>
////1.空指针
////2.空字符串（注意考虑空字符串与0返回的值，空字符串返回的0是非法的，0返回的0是合法的）
////3.空白字符
////4.+-
////5.非数字字符
////6.超大数字
//enum State
//{
//	INVALID,//非法
//	VALID//合法
//};
//enum State status = INVALID ;
//int my_atoi(const char* str)
//{
//	//空指针
//	assert(str);
//	//空字符串
//	if (*str == '\0')
//	{
//		return 0;
//	}
//	//跳过空白字符
//	while (isspace(*str))
//	{
//		str++;
//	}
//	int flag = 1;
//	//正负数
//	if (*str == '+')
//	{
//		flag = 1;
//		str++;
//	}
//	else if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//	long long int n = 0;
//	while (isdigit(*str))
//	{
//		n = n*10+flag*(*str - '0');
//		//为什么不在return处*flag，因为可能是越界，也有可能是赋越界
//		//越界的值
//		if (n > INT_MAX || n < INT_MIN)
//		{
//			return 0;
//		}
//		str++;
//	}
//	if (*str == '\0')
//	{
//		status = VALID;
//		return (int)n;
//	}
//	return (int)n;
//}
//int main()
//{
//	char str[100] = { 0 };
//	scanf("%s", str);
//	int ret = my_atoi(str);
//	if (status == VALID)
//	{
//		printf("%d\n", ret);
//	}
//	else
//	{
//		printf("非法返回\n");
//	}
//	return 0;
//}
