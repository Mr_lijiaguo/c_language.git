#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//int main()
//{
//	//实现一个代码data.txt拷贝一份生成data2.txt
//	FILE* pr = fopen("data.txt", "r");
//	if (pr == NULL)
//	{
//		printf("open for reading: %s\n",strerror(errno));
//		return 0;
//	}
//	FILE* pw = fopen("data2.txt", "w");
//	if (pw == NULL)
//	{
//		printf("open for writting: %s\n",strerror(errno));
//		fclose(pr);
//		pr = NULL;
//		return 0;
//	}
//	//拷贝文件的操作
//	int ch = 0;
//	while ((ch = fgetc(pr)) != EOF)
//	{
//		fputc(ch, pw);
//	}
//	//关闭文件
//	fclose(pr);
//	pr = NULL;
//	fclose(pw);
//	pw = NULL;
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//struct stu
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct stu s = { "张三",20,95.5 };
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		printf("open for writting: %s\n", strerror(errno));
//		return 0;
//	}
//	//写格式化的数据
//	fprintf(stdout,"%s %d %f", s.name, s.age, s.score);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//struct Stu
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct Stu s = {0};
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		printf("open for writting: %s\n", strerror(errno));
//		return 0;
//	}
//	//读格式化的数据
//	fscanf(pf, "%s %d %lf", s.name, &(s.age), &(s.score));
//	printf("%s %d %lf", s.name, s.age, s.score);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//二进制的方式写
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//struct Stu
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct Stu s[2] = { { "张三",20,95.55},{"李四",21,60}};
//	FILE* pf = fopen("data.txt", "wb");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//按照二进制的方式写文件
//	fwrite(&s, sizeof(struct Stu), 2, pf);
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//二进制的方式读
//#include<stdio.h>
//#include<string.h>
//#include<errno.h>
//struct Stu
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct Stu s[2] = {0};
//	FILE* pf = fopen("data.txt", "rb");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//按照二进制的方式读文件
//	fread(s, sizeof(struct Stu), 2, pf);
//	printf("%s %d %lf\n", s[0].name, s[0].age, s[0].score);
//	printf("%s %d %lf\n", s[1].name, s[1].age, s[1].score);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int m;
//	while (~scanf("%d", &m)) {
//		int start = m * (m - 1) + 1;//找到对应m^3的起始奇数
//		char buf[10240] = { 0 };
//		//sprintf(buf, format, ...) 与printf用法类似，格式化字符串但是不用于打印而是放到一个buf中
//		sprintf(buf, "%d", start);//先将起始奇数转换成为字符串存入buf中
//		for (int i = 1; i < m; i++) {
//			//然后将紧随随后的m-1个奇数数字转换为字符串，按照指定格式放入buf中
//			//%s+%d, 要求先有一个字符串，然后是+符号，然后是个数字的格式，对应是buf原先的数据，和奇数
//			sprintf(buf, "%s+%d", buf, start += 2);
//		}
//		printf("%s\n", buf);
//	}
//	return 0;
//}
//int minNumberInRotateArray(int* rotateArray, int rotateArrayLen) {
//	if (rotateArrayLen == 0) return 0;
//	int left = 0, right = rotateArrayLen - 1, mid;
//	if (rotateArray[right] > rotateArray[left]) return rotateArray[0];
//	while (left < right) {
//		mid = left + (right - left) / 2;
//		if (rotateArray[mid] > rotateArray[right]) left = mid + 1;
//		else if (rotateArray[mid] == rotateArray[right]) right--;
//		else right = mid;
//	}
//	return rotateArray[left];
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//    char code[100] = { 0 };
//    while (scanf(" %s", code) != EOF)
//    {
//        if (code[0] <= 58 && code[0] >= 48)
//        {
//            printf("NO\n");
//            continue;
//        }
//        if (strlen(code) < 8)
//        {
//            printf("NO\n");
//            continue;
//        }
//        int big = 0;
//        int small = 0;
//        int num = 0;
//        int i = 0;
//        for (i = 0; i < strlen(code); i++)
//        {
//            if (code[i] >= 48 && code[i] <= 58)
//            {
//                num = 1;
//            }
//            else if (code[i] >= 65 && code[i] <= 90)
//            {
//                big = 1;
//            }
//            else if (code[i] >= 96 && code[i] <= 122)
//            {
//                small = 1;
//            }
//            else
//            {
//                printf("NO\n");
//                continue;
//            }
//        }
//        int flag = num + big + small;
//        if (flag >= 2 && flag <= 3)
//        {
//            printf("YES\n");
//            continue;
//        }
//        else
//        {
//            printf("NO\n");
//            continue;
//        }
//    }
//
//    return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int n;
//	while (~scanf("%d", &n)) {
//		for (int i = 0; i < n; i++) {
//			char password[101] = { 0 };
//			int upper = 0, lower = 0, digit = 0, other = 0;
//			scanf("%s", password);//捕捉输入的密码
//			if (strlen(password) < 8) {//密码长度小于8
//				printf("NO\n");
//				continue;
//			}
//			if (password[0] >= '0' && password[0] <= '9') {//密码以数字开头
//				printf("NO\n");
//				continue;
//			}
//			char* ptr = password;
//			while (*ptr != '\0') { //统计各种字符个数
//				if (*ptr >= 'a' && *ptr <= 'z') lower++;
//				else if (*ptr >= 'A' && *ptr <= 'Z') upper++;
//				else if (*ptr >= '0' && *ptr <= '9') digit++;
//				else other++;
//				ptr++;
//			}
//			if (other > 0) { // 有其他字符（注意：密码只能由数字和字母组成）
//				printf("NO\n");
//				continue;
//			}
//			//大写，小写，数字，必须具有两种以上，而比较运算真则1，假则0
//			if ((upper > 0) + (lower > 0) + (digit > 0) < 2) { // 密码只有一种字符
//				printf("NO\n");
//				continue;
//			}
//			printf("YES\n");
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int dominantIndex(int* nums, int numsSize)
//{
//	if (numsSize == 1)
//	{
//		return 0;
//	}
//	int firstmax = 0;
//	int secondmax = 0;
//	int i = 0;
//	int index = 0;
//	if (nums[0] > nums[1])
//	{
//		firstmax = nums[0];
//		secondmax = nums[1];
//	}
//	else
//	{
//		firstmax = nums[1];
//		secondmax = nums[0];
//	}
//	for (i = 2; i < numsSize; i++)
//	{
//		if (nums[i] > firstmax)
//		{
//			secondmax = firstmax;
//			firstmax = nums[i];
//			index = i;
//		}
//		if (nums[i]>secondmax)
//		{
//			secondmax = nums[i];
//		}
//	}
//	if (firstmax > 2 * secondmax)
//	{
//		return index;
//	}
//	else
//	{
//		return -1;
//	}
//}
//int main()
//{
//	int arr[10] = { 1,2,4,6,7,9,8,5,3,0 };
//	int ret = dominantIndex(arr, 10);
//	printf("%d", ret);
//	return 0;
//}
//#include<stdio.h>
//int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize)
//{
//	static int arr[1000] = { 0 };
//	int i = 0;
//	int j = 0;
//	int num = 0;
//	for (i = 0; i < nums1Size; i++)
//	{
//		for (j = 0; j < nums2Size; j++)
//		{
//			if (nums1[i] == nums2[j])
//			{
//				arr[num++] = nums1[i];
//				break;
//			}
//			else
//			{
//				continue;
//			}
//		}
//	}
//}
//int main()
//{
//	int arr1[5] = { 1,2,3,4,5 };
//	int arr2[5] = { 3,4,5,6,7 };
//	int ret = 0;
//	intersection(arr1, 5, arr2, 5, &ret);
//	return 0;
//}
//int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize) {
//	static int arr[1000];
//	*returnSize = 0;
//	int i, j, k;
//	for (i = 0; i < nums1Size; i++) {
//		for (j = 0; j < nums2Size; j++) {
//			if (nums2[j] == nums1[i]) break;//判断nums1[i] 是否在nums2数组中
//		}
//		if (j == nums2Size) {// nums1中i位置的数据在nums2数组中不存在，则非交集数据
//			continue;
//		}
//		//只有在另一个数组中存在的数据才能走下来，判断是否已经被添加到返回数组中
//		for (j = 0; j < *returnSize; j++) {
//			if (nums1[i] == arr[j]) break;//判断nums1[i] 是否在 arr 这个返回数组中
//		}
//		if (j == *returnSize) {//不在返回数组中，则添加到返回数组中
//			arr[*returnSize] = nums1[i];
//			*returnSize += 1;
//		}
//	}
//	return arr;
//}
//int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize)
//{
//	static int arr[1000] = { 0 };
//	int i = 0;
//	int j = 0;
//	int k = 0;
//	for (i = 0; i < nums1Size; i++)
//	{
//		for (j = 0; j < nums2Size; j++)
//		{
//			if (nums1[i] == nums2[j])
//			{
//				break;//nums[i]在nums[i]中的数字存在
//			}
//		}
//		if (j == nums2Size)
//		{
//			continue;//nums1[i]在nuns2[i]中的数字不存在
//		}
//		//判断是否已经被添加到数组中
//		for (j = 0; j < *returnSize; j++)
//		{
//			if (nums1[i] == arr[j])
//			{
//				break;//已经存在，不再添加
//			}
//		}
//		if (j == *returnSize)//不在返回数组中，添加
//		{
//			arr[*returnSize] = nums1[i];
//			*returnSize++;
//		}
//	}
//	return arr;
//}
//int* intersection(int* nums1, int nums1Size, int* nums2, int nums2Size, int* returnSize)
//{
//	int i = 0;
//	int j = 0;
//	static int arr[1000] = { 0 };
//	for (i = 0; i < nums1Size; i++)
//	{
//		for (j = 0; j < nums2Size; j++)
//		{
//			if (nums1[i] == nums2[j])
//			{
//				break;
//			}
//			if (j == nums2Size)
//			{
//				continue;
//			}
//			for (j = 0; j < *returnSize; j++)
//			{
//				if (nums1[i] == arr[j])
//				{
//					break;
//				}
//			}
//			if (j == *returnSize)
//			{
//				arr[*returnSize] = nums1[i];
//				*returnSize++;
//			}
//		}
//	}
//	return arr;
//}
//int get_bin_count(int num) {
//	int count = 0;
//	for (int i = 0; i < 32; i++) {
//		if ((num >> i) & 1)
//			count++;
//	}
//	return count;
//}
//int convertInteger(int A, int B) {
//	return get_bin_count(A ^ B);
//}
#include<stdio.h>
int* findErrorNums(int* nums, int numsSize, int* returnSize)
{
	int repeat = 0;
	int less = 0;
	for (int i = 0; i < numsSize; i++)
	{

	}
}
int main()
{
	int arr[10] = { 1,2,2,4,5,6,7,8,9,10 };
	return 0;
}